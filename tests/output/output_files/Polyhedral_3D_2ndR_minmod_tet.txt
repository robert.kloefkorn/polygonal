Warning: Unable to read parameter file 'rotation/problems/parameter'
Loading macro grid: ../grids/tet.0.poly.dgf
Warning: codimension 2 not available in PolyhedralGrid
LimiterFunction: minmod with limitEps = 1e-08
LimiterFunction: minmod with limitEps = 1e-08
output = ./output/3D/transport_-1_PolyhedralGrid_3D_2nd-R
xMin = 0.062485  xMax 0.953084
Volume of domain = 1
Warning: codimension 2 not available in PolyhedralGrid
Number of elements: 215
L1-error at final time t=0.503145: 0.0835612  h 0.0273782
Mass change: 0.0838576
CPU time used: 3.68729600e-02 seconds.
l1 error 0.0835612  215 0.0273782
Warning: Unable to read parameter file 'rotation/problems/parameter'
Loading macro grid: ../grids/tet.1.poly.dgf
Warning: codimension 2 not available in PolyhedralGrid
LimiterFunction: minmod with limitEps = 1e-08
LimiterFunction: minmod with limitEps = 1e-08
output = ./output/3D/transport_-1_PolyhedralGrid_3D_2nd-R
xMin = 0.026077  xMax 0.974045
Volume of domain = 1
Warning: codimension 2 not available in PolyhedralGrid
Number of elements: 2003
L1-error at final time t=0.500358: 0.0488201  h 0.0104585
Mass change: 0.0833929
CPU time used: 9.44244254e-01 seconds.
l1 error 0.0488201  2003 0.0104585
EOC( 1 ) = .55847375614698496068
--------------------------------------------
Warning: Unable to read parameter file 'rotation/problems/parameter'
Loading macro grid: ../grids/tet.2.poly.dgf
Warning: codimension 2 not available in PolyhedralGrid
LimiterFunction: minmod with limitEps = 1e-08
LimiterFunction: minmod with limitEps = 1e-08
output = ./output/3D/transport_-1_PolyhedralGrid_3D_2nd-R
xMin = 0.0158457  xMax 0.982613
Volume of domain = 1
Warning: codimension 2 not available in PolyhedralGrid
Number of elements: 3898
L1-error at final time t=0.500443: 0.0452693  h 0.00805728
Mass change: 0.0834071
CPU time used: 2.47376370e+00 seconds.
l1 error 0.0452693  3898 0.00805728
EOC( 2 ) = .28950046045713094179
--------------------------------------------
Warning: Unable to read parameter file 'rotation/problems/parameter'
Loading macro grid: ../grids/tet.3.poly.dgf
Warning: codimension 2 not available in PolyhedralGrid
LimiterFunction: minmod with limitEps = 1e-08
LimiterFunction: minmod with limitEps = 1e-08
output = ./output/3D/transport_-1_PolyhedralGrid_3D_2nd-R
xMin = 0.0158282  xMax 0.984402
Volume of domain = 1
Warning: codimension 2 not available in PolyhedralGrid
Number of elements: 7711
L1-error at final time t=0.500161: 0.0409588  h 0.00627796
Mass change: 0.0833602
CPU time used: 6.21577511e+00 seconds.
l1 error 0.0409588  7711 0.00627796
EOC( 3 ) = .40100203070593406811
--------------------------------------------
Warning: Unable to read parameter file 'rotation/problems/parameter'
Loading macro grid: ../grids/tet.4.poly.dgf
Warning: codimension 2 not available in PolyhedralGrid
LimiterFunction: minmod with limitEps = 1e-08
LimiterFunction: minmod with limitEps = 1e-08
output = ./output/3D/transport_-1_PolyhedralGrid_3D_2nd-R
xMin = 0.00978387  xMax 0.989406
Volume of domain = 1
Warning: codimension 2 not available in PolyhedralGrid
Number of elements: 15266
L1-error at final time t=0.500313: 0.0371349  h 0.00467169
Mass change: 0.0833855
CPU time used: 1.60860571e+01 seconds.
l1 error 0.0371349  15266 0.00467169
EOC( 4 ) = .33164612710017308918
--------------------------------------------
Warning: Unable to read parameter file 'rotation/problems/parameter'
Loading macro grid: ../grids/tet.5.poly.dgf
Warning: codimension 2 not available in PolyhedralGrid
LimiterFunction: minmod with limitEps = 1e-08
LimiterFunction: minmod with limitEps = 1e-08
output = ./output/3D/transport_-1_PolyhedralGrid_3D_2nd-R
xMin = 0.00775771  xMax 0.99214
Volume of domain = 1
Warning: codimension 2 not available in PolyhedralGrid
Number of elements: 30480
L1-error at final time t=0.500341: 0.03336  h 0.00373453
Mass change: 0.0833902
CPU time used: 4.58845422e+01 seconds.
l1 error 0.03336  30480 0.00373453
EOC( 5 ) = .47878592884018909575
--------------------------------------------
Warning: Unable to read parameter file 'rotation/problems/parameter'
Loading macro grid: ../grids/tet.6.poly.dgf
Warning: codimension 2 not available in PolyhedralGrid
LimiterFunction: minmod with limitEps = 1e-08
LimiterFunction: minmod with limitEps = 1e-08
output = ./output/3D/transport_-1_PolyhedralGrid_3D_2nd-R
xMin = 0.00777909  xMax 0.992775
Volume of domain = 1
Warning: codimension 2 not available in PolyhedralGrid
Number of elements: 61052
L1-error at final time t=0.50022: 0.0302079  h 0.00309906
Mass change: 0.08337
CPU time used: 1.55322320e+02 seconds.
l1 error 0.0302079  61052 0.00309906
EOC( 6 ) = .53212754704759669789
--------------------------------------------
