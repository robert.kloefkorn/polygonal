Warning: Unable to read parameter file 'rotation/problems/parameter'
Loading macro grid: ../grids/simplex2-0.dgf
output = ./output/2D/solid_body_-1_PolygonGrid_2D_2nd-R
xMin = 0.0212622  xMax 0.979579
Volume of domain = 1
Number of elements: 101
L1-error at final time t=1.57905: 0.088469  h 0.0693422
Mass change: 0.252744
CPU time used: 5.16380760e-02 seconds.
l1 error 0.088469  101 0.0693422
Warning: Unable to read parameter file 'rotation/problems/parameter'
Loading macro grid: ../grids/simplex2-1.dgf
output = ./output/2D/solid_body_-1_PolygonGrid_2D_2nd-R
xMin = 0.00810185  xMax 0.990823
Volume of domain = 1
Number of elements: 370
L1-error at final time t=1.58405: 0.0707465  h 0.0346711
Mass change: 0.752008
CPU time used: 2.63512381e-01 seconds.
l1 error 0.0707465  370 0.0346711
EOC( 1 ) = .32251323975547977444
--------------------------------------------
Warning: Unable to read parameter file 'rotation/problems/parameter'
Loading macro grid: ../grids/simplex2-2.dgf
output = ./output/2D/solid_body_-1_PolygonGrid_2D_2nd-R
xMin = 0.00405093  xMax 0.995411
Volume of domain = 1
Number of elements: 1415
L1-error at final time t=1.57192: 0.0564955  h 0.0173356
Mass change: 3.32428
CPU time used: 1.63360283e+00 seconds.
l1 error 0.0564955  1415 0.0173356
EOC( 2 ) = .32452416844119936847
--------------------------------------------
Warning: Unable to read parameter file 'rotation/problems/parameter'
Loading macro grid: ../grids/simplex2-3.dgf
output = ./output/2D/solid_body_-1_PolygonGrid_2D_2nd-R
xMin = 0.00202546  xMax 0.997706
Volume of domain = 1
Number of elements: 5533
L1-error at final time t=1.5735: 0.0397806  h 0.00866778
Mass change: 12.2909
CPU time used: 1.39812675e+01 seconds.
l1 error 0.0397806  5533 0.00866778
EOC( 3 ) = .50606923728715099901
--------------------------------------------
Warning: Unable to read parameter file 'rotation/problems/parameter'
Loading macro grid: ../grids/simplex2-4.dgf
output = ./output/2D/solid_body_-1_PolygonGrid_2D_2nd-R
xMin = 0.00101273  xMax 0.998853
Volume of domain = 1
Number of elements: 21881
L1-error at final time t=1.57248: 0.0259184  h 0.00433389
Mass change: 47.6228
CPU time used: 1.24931963e+02 seconds.
l1 error 0.0259184  21881 0.00433389
EOC( 4 ) = .61808837588938442835
--------------------------------------------
