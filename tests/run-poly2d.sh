#!/bin/bash
RUNS="0 1 2 3 4"

EXEC=femfv_poly2d_vl
#EXEC=femfv_alusimp_mm
DIMP=2
#PARAM=solid-body-2d
PARAM=bucklev
GRIDFILE=fem.io.macroGridFile_2d:../grids/simplex2-
GRIDEXT=.dgf
if [ "$1" == "3" ] ; then
  EXEC=femfv_poly_sb
  DIMP=4
  PARAM=transport-3d
  #GRIDFILE=fem.io.macroGridFile_3d:../grids/tet.
  #GRIDFILE=fem.io.macroGridFile_3d:../grids/vmesh_
  GRIDFILE=fem.io.macroGridFile_3d:../grids/dbls_
  GRIDEXT=0.msh.dgf
  #GRIDEXT=poly.dgf
fi
if [ "$1" == "3tet" ] ; then
  RUNS="0 1 2 3 4 5 6"
  EXEC=femfv_poly_vl
  DIMP=4
  #PARAM=transport-3d
  PARAM=bucklev-3d
  GRIDFILE=fem.io.macroGridFile_3d:../grids/tet.
  #GRIDFILE=fem.io.macroGridFile_3d:../grids/vmesh_
  GRIDEXT=.poly.dgf
fi
if [ "$1" == "3vm" ] ; then
  RUNS="0 1 2 3 4 5"
  EXEC=femfv_poly
  DIMP=4
  PARAM=transport-3d
  GRIDFILE=fem.io.macroGridFile_3d:../grids/vmesh_
  GRIDEXT=.msh.dgf
fi

PREVERR=
PREVELM=
PREVH=
for RUN in $RUNS; do
  OUTPUT=/tmp/out-$RUN.txt
  ../fem-fv/$EXEC $GRIDFILE$RUN$GRIDEXT finitevolume.repeats:0 $PARAM &> $OUTPUT
  L1ERROR=`cat $OUTPUT | grep "L1-error" | cut -d ":" -f 2 | cut -d " " -f 2`
  H=`cat $OUTPUT | grep "L1-error" | cut -d ":" -f 2 | cut -d " " -f 5`
  ELEM=`cat $OUTPUT | grep "Number of elements" | cut -d ":" -f 2`
  cat $OUTPUT
  echo "l1 error $L1ERROR $ELEM $H"
  if [ "$PREVERR" != "" ]; then
    #EOC=`echo "l($L1ERROR/$PREVERR) / l($ELEM/$PREVELM) " | bc -l`
    EOC=`echo "l($PREVERR/$L1ERROR) / l($PREVH/$H) " | bc -l`
    echo "EOC( $RUN ) = $EOC"
    echo "--------------------------------------------"
  fi
  PREVERR=$L1ERROR
  PREVELM=$ELEM
  PREVH=$H
done
