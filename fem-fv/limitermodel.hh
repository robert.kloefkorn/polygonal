#ifndef LIMITER_MODEL_HH
#define LIMITER_MODEL_HH

#include <cmath>
#include <dune/common/fvector.hh>
#include <dune/fem/io/parameter.hh>


namespace Dune
{

namespace Fem
{

typedef MinModLimiter< double > FemFVLimiterFunctionType ;
//typedef SuperBeeLimiter< double > FemFVLimiterFunctionType ;
//typedef VanLeerLimiter< double > FemFVLimiterFunctionType ;

template <class GridPartType, class ProblemType>
class LimiterModel;

template <class GridPart, class Problem>
class LimiterModelTraits
{
public:
  typedef Problem  ProblemType;
  typedef double Field;
  typedef Field RangeFieldType;
  typedef Field FieldType;
  typedef GridPart GridPartType;
  typedef typename GridPart::GridType GridType;
  enum { dimDomain = GridType::dimensionworld };
  enum { dimRange  = ProblemType::dimRange };
  enum { dimGradRange = dimRange * dimDomain };
  typedef FieldVector<typename GridType::ctype, dimDomain> DomainType;
  typedef FieldVector<typename GridType::ctype, dimDomain-1> FaceDomainType;
  typedef FieldVector<RangeFieldType, dimRange> RangeType;
  typedef FieldVector<RangeFieldType, dimGradRange> GradientType;
  typedef typename GridPart::IntersectionIteratorType IntersectionIteratorType;
  typedef typename IntersectionIteratorType :: Intersection IntersectionType;
  typedef typename GridType::template Codim<0>::Entity EntityType;
  typedef FemFVLimiterFunctionType  LimiterFunctionType;
  typedef LimiterModel< GridPart, Problem > ModelType;
};

/** \brief LimiterModel for second order extension by RK */
template <class GridPartType, class Problem>
class LimiterModel
{
public:
  typedef Problem ProblemType;
  enum { dimDomain = GridPartType::GridType::dimensionworld };
  enum { dimGrid = GridPartType::GridType::dimension };
  enum { dimRange = ProblemType :: dimRange };
  typedef LimiterModelTraits<GridPartType, ProblemType> Traits;
  typedef typename Traits::FieldType FieldType;
  typedef typename Traits::GridType GridType;
  typedef typename Traits::RangeType RangeType;
  typedef typename Traits::DomainType DomainType;
  typedef typename Traits::FaceDomainType FaceDomainType;
  typedef typename GridPartType::IntersectionIteratorType IntersectionIteratorType;
  typedef typename IntersectionIteratorType :: Intersection IntersectionType;
  typedef typename Traits::EntityType EntityType;

protected:
  typedef LimiterModel<GridPartType,ProblemType> ThisType;

public:
  LimiterModel( const ProblemType& problem ) :
    problem_(problem)
  {}


  // has flux
  bool hasFlux() const
  {
    return true;
  }

  // has source term
  bool hasSource() const
  {
    return false;
  }

  //! needed for limitation to determine inflow and outflow intersections
  inline void velocity( const EntityType& entity,
                        const double time,
                        const DomainType& x,
                        const RangeType& u,
                        DomainType& velocity) const
  {
    velocity = problem_.velocity();
    //problem_.velocity( x, time, u, velocity );
  }


  /** \brief adjust average values, e.g. transform to primitive or something similar */
  void adjustAverageValue( const EntityType& entity,
                           const DomainType& xLocal,
                           RangeType& u ) const
  {
  }

  inline void reAdjust( RangeType &u ) const
  {
  }

  // we have physical check for this model
  bool hasPhysical() const
  {
    return true;
  }



  // calculate jump between left and right value
  inline bool physical( const double value ) const
  {
    return (value >= 0.0) && (value <= 1.0);
  }

  // calculate jump between left and right value
  inline bool physical(  const EntityType& entity,
                          const DomainType& xLocal,
                          const RangeType& u ) const
  {
    bool result = true;
    for( int i=0; i<dimRange; ++ i )
      result = result && physical( u[ i ] );
    return result;
  }

  inline void jump(
        const IntersectionType& it,
        const double time,
        const FaceDomainType& x,
        const RangeType& uLeft,
        const RangeType& uRight,
        RangeType &shockIndicator ) const
  {
    shockIndicator[0] = (uLeft[0] - uRight[0])/(0.5 * (uLeft[0] + uRight[0]));
  }

  // calculate jump between left and right value
  inline void adaptationIndicator(
                   const IntersectionType& it,
                   const double time,
                   const FaceDomainType& x,
                   const RangeType& uLeft,
                   const RangeType& uRight,
                   RangeType& indicator) const
  {
    // use jump of values
    jump( it, time, x, uLeft, uRight, indicator );
  }

  inline bool hasBoundaryValue(const IntersectionType& it,
                               const double time,
                               const FaceDomainType& x) const
  {
    return false;
  }

  template <class LocalEvaluation>
  inline void boundaryValue(const LocalEvaluation& local,
                            const RangeType& uLeft,
                            RangeType& uRight) const
  {
    // copy value (for limitation)
    uRight = uLeft;
  }

  const ProblemType& problem () const { return problem_; }

protected:
  const ProblemType& problem_;
};

} // end namespace Fem
} // end namespace Dune

#endif
