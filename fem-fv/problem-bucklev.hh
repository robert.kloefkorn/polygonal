#ifndef PROBLEM_BUCKLEV_HH
#define PROBLEM_BUCKLEV_HH

// C++ includes
#include <iostream>
#include <sstream>

// dune-common includes
#include <dune/common/fvector.hh>

// dune-fem includes
#include <dune/fem/space/common/functionspace.hh>

// local includes
#include "problem.hh"

// BuckleyLeverettModelTraits
// --------------------

template< class DomainField, int dimD >
struct BuckleyLeverettModelTraits
{
  const static int dimDomain = dimD;
  const static int dimRange = 1;

  typedef DomainField DomainFieldType;
  typedef double RangeFieldType;

  typedef Dune::Fem::FunctionSpace< DomainFieldType, RangeFieldType, dimDomain, dimRange > FunctionSpaceType;
};

/** \class BuckleyLeverettProblemData
 *  \brief basic problem data for BuckleyLeverettProblem
 *
 *  This class defines the data for the following transport problem:
 *  \f{eqnarray*}
 *  \partial_t c + \nabla \cdot (v c)
 *    &=& 0 \quad\mbox{in $\Omega \times ]0,T[$}\\
 *  c &=& g \quad\mbox{on $\Gamma_{\mathrm{in}}$}\\
 *  c &=& c_0 \quad\mbox{on $\Omega \times \lbrace 0 \rbrace$}
 *  \f}
 *
 *  We define the following parameters:
 *  - a grid for the domain \f$\Omega\f$,
 *  - the (constant) velocity \f$v\f$,
 *  - the boundary data \f$b\f$,
 *  - the initial data \f$c_0\f$.
 */
template< class FunctionSpace >
class BuckleyLeverettProblemData
: public ProblemData< FunctionSpace >
{
  typedef ProblemData< FunctionSpace > BaseType;

public:
  static const int dimDomain = BaseType::dimDomain;
  static const int dimRange  = BaseType::dimRange;

  typedef typename BaseType::DomainType DomainType;
  typedef typename BaseType::RangeType RangeType;

  /** \brief constructor */
  BuckleyLeverettProblemData ()
  //: velocity_( 0 )
  {}

  /** \brief obtain the (constant) velocity for the transport problem */
 /* const DomainType &velocity () const
  {
    return velocity_;
  }
*/

  virtual bool physical( const RangeType &value ) const
  {
    if( value[ 0 ] < 0.0 || value[1] > 1.0 ) return false;
    return true;
  }

  virtual void velocity ( const DomainType &x, double time, const RangeType& u, DomainType& v ) const
  {
    v = DomainType(0);
  }


  std::string gridFile ( const std::string &path ) const
  {
    std::stringstream key;
    key << "fem.io.macroGridFile_" << dimDomain << "d";
    return Dune::Fem::Parameter::getValue<std::string>(key.str());
  }

  /** \brief evaluate the exact solution
   *
   *  \param[in]  x     coordinate to evaluate the exact solution in
   *  \param[in]  time  time to evaluate exact solution at
   *
   *  \returns the evaluated exact solution
   */
  virtual RangeType exact ( const DomainType &x, double time ) const
  {
    DomainType x0( x );
    DomainType v( 0 );
    x0.axpy( -time, v );
    return initial( x0 );

  }

  //! \copydoc ProblemData::boundaryValue
  RangeType boundaryValue ( const DomainType &x, double time ) const
  {
    return exact( x, time );

  }

  double f(double u) const {
    if (u<0.0)
      return 0.;
    if (u>1.0)
      return 1.;
    return u*u/(u*u+0.5*(1.-u)*(1.-u));
  }

  double f1(const double u) const {
    if (u<0.0)
      return 0.;
    if (u>1.0)
      return 0.;
    double d=3.*u*u+1.-2.*u;
    return -4.*u*(-1.+u)/d/d;
  }

  double f2(double u) const {
    double d=3.*u*u+1.-2.*u;
    return 4.*(-9.*u*u+6.*u*u*u+1.)/d/d/d;
  }

  // note: we are overloading one method evaluate and need to tell
  //       the compiler that we what the inherit the other versions of evaluate
  using BaseType::evaluate;

  //! \copydoc ProblemData::evaluate
  void evaluate ( const DomainType &x, double time, RangeType &ret ) const
  {
    ret = exact( x, time );
  }

  double saveInterval () const
  {
    return 0.05;
  }

virtual bool hasExactSolution() const
{
    return true;
}

//! \copydoc ProblemData::initial
  virtual RangeType initial ( const DomainType &x ) const
  {
    //dummy values to avoid pure virtual functions
    return RangeType( 0 );
  }

  //! \copydoc ProblemData::endTime
  virtual double endTime () const
  {
    //dummy values to avoid pure virtual functions
    return 0.5;
  }

/*********************************************************/
/***                 NEW FOR LESSON 1                  ***/
/*********************************************************/
  //! \copydoc ProblemData::refineTol
  virtual double refineTol () const
  {
    //dummy values to avoid pure virtual functions
    return 0.1;
  }

/** evaluate adaptation indicator
   *
   *  \param[in]  uLeft   value on the "left" side of the intersection
   *  \param[in]  uRight  value on the "right" side of the intersection
   *
   *  \returns a real number indicating how to adapt the grid
   */
  double adaptationIndicator ( const RangeType &uLeft, const RangeType &uRight ) const
  {
    return std::abs( uLeft[ 0 ] - uRight[ 0 ] );
  }
/*********************************************************/

protected:
 // DomainType velocity_;
};

/**
 * \brief Discontinuous initial data problem: characteristic function for
    \f$ \{ x\colon |x| < \frac{1}{2} \} \f$
 */
template< class FunctionSpace >
class BuckleyLeverettProblemData1
: public BuckleyLeverettProblemData< FunctionSpace >
{
  typedef BuckleyLeverettProblemData< FunctionSpace > BaseType;

public:
  using BaseType :: f;
  using BaseType :: f1;
  using BaseType :: f2;

  const static int dimDomain = BaseType::dimDomain;
  const static int dimRange = BaseType::dimRange;

  typedef typename BaseType::DomainType DomainType;
  typedef typename BaseType::RangeType RangeType;

  BuckleyLeverettProblemData1 () : flag_( 3 )
  {
    velocity_ = DomainType( 0 );
    velocity_[ 0 ] = 1;// 000.0;
  }

  virtual bool hasExactSolution () const { return true; }

  virtual void velocity ( const DomainType &x, double time, const RangeType& u, DomainType& v ) const
  {
    v = velocity_;
  }

  double velo() const {
    return velocity_[0];
  }

  double Newton(double u0,double xi) const {
    double u=u0;
    while (fabs(f1(u)-xi)>1e-8) {
      double u1=u;
      double df2=f2(u1);
      assert(fabs(df2)>1e-10);
      u=u1-(f1(u1)-xi)/df2;
      // cerr << u1 << " " << u << " " << f1(u)-xi << endl;
    }
    return u;
  }

  double rp_sol(double ul,double ur,double t,double x) const {
    double u1=(3.-sqrt(6.))/3.;
    double u2=sqrt(3.)/3.;
    double xi=x/t;
    if (ul<ur)
    {
      if (ul>u1)
      {
        double fl=f(ul);
        double fr=f(ur);
        double s=(fl-fr)/(ul-ur);
        if (xi<s) return ul;
        else return ur;
            } else if (ur<u1) {
        double dfl=f1(ul);
        double dfr=f1(ur);
        if (xi<dfl)
          return ul;
        else if (xi>dfr)
          return ur;
        else
          return Newton(0.5*(ul+ur),xi);
      }
      else
      {
        double dfl=f1(ul);
        double df1=f1(u1);
        assert(dfl<df1);
        if (xi<dfl)
          return ul;
        else if (xi>df1) {
          double f1=f(u1);
          double fr=f(ur);
          double s=(f1-fr)/(u1-ur);
          if (xi<s) return u1;
          else return ur;
        }
        else
          return Newton(0.5*(ul+u1),xi);
      }
    }
    else if (ul>ur)
    {
      if (ul<u2)
      {
        double fl=f(ul);
        double fr=f(ur);
        double s=(fl-fr)/(ul-ur);
        if (xi<s)
          return ul;
        else
          return ur;
      }
      else if (ur>u2)
      {
        double dfl=f1(ul);
        double dfr=f1(ur);
        assert(dfl<dfr);
        if (xi<dfl)
          return ul;
        else if (xi>dfr)
          return ur;
        else
          return Newton(0.5*(ul+ur),xi);
      }
      else
      {
        double dfl=f1(ul);
        double df2=f1(u2);
        assert(dfl<df2);
        if (xi<dfl)
          return ul;
        else if (xi>df2)
        {
          double f2=f(u2);
          double fr=f(ur);
          double s=(f2-fr)/(u2-ur);
          if (xi<s)
            return u2;
          else
            return ur;
        }
        else
          return Newton(0.5*(ul+u2),xi);
      }
    }
    else return ul;
  }

  RangeType exact ( const DomainType &arg, const double t ) const
  {
    /*
    if (t>0.01 && t<endtime()*0.9) {
      std::cerr << "calling solution between t0,t1!" << std::endl;
      abort();
    }
    */
    RangeType res = 0.;
    if (flag_ == 1)
    {
      return res;
    }
    if (flag_ == 2) {
      DomainType x(arg);
      x[1] -= velo()*t;
      // x[0] += 0.4*x[1];
      if (fabs(x[1]+0.35)<0.4) {
        double x1d = x[0]+0.075;
        double y1d = x[1]+0.35;
        if (y1d>0) x1d-=0.4*y1d;
        else x1d+=0.4*y1d;
        // double uminus = 0.5*(1.-cos(M_PI*y1d/0.5));
        double uminus = 1.;
        if (y1d<=0 && y1d>-0.4)
          uminus = 0.5*(1.-cos(M_PI*y1d/0.4))*pow(cos(M_PI*y1d/0.4*1.75),2.);
        else if (y1d>=0 && y1d<0.4)
          uminus = 0.5*(1.-cos(M_PI*y1d/0.4))*pow(cos(M_PI*y1d/0.4*3.5),2.);
        if (x1d>0.25) {
          res[0]=rp_sol(uminus,1.,t,x1d-0.3);
          double test = rp_sol(1.,uminus,t,x1d+0.8);
          if (fabs(test-uminus)>1e-5) {
            std::cerr << "ERROR in LSG: (t,x1d,y1d): " << t << " "
                      << x1d << " " << y1d << " right RP to fast" << " "
                      << uminus << " " << test << " " << res[0] << std::endl;
          }
        }
        else {
          res[0]=rp_sol(1.,uminus,t,x1d+0.8);
          double test = rp_sol(uminus,1.,t,x1d-0.3);
          if (fabs(test-uminus)>1e-5) {
            std::cerr << "ERROR in LSG: (t,x1d,y1d): " << t << " "
                      << x1d << " " << y1d << " left RP to fast" << " "
                      << uminus << " " << test << " " << res[0] << std::endl;
          }
        }
      } else res[0] = 1.;
    }
    else {
      double x = arg[0];
      //if (x<0.5)
        res[0]=rp_sol(1.,0.,t,x);
      //else
      //  res[0]=rp_sol(0.,1.,t,x-0.6);
    }

    return res;
  }

  //! \copydoc ProblemData::initial
  RangeType initial ( const DomainType &arg ) const
  {
    /*
    DomainType x(0.6);
    if (flag_ == 1) {
      RangeType res( 0 );
      x -= arg;
      x[1] *= 0.66;
      double r = x.two_norm();
      if (r<0.3)
        res = 0.;
      else
        res = 1.;
      return res;
    }
    else
    */
      return exact( arg, 0.0 );
  }

//! \copydoc ProblemData::endTime
  double endTime () const
  {
    return Dune::Fem::Parameter::getValue("finitevolume.endtime", double(.45) );
  }

/*********************************************************/
/***                 NEW FOR LESSON 1                  ***/
/*********************************************************/
  //! \copydoc ProblemData::refineTol
  double refineTol () const
  {
    return 0.1;
  }

 /*********************************************************/

protected:
  DomainType velocity_;
  int flag_;
  //using BaseType::velocity_;
};

// BuckleyLeverettProblem
// ----------------

/** \class BuckleyLeverettProblem
 *  \brief description of a transport problem
 *
 *  This class describes the following transport problem:
 *  \f{eqnarray*}
 *  \partial_t c + \nabla \cdot (v c)
 *    &=& 0 \quad\mbox{in $\Omega \times ]0,T[$}\\
 *  c &=& g \quad\mbox{on $\Gamma_{\mathrm{in}}$}\\
 *  c &=& c_0 \quad\mbox{on $\Omega \times \lbrace 0 \rbrace$}
 *  \f}
 */
template< class DomainField, int dimD >
struct BuckleyLeverettModel
{
  typedef BuckleyLeverettModelTraits< DomainField, dimD > Traits;
  typedef typename Traits::FunctionSpaceType FunctionSpaceType;
  typedef BuckleyLeverettProblemData< FunctionSpaceType > ProblemType;

  static const int dimDomain = ProblemType::dimDomain;
  static const int dimRange = ProblemType::dimRange;

  typedef typename ProblemType::DomainType DomainType;
  typedef typename ProblemType::RangeType RangeType;

  /** \brief constructor
   */
  BuckleyLeverettModel ( )
  {
    problem_.reset( new BuckleyLeverettProblemData1< FunctionSpaceType >() );
  }

  /** \brief obtain problem */
  const ProblemType &problem () const
  {
    return *problem_;
  }

  bool fluxReconstruction () const { return false; }

  RangeType reactionTerm( const double time, const DomainType &xGlobal, const RangeType& u ) const
  {
    RangeType source ( 0 );
    //source[ 0 ] = -0.1 * u[ 0 ] * u[ 1 ];
    //source[ 0 ] = 0;
    //if( source.dimension > 1 )
    //  source[ 1 ] = -source[ 0 ];
    return source;
  }

  /** \brief evaluate the numerical flux on an intersection
   *
   *  \param[in]   normal   scaled normal of the intersection
   *  \param[in]   time     current time
   *  \param[in]   xGlobal  evaluation point in global coordinates
   *  \param[in]   uLeft    value of the solution in the inside entity
   *  \param[in]   uRight   value of the solution in the outside entity
   *  \param[out]  flux     numercial flux
   *
   *  \returns the maximum wave speed
   */
  double numericalFlux ( const DomainType &normal,
                         const double time,
                         const DomainType &xGlobal,
                         const RangeType &uLeft, const RangeType &uRight,
                         RangeType &flux ) const
  {
    double upwind;
    DomainType vel(0);
    problem_->velocity( xGlobal, time, uLeft, vel );

    upwind = normal*vel;
    if (upwind>0){
      vel[0]   = problem_->f(uLeft[0]);
      flux[0] = normal*vel;
      vel[0]   = problem_->f1(uLeft[0]);
      upwind   = normal*vel;
    }
    else{
      vel[0]   = problem_->f(uRight[0]);
      flux[0] = normal*vel;
      vel[0]   = problem_->f1(uRight[0]);
      upwind   = normal*vel;
    }
    double wave = std::abs( upwind );
    vel[0] = problem_->f1(0.5*(uLeft[0]+uRight[0]));
    double wavem = std::abs(normal*vel);
    wave = (wave>wavem)?wave:wavem;
    return wave; // +model_.tstep_eps;
  }

  /** \brief evaluate the numerical flux on a boundary
   *
   *  \param[in]   bndId    boundary id
   *  \param[in]   normal   scaled normal of the boundary
   *  \param[in]   time     current time
   *  \param[in]   xGlobal  evaluation point in global coordinates
   *  \param[in]   uLeft    value of the solution in the inside entity
   *  \param[out]  flux     numercial flux
   *
   *  \returns the maximum wave speed
   */
  double boundaryFlux ( const int bndId,
                        const DomainType &normal,
                        const double time,
                        const DomainType &xGlobal,
                        const RangeType& uLeft,
                        RangeType &flux ) const
  {
    RangeType uRight = problem().boundaryValue( xGlobal, time );
    return numericalFlux( normal, time, xGlobal, uLeft, uRight, flux );
  }

/*********************************************************/
/***                 NEW FOR LESSON 1                  ***/
/*********************************************************/
  /** \brief compute adaptation indicator at intersection
   *
   *  \param[in]   normal   scaled normal of the intersection
   *  \param[in]   time     current time
   *  \param[in]   xGlobal  evaluation point in global coordinates
   *  \param[in]   uLeft    value of the solution in the inside entity
   *  \param[in]   uRight   value of the solution in the outside entity
   *
   *  \return value of indicator
   */
  double indicator ( const DomainType &normal,
                     const double time,
                     const DomainType &xGlobal,
                     const RangeType &uLeft, const RangeType &uRight) const
  {
    return problem().adaptationIndicator( uLeft, uRight );
  }

  /** \brief compute adaptation indicator at boundary
   *
   *  \param[in]   bndId    boundary id
   *  \param[in]   normal   scaled normal of the intersection
   *  \param[in]   time     current time
   *  \param[in]   xGlobal  evaluation point in global coordinates
   *  \param[in]   uLeft    value of the solution in the inside entity
   *
   *  \return value of indicator
   */
  double boundaryIndicator ( const int bndId,
                             const DomainType &normal,
                             const double time,
                             const DomainType &xGlobal,
                             const RangeType& uLeft) const
  {
    return indicator( normal,time,xGlobal, uLeft, problem().boundaryValue(xGlobal,time) );
  }
/*********************************************************/

protected:
  std::unique_ptr< ProblemType > problem_;

}; // end class BuckleyLeverettProblem
#endif // PROBLEM_TRANSPORT_HH
