#ifndef PROBLEM_TRANSPORT_HH
#define PROBLEM_TRANSPORT_HH

// C++ includes
#include <iostream>
#include <sstream>

// dune-common includes
#include <dune/common/fvector.hh>

// dune-fem includes
#include <dune/fem/space/common/functionspace.hh>

// local includes
#include "problem.hh"

// TransportModelTraits
// --------------------

template< class DomainField, int dimD >
struct TransportModelTraits
{
  const static int dimDomain = dimD;
  const static int dimRange = 1;

  typedef DomainField DomainFieldType;
  typedef double RangeFieldType;

  typedef Dune::Fem::FunctionSpace< DomainFieldType, RangeFieldType, dimDomain, dimRange > FunctionSpaceType;
};

/** \class TransportProblemData
 *  \brief basic problem data for TransportProblem
 *
 *  This class defines the data for the following transport problem:
 *  \f{eqnarray*}
 *  \partial_t c + \nabla \cdot (v c)
 *    &=& 0 \quad\mbox{in $\Omega \times ]0,T[$}\\
 *  c &=& g \quad\mbox{on $\Gamma_{\mathrm{in}}$}\\
 *  c &=& c_0 \quad\mbox{on $\Omega \times \lbrace 0 \rbrace$}
 *  \f}
 *
 *  We define the following parameters:
 *  - a grid for the domain \f$\Omega\f$,
 *  - the (constant) velocity \f$v\f$,
 *  - the boundary data \f$b\f$,
 *  - the initial data \f$c_0\f$.
 */
template< class FunctionSpace >
class TransportProblemData
: public ProblemData< FunctionSpace >
{
  typedef ProblemData< FunctionSpace > BaseType;

public:
  static const int dimDomain = BaseType::dimDomain;
  static const int dimRange  = BaseType::dimRange;

  typedef typename BaseType::DomainType DomainType;
  typedef typename BaseType::RangeType RangeType;

  /** \brief constructor */
  TransportProblemData ()
  //: velocity_( 0 )
  {}

  /** \brief obtain the (constant) velocity for the transport problem */
 /* const DomainType &velocity () const
  {
    return velocity_;
  }
*/


  virtual void velocity ( const DomainType &x, double time, const RangeType& u, DomainType& v ) const
  {
    v = DomainType(0);
  }


  std::string gridFile ( const std::string &path ) const
  {
    std::stringstream key;
    key << "fem.io.macroGridFile_" << dimDomain << "d";
    return Dune::Fem::Parameter::getValue<std::string>(key.str());
  }

  /** \brief evaluate the exact solution
   *
   *  \param[in]  x     coordinate to evaluate the exact solution in
   *  \param[in]  time  time to evaluate exact solution at
   *
   *  \returns the evaluated exact solution
   */
  virtual RangeType exact ( const DomainType &x, double time ) const
  {
    DomainType x0( x );
    DomainType v( 0 );
    x0.axpy( -time, v );
    return initial( x0 );

  }

  virtual bool physical( const RangeType &value ) const
  {
    if( value[ 0 ] < 0.0 || value[1] > 1.0 ) return false;
    return true;
  }

  //! \copydoc ProblemData::boundaryValue
  RangeType boundaryValue ( const DomainType &x, double time ) const
  {
    return exact( x, time );

  }

  // note: we are overloading one method evaluate and need to tell
  //       the compiler that we what the inherit the other versions of evaluate
  using BaseType::evaluate;

  //! \copydoc ProblemData::evaluate
  void evaluate ( const DomainType &x, double time, RangeType &ret ) const
  {
    ret = exact( x, time );
  }

  double saveInterval () const
  {
    return 0.05;
  }

virtual bool hasExactSolution() const
{
    return true;
}

//! \copydoc ProblemData::initial
  virtual RangeType initial ( const DomainType &x ) const
  {
    //dummy values to avoid pure virtual functions
    return RangeType( 0 );
  }

  //! \copydoc ProblemData::endTime
  virtual double endTime () const
  {
    //dummy values to avoid pure virtual functions
    return 6.28;
  }

/*********************************************************/
/***                 NEW FOR LESSON 1                  ***/
/*********************************************************/
  //! \copydoc ProblemData::refineTol
  virtual double refineTol () const
  {
    //dummy values to avoid pure virtual functions
    return 0.1;
  }

/** evaluate adaptation indicator
   *
   *  \param[in]  uLeft   value on the "left" side of the intersection
   *  \param[in]  uRight  value on the "right" side of the intersection
   *
   *  \returns a real number indicating how to adapt the grid
   */
  double adaptationIndicator ( const RangeType &uLeft, const RangeType &uRight ) const
  {
    return std::abs( uLeft[ 0 ] - uRight[ 0 ] );
  }
/*********************************************************/

protected:
 // DomainType velocity_;
};

/**
 * \brief Smooth initial data problem:
    \f$ \sin( 2\pi x\cdot x ) \f$
 */
template< class FunctionSpace >
class TransportProblemData2
: public TransportProblemData< FunctionSpace >
{
  typedef TransportProblemData< FunctionSpace > BaseType;

public:
  const static int dimDomain = BaseType::dimDomain;
  const static int dimRange = BaseType::dimRange;

  typedef typename BaseType::DomainType DomainType;
  typedef typename BaseType::RangeType RangeType;

  TransportProblemData2 ()
  {
      //velocity_ = DomainType( 0 );
      //velocity_ [0] = 1;

  }

  virtual void velocity ( const DomainType &x, double time, const RangeType& u, DomainType& v ) const
  {
    //v = velocity_;
    double angular_velocity = 1.0;
    DomainType rotation_centre = DomainType(0.5);
    DomainType radius = x - rotation_centre;
    v[0] = -angular_velocity*radius[1];
    v[1] = angular_velocity*radius[0];

  }

  RangeType exact ( const DomainType &x, double time ) const
  {
    DomainType x0( x );
    x0 -= .5;

    // compute element error
    const double angle = time;
    Dune::FieldMatrix< double, DomainType::dimension, DomainType::dimension > rotation( 0 );
    for( int i=0; i<DomainType::dimension; ++i ) rotation[i][i] = 1.0;
    rotation[ 0 ][ 0 ] =  std::cos( angle );
    rotation[ 0 ][ 1 ] =  std::sin( angle );
    rotation[ 1 ][ 0 ] = -std::sin( angle );
    rotation[ 1 ][ 1 ] =  std::cos( angle );

    DomainType x1;
    rotation.mv( x0, x1 );
    x1 += .5;

    return initial( x1 );
  }

  //! \copydoc ProblemData::initial
  RangeType initial ( const DomainType &x ) const
  {
    //return RangeType(sin( 2 * M_PI * (x*x) ));

      RangeType res(0) ;

        DomainType c1( 0.5 ), c2( 0.5 ), c3( 0.5 ), c4( 0.5 );
        c1[ 0 ] = 0.5;  c1[ 1 ] = 0.75;
        c2[ 0 ] = 0.5;  c2[ 1 ] = 0.25;
        c3[ 0 ] = 0.25; c3[ 1 ] = 0.5;
        c4[ 0 ] = 0.35; c4[ 1 ] = 0.65;
        const double r = 0.15;

     /*   //ball test case
        if( (x - c4).two_norm() < r )
            return RangeType( 1.0 );
        else
            return RangeType( 0.0 );

        //cube
        if ( (x[0]>=0.5 && x[0]<=0.75) && (x[1]>=0.25 && x[1]<=0.5) && (x[2]>=0.25 && x[2]<=0.5) )
            return RangeType{ 1.0 };
        else
            return RangeType{ 0.0 };



        // packman
              if( (x - c1).two_norm() < r )
              {
                if( x[ 0 ] - c1[ 0 ] >= - std::abs(x[ 1 ] - c1[ 1 ]) )
                  res = 1.0;
                else
                  res = 0.0;
              }
              else
              {
                  res = 0.0;
              }
             return res;
*/
             // res[1] = 1 - res[0];
     // slotted cylinder
        if( (x - c1).two_norm() < r )
        {
          if( (std::abs( x[ 0 ] - c1[ 0 ] ) >= 0.025) || (x[ 1 ] >= c1[ 1 ] + 0.1) )
            return RangeType{ 1.0 };
          else
            return RangeType{ 0.0 };
        }

     // cone
        if( (x - c2).two_norm() < r )
          return RangeType{ (r - (x - c2).two_norm()) / r };

        // hump
        if( (x - c3).two_norm() < r )
          return RangeType{ (1.0 + std::cos( M_PI * (x - c3).two_norm() / r )) / 4.0 };

        // default
        return RangeType( 0.0 );

      }

  //! \copydoc ProblemData::endTime
  double endTime () const
  {
    return 1.5708;
  }

/*********************************************************/
/***                 NEW FOR LESSON 1                  ***/
/*********************************************************/
  //! \copydoc ProblemData::refineTol
  double refineTol () const
  {
    return 0.1;
  }
/*********************************************************/

protected:
 // using BaseType::velocity_;
};

/**
 * \brief Discontinuous initial data problem: characteristic function for
    \f$ \{ x\colon |x| < \frac{1}{2} \} \f$
 */
template< class FunctionSpace >
class TransportProblemData1
: public TransportProblemData< FunctionSpace >
{
  typedef TransportProblemData< FunctionSpace > BaseType;

public:
  const static int dimDomain = BaseType::dimDomain;
  const static int dimRange = BaseType::dimRange;

  typedef typename BaseType::DomainType DomainType;
  typedef typename BaseType::RangeType RangeType;

  TransportProblemData1 ()
  {
    velocity_ = DomainType( 0 );
    velocity_[ 0 ] = 1;// 000.0;
  }

  virtual void velocity ( const DomainType &x, double time, const RangeType& u, DomainType& v ) const
  {
    v = velocity_;
  }

  RangeType exact ( const DomainType &x, double time ) const

{
    DomainType x0( x );
    x0.axpy( -time, velocity_ );
    return initial( x0 );

  }

  //! \copydoc ProblemData::initial
  RangeType initial ( const DomainType &x ) const
  {
      if( x[0] <= 0.0 )
      //if( x[0] <= 458000 )
          return RangeType( 1.0 );
      else
          return RangeType( 0.0 );

      //default
      return RangeType( 0.0 );

  }

//! \copydoc ProblemData::endTime
  double endTime () const
  {
    return Dune::Fem::Parameter::getValue("finitevolume.endtime", double(.5) );
  }

/*********************************************************/
/***                 NEW FOR LESSON 1                  ***/
/*********************************************************/
  //! \copydoc ProblemData::refineTol
  double refineTol () const
  {
    return 0.1;
  }

 /*********************************************************/

protected:
  DomainType velocity_;
  //using BaseType::velocity_;
};
/**
 * \brief Discontinuous initial data problem: characteristic function for
    \f$ \{ x\colon |x| < \frac{1}{2} \} \f$
 */
template< class FunctionSpace >
class TransportProblemData3
: public TransportProblemData< FunctionSpace >
{
  typedef TransportProblemData< FunctionSpace > BaseType;

public:
  const static int dimDomain = BaseType::dimDomain;
  const static int dimRange = BaseType::dimRange;

  typedef typename BaseType::DomainType DomainType;
  typedef typename BaseType::RangeType RangeType;

  TransportProblemData3 ()
  {
    velocity_ = DomainType( 0 );
    velocity_[ 0 ] = 1;// 000.0;
  }

  virtual void velocity ( const DomainType &x, double time, const RangeType& u, DomainType& v ) const
  {
    v = velocity_;
  }

  RangeType exact ( const DomainType &x, double time ) const
  {
    DomainType x0( x );
    x0.axpy( -time, velocity_ );
    return initial( x0 );

  }

  //! \copydoc ProblemData::initial
  RangeType initial ( const DomainType &x ) const
  {
    return RangeType( std::exp( - std::pow( (5.*x[0]+3.), 2.0) ));
  }

//! \copydoc ProblemData::endTime
  double endTime () const
  {
    return Dune::Fem::Parameter::getValue("finitevolume.endtime", double(.5) );
  }

/*********************************************************/
/***                 NEW FOR LESSON 1                  ***/
/*********************************************************/
  //! \copydoc ProblemData::refineTol
  double refineTol () const
  {
    return 0.1;
  }

 /*********************************************************/

protected:
  DomainType velocity_;
  //using BaseType::velocity_;
};

// TransportProblem
// ----------------

/** \class TransportProblem
 *  \brief description of a transport problem
 *
 *  This class describes the following transport problem:
 *  \f{eqnarray*}
 *  \partial_t c + \nabla \cdot (v c)
 *    &=& 0 \quad\mbox{in $\Omega \times ]0,T[$}\\
 *  c &=& g \quad\mbox{on $\Gamma_{\mathrm{in}}$}\\
 *  c &=& c_0 \quad\mbox{on $\Omega \times \lbrace 0 \rbrace$}
 *  \f}
 */
template< class DomainField, int dimD >
struct TransportModel
{
  typedef TransportModelTraits< DomainField, dimD > Traits;
  typedef typename Traits::FunctionSpaceType FunctionSpaceType;
  typedef TransportProblemData< FunctionSpaceType > ProblemType;

  static const int dimDomain = ProblemType::dimDomain;
  static const int dimRange = ProblemType::dimRange;

  typedef typename ProblemType::DomainType DomainType;
  typedef typename ProblemType::RangeType RangeType;

  /** \brief constructor
   */
  TransportModel ( )
  {
    int problem = Dune::Fem::Parameter::getValue<int>("finitevolume.problem");
    switch( problem )
    {
    case 1:
      problem_ = new TransportProblemData1< FunctionSpaceType >();
      break;
    case 2:
      problem_ = new TransportProblemData2< FunctionSpaceType >();
      break;
    case 3:
      problem_ = new TransportProblemData3< FunctionSpaceType >();
      break;
    default:
      std::cerr << "Problem " << problem << " does not exists." << std::endl;
      exit( 1 );
    }
  }

  bool fluxReconstruction () const { return false; }

  /** \brief destructor
   */
  ~TransportModel ()
  {
    delete problem_;
  }

  /** \brief obtain problem */
  const ProblemType &problem () const
  {
    return *problem_;
  }

  RangeType reactionTerm( const double time, const DomainType &xGlobal, const RangeType& u ) const
  {
    RangeType source ( 0 );
    //source[ 0 ] = -0.1 * u[ 0 ] * u[ 1 ];
    source[ 0 ] = 0;
    if( source.dimension > 1 )
      source[ 1 ] = -source[ 0 ];
    return source;
  }

  /** \brief evaluate the numerical flux on an intersection
   *
   *  \param[in]   normal   scaled normal of the intersection
   *  \param[in]   time     current time
   *  \param[in]   xGlobal  evaluation point in global coordinates
   *  \param[in]   uLeft    value of the solution in the inside entity
   *  \param[in]   uRight   value of the solution in the outside entity
   *  \param[out]  flux     numercial flux
   *
   *  \returns the maximum wave speed
   */
  double numericalFlux ( const DomainType &normal,
                         const double time,
                         const DomainType &xGlobal,
                         const RangeType &uLeft, const RangeType &uRight,
                         RangeType &flux ) const
  {
    DomainType v;
    problem().velocity(xGlobal, time, uLeft, v);
    const double upwind = normal * v;
    if( upwind > 0 )
    {
      flux = uLeft;
    }
    else
    {
      flux = uRight;
    }
    flux *= upwind;
    return std::abs( upwind );
  }

  /** \brief evaluate the numerical flux on a boundary
   *
   *  \param[in]   bndId    boundary id
   *  \param[in]   normal   scaled normal of the boundary
   *  \param[in]   time     current time
   *  \param[in]   xGlobal  evaluation point in global coordinates
   *  \param[in]   uLeft    value of the solution in the inside entity
   *  \param[out]  flux     numercial flux
   *
   *  \returns the maximum wave speed
   */
  double boundaryFlux ( const int bndId,
                        const DomainType &normal,
                        const double time,
                        const DomainType &xGlobal,
                        const RangeType& uLeft,
                        RangeType &flux ) const
  {
    RangeType uRight = problem().boundaryValue( xGlobal, time );
    return numericalFlux( normal, time, xGlobal, uLeft, uRight, flux );
  }

/*********************************************************/
/***                 NEW FOR LESSON 1                  ***/
/*********************************************************/
  /** \brief compute adaptation indicator at intersection
   *
   *  \param[in]   normal   scaled normal of the intersection
   *  \param[in]   time     current time
   *  \param[in]   xGlobal  evaluation point in global coordinates
   *  \param[in]   uLeft    value of the solution in the inside entity
   *  \param[in]   uRight   value of the solution in the outside entity
   *
   *  \return value of indicator
   */
  double indicator ( const DomainType &normal,
                     const double time,
                     const DomainType &xGlobal,
                     const RangeType &uLeft, const RangeType &uRight) const
  {
    return problem().adaptationIndicator( uLeft, uRight );
  }

  /** \brief compute adaptation indicator at boundary
   *
   *  \param[in]   bndId    boundary id
   *  \param[in]   normal   scaled normal of the intersection
   *  \param[in]   time     current time
   *  \param[in]   xGlobal  evaluation point in global coordinates
   *  \param[in]   uLeft    value of the solution in the inside entity
   *
   *  \return value of indicator
   */
  double boundaryIndicator ( const int bndId,
                             const DomainType &normal,
                             const double time,
                             const DomainType &xGlobal,
                             const RangeType& uLeft) const
  {
    return indicator( normal,time,xGlobal, uLeft, problem().boundaryValue(xGlobal,time) );
  }
/*********************************************************/

private:
  ProblemType  *problem_;
}; // end class TransportProblem
#endif // PROBLEM_TRANSPORT_HH
