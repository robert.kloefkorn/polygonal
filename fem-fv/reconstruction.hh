#ifndef DUNE_FEM_LIMITER_HH
#define DUNE_FEM_LIMITER_HH

#include <dune/fem/space/finitevolume.hh>
#include <dune/fem/io/parameter.hh>

#include <dune/fv/leastsquaresreconstruction.hh>
#include "limiterutility.hh"

#include <dune/fv/lpreconstruction.hh>
#include <dune/fv/qpreconstruction.hh>

namespace Dune
{
namespace Fem
{

  /**
   * \brief Limited reconstruction.
   *
   * \ingroup PassBased
   */
  template <class Model, class DiscreteFunction>
  class LimitedReconstruction
  {
    typedef LimitedReconstruction< Model, DiscreteFunction > ThisType;
  public:
    typedef DiscreteFunction  DiscreteFunctionType;
    typedef typename DiscreteFunctionType :: DiscreteFunctionSpaceType
      DiscreteFunctionSpaceType;

    typedef typename DiscreteFunctionSpaceType :: FunctionSpaceType  FunctionSpaceType;
    typedef typename FunctionSpaceType :: DomainType DomainType;
    typedef typename FunctionSpaceType :: RangeType  RangeType;

    typedef typename DiscreteFunctionSpaceType :: GridPartType       GridPartType;
    typedef typename GridPartType :: GridViewType                    GridViewType;
    typedef typename GridPartType :: GridType                        GridType;

    typedef LimiterUtility< FunctionSpaceType, GridType :: dimension >      LimiterUtilityType;
    typedef typename LimiterUtilityType :: GradientType      GradientType;
    typedef typename LimiterUtilityType :: CheckType         CheckType;
    typedef typename LimiterUtilityType :: ComboSetType      ComboSetType;
    typedef typename LimiterUtilityType :: KeyType           KeyType;

    typedef std::map< int, ComboSetType > ComboSetMapType ;

    typedef typename Model :: Traits :: LimiterFunctionType  LimiterFunctionType;

    typedef typename DiscreteFunctionSpaceType :: EntityType         EntityType;
    typedef typename EntityType :: Geometry                          Geometry;

    typedef typename LimiterUtilityType::MatrixStorage MatrixCacheEntry;
    typedef std::map< KeyType, MatrixCacheEntry > MatrixCacheType;

    static const bool StructuredGrid     = GridPartCapabilities::isCartesian< GridPartType >::v;

    //! type of cartesian grid checker
    typedef CheckCartesian< GridPartType >  CheckCartesianType;

    enum ReconstructionScheme
    {
      firstOrder = 0, // first order scheme
      secondRecon = 1, // 2nd order selected reconstruction
      secondLeast = 2, // 2nd order least squares
      secondLP    = 3,  // 2nd order linear programming
      secondQP    = 4   // 2nd order quadratic programming
    };

    static ReconstructionScheme getReconstructionSchemeId()
    {
      static const std::string reconstructions[] = { "1st", "2nd-R", "2nd-LS", "2nd-LP", "2nd-QP" };
      return (ReconstructionScheme) Dune::Fem::Parameter::getEnum( "finitevolume.reconstruction", reconstructions );
    }

    static const std::string& schemeName (const int scheme)
    {
      static const std::string reconstructions[] = { "1st", "2nd-R", "2nd-LS", "2nd-LP", "2nd-QP" };
      return reconstructions[ scheme ];
    }

    struct BoundaryValue
    {
      const ThisType& op_;
      BoundaryValue( const ThisType& op ) : op_( op ) {}

      RangeType operator () ( const typename GridViewType::Intersection &i,
                              const DomainType &x,
                              const DomainType &n,
                              const RangeType &uIn ) const
      {
        return uIn;
        return op_.model().problem().boundaryValue( x, op_.time() );
      }
    };

    typedef Dune::FV::LimitedLeastSquaresReconstruction< GridViewType, RangeType, BoundaryValue > LSRecon;
#if HAVE_DUNE_OPTIM
    typedef Dune::FV::LPReconstruction< GridViewType, RangeType, BoundaryValue > LinearProgramming;
    typedef Dune::FV::QPReconstruction< GridViewType, RangeType, BoundaryValue > QuadraticProgramming;
#else
    typedef LSRecon LinearProgramming;
    typedef LSRecon QuadraticProgramming;
#endif

  public:
    LimitedReconstruction( const Model& model, const DiscreteFunctionSpaceType& space )
      : space_( space )
      , gridPart_( space_.gridPart() )
      , model_( model )
      , lsRecon_( static_cast< GridViewType > (gridPart_), BoundaryValue( *this ) )
      , linProg_( static_cast< GridViewType > (gridPart_), BoundaryValue( *this )
#if HAVE_DUNE_OPTIM
                  , Dune::Fem::Parameter::getValue<double>("finitevolume.linearprogramming.tol", 1e-8 )
#endif
        )
      , quadProg_( static_cast< GridViewType > (gridPart_), BoundaryValue( *this )
#if HAVE_DUNE_OPTIM
                  , Dune::Fem::Parameter::getValue<double>("finitevolume.linearprogramming.tol", 1e-8 )
#endif
                 )
      , limiterFunction_()
      , storedComboSets_()
      , matrixCacheVec_( gridPart_.grid().maxLevel() + 1 )
      , cartesianGrid_( CheckCartesianType::check( gridPart_ ) )
      , sequence_( -1 )
    {
      time_ = 0;
    }

    const Model& model () const { return model_; }

    double time () const { return time_; }

    //! calculate internal reconstruction based on mean values of U
    void update( const double time,
                 const DiscreteFunctionType& U,
                 const ReconstructionScheme& schemeId,
                 const bool recompute = true )
    {
      // set time
      time_ = time;

      const size_t size = gridPart_.indexSet().size( 0 ) ;
      if( sequence_ != space_.sequence() )
      {
        // resize gradient vector
        values_.resize( size );
        centers_.resize( size );
        gradients_.resize( size );
        numbers_.resize( size );
        factor_.resize( size );

        const auto end = gridPart_.template end< 0, Dune::Interior_Partition >();
        for( auto it = gridPart_.template begin< 0, Dune::Interior_Partition >(); it != end; ++it )
        {
          const auto& entity = *it ;
          const unsigned int entityIndex = U.space().gridPart().indexSet().index( entity );
          centers_[ entityIndex ] = entity.geometry().center() ;
        }
        sequence_ = space_.sequence();
      }

      if( schemeId == secondLeast )
      {
        // helper class for evaluation of average value of discrete function
        EvalAverage average( U );

        const auto end = gridPart_.template end< 0, Dune::Interior_Partition >();
        for( auto it = gridPart_.template begin< 0, Dune::Interior_Partition >(); it != end; ++it )
        {
          const auto& entity = *it ;
          const unsigned int entityIndex = U.space().gridPart().indexSet().index( entity );
          average.evaluate( entity, values_[ entityIndex ] );
        }

        lsRecon_( gridPart_.indexSet(), values_, gradients_, factor_, recompute );
      }
      else if( schemeId == secondLP )
      {
#if HAVE_DUNE_OPTIM
        // helper class for evaluation of average value of discrete function
        EvalAverage average( U );

        const auto end = gridPart_.template end< 0, Dune::Interior_Partition >();
        for( auto it = gridPart_.template begin< 0, Dune::Interior_Partition >(); it != end; ++it )
        {
          const auto& entity = *it ;
          const unsigned int entityIndex = U.space().gridPart().indexSet().index( entity );
          average.evaluate( entity, values_[ entityIndex ] );
        }

        linProg_( gridPart_.indexSet(), values_, gradients_ ); //, factor_, reco);
#else
        std::cerr << "dune-optim needed for linear programming reconstruction" << std::endl;
        std::abort();
#endif
      }
      else if( schemeId == secondQP )
      {
#if HAVE_DUNE_OPTIM
        // helper class for evaluation of average value of discrete function
        EvalAverage average( U );

        const auto end = gridPart_.template end< 0, Dune::Interior_Partition >();
        for( auto it = gridPart_.template begin< 0, Dune::Interior_Partition >(); it != end; ++it )
        {
          const auto& entity = *it ;
          const unsigned int entityIndex = U.space().gridPart().indexSet().index( entity );
          average.evaluate( entity, values_[ entityIndex ] );
        }

        quadProg_( gridPart_.indexSet(), values_, gradients_ ); //, factor_, reco);
#else
        std::cerr << "dune-optim needed for quadratic programming reconstruction" << std::endl;
        std::abort();
#endif
      }
      else
      {
        assert( schemeId == secondRecon );
        if( recompute )
        {
          for( size_t i = 0; i<size; ++i ) numbers_[ i ].clear();
        }

        const auto end = gridPart_.template end< 0, Dune::Interior_Partition >();
        for( auto it = gridPart_.template begin< 0, Dune::Interior_Partition >(); it != end; ++it )
        {
          applyLocal( *it, U, recompute );
        }
      }
    }

  private:
    LimitedReconstruction( const LimitedReconstruction& );

  protected:
    struct CheckPhysical
    {
      std::vector< DomainType > points_;
      const Model& model_;
      const RangeType& value_;

      CheckPhysical( const Model& model,
                     const Geometry& geometry,
                     const RangeType& entityValue,
                     const DomainType& entityCenter )
        : points_( geometry.corners() ),
          model_( model ),
          value_( entityValue )
      {
        const int nCorners = points_.size();
        for( int i=0; i<nCorners; ++i )
        {
          points_[ i ]  = geometry.corner( i );
          points_[ i ] -= entityCenter;
        }
      }

      bool operator() ( const int r, const DomainType& gradient ) const
      {
        const int nCorners = points_.size();
        for( int i=0; i<nCorners; ++i )
        {
          const double functionValue = value_[ r ] + ( points_[ i ] * gradient );
          if( ! model_.physical( functionValue ) )
            return false ;
        }
        return true;
      }
    };

    struct EvalAverage {
      static const int dimRange = DiscreteFunctionSpaceType::dimRange;
      const DiscreteFunctionType& U_;
      typedef typename DiscreteFunctionType :: LocalFunctionType LocalFunctionType;
      EvalAverage( const DiscreteFunctionType& U )
        : U_( U )
      {}

      bool evaluate( const EntityType& entity, RangeType& value ) const
      {
        const LocalFunctionType& uEn = U_.localFunction( entity );
        for( int i=0; i<dimRange; ++i )
        {
          value[ i ] = uEn[ i ];
        }
        return true;
      }

      template <class IntersectionType, class IntersectionGeometry>
      bool boundaryValue( const EntityType& entity,
                          const IntersectionType& intersection,
                          const IntersectionGeometry& interGeo,
                          const DomainType& globalPoint,
                          const RangeType& entityValue,
                          RangeType& neighborValue ) const
      {
        return false;
      }
    };

    class LocalFunction
    {
      static const int dimRange = DiscreteFunctionSpaceType::dimRange;
      const Geometry       geometry_;
      const DomainType&    center_;
      const RangeType&     value_;
      const GradientType&  gradient_;
    public:
      LocalFunction( const EntityType& entity, const DomainType& center,
                     const RangeType& value, const GradientType& gradient )
        : geometry_( entity.geometry() ),
          center_( center ),
          value_( value ),
          gradient_( gradient )
      {}

      const Geometry& geometry () const { return geometry_; }

      template <class LocalPoint>
      void evaluate( const LocalPoint& local, RangeType& value ) const
      {
        // compute point of evaluation
        DomainType x = geometry_.global( Fem::coordinate( local ) );
        evaluateGlobal( x, value );
      }

      void evaluateGlobal( const DomainType& point, RangeType& value ) const
      {
        // compute point of evaluation
        DomainType x( point );
        x -= center_;

        // set u
        value = value_;

        for( int i=0; i<dimRange; ++i )
        {
          // f(x) = u + grad * ( x - center )
          value[ i ] += gradient_[ i ] * x;
          //std::cout<<"Gradient[i] "<<gradient_[ i ]<<std::endl;
        }
      }
    };


    void applyLocal( const EntityType& entity, const DiscreteFunctionType& U, const bool recompute )
    {
      // helper class for evaluation of average value of discrete function
      EvalAverage average( U );

      const unsigned int entityIndex = U.space().gridPart().indexSet().index( entity );

      // get geometry
      const Geometry& geo = entity.geometry();

      // cache geometry type
      const GeometryType geomType = entity.type();

      // get bary center of element
      const DomainType& entityCenter = centers_[ entityIndex ];

      // evaluate mean value on current entity
      average.evaluate( entity, values_[ entityIndex ] );

      // boundary is true if boundary segment was found
      // nonconforming is true if entity has at least one non-conforming intersections
      // cartesian is true if the grid is cartesian and no nonconforming refinement present
      typename LimiterUtilityType::Flags flags( cartesianGrid_, true );


      std::vector< DomainType > barysFull;
      std::vector< RangeType >  nbValsFull;

      // setup neighbors barycenter and mean value for all neighbors
      LimiterUtilityType::setupNeighborValues( gridPart_, entity, average, entityCenter, values_[ entityIndex ],
                                               centers_, StructuredGrid, flags, barys_, nbVals_, barysFull, nbValsFull);

      // mark entity as finished, even if not limited everything necessary was done
      //assert( entityIndex  < visited_.size() );
      //visited_[ entityIndex ] = true ;

      ComboSetType& comboSet = storedComboSets_[ nbVals_.size() ];
      if( comboSet.empty() )
      {
        // create combination set
        LimiterUtilityType::buildComboSet( nbVals_.size(), comboSet );
      }
      ComboSetType& comboSetFull = storedComboSets_[ nbValsFull.size() ];
      //std::cout << "Neigh " << nbValsFull.size() << " combinations " << comboSetFull.size() << std::endl;

      if( comboSetFull.empty() )
      {
        // create combination set
        LimiterUtilityType::buildComboSet( nbValsFull.size(), comboSetFull );
      }

      // reset values
      localGradients_.clear();
      comboVec_.clear();

      // level is only needed for Cartesian grids to access the matrix caches
      const int matrixCacheLevel = ( flags.cartesian ) ? entity.level() : 0 ;
      assert( matrixCacheLevel < (int) matrixCacheVec_.size() );
      MatrixCacheType& matrixCache = matrixCacheVec_[ matrixCacheLevel ];

      // calculate linear functions, stored in localGradients_ and comboVec_
      LimiterUtilityType::
        calculateLinearFunctions( comboSet, geomType, flags,
                                  barys_, nbVals_,
                                  matrixCache,
                                  localGradients_,
                                  comboVec_);

      // functor for checking the physicallity of reconstructed functions
      CheckPhysical checkPhysical( model_, geo, values_[ entityIndex ], entityCenter );

      std::vector< RangeType > factors;
      if( recompute )
      {
        // Limiting
        LimiterUtilityType::limitFunctions( limiterFunction_, checkPhysical,
                                            comboVec_, barysFull, nbValsFull, localGradients_, factors );
        // std::cout << "nbvals: " << nbVals_.size() << std::endl;
      }

      assert( gradients_.size() > 0 );
      // take maximum of limited functions and store in gradients
      LimiterUtilityType::getMaxFunction(localGradients_, gradients_[ entityIndex ],
                                         factor_[ entityIndex ], numbers_[ entityIndex ], factors );

      if( ! recompute )
      {
        for( int r=0; r<RangeType::dimension; ++r )
          gradients_[ entityIndex ][ r ] *= factor_[ entityIndex ][ r ];
      }
    }

  public:
    typedef LocalFunction  LocalFunctionType;

    //! return local reconstruction
    LocalFunctionType localFunction( const EntityType& entity )
    {
      const unsigned int entityIndex = gridPart_.indexSet().index( entity );
      return LocalFunctionType( entity, centers_[ entityIndex ],
                                values_[ entityIndex ], gradients_[ entityIndex ] );
    }

    //! return local reconstruction
    const LocalFunctionType localFunction( const EntityType& entity ) const
    {
      const unsigned int entityIndex = gridPart_.indexSet().index( entity );
      return LocalFunctionType( entity, centers_[ entityIndex ],
                                values_[ entityIndex ], gradients_[ entityIndex ] );
    }

  protected:
    bool checkPhysical( const LocalFunctionType& localRecon ) const
    {
      const auto& geometry = localRecon.geometry();
      const int corners = geometry.corners();
      RangeType value;
      for( int i = 0; i < corners; ++ i )
      {
        localRecon.evaluateGlobal( geometry.corner( i ), value );
        if( ! model_.problem().physical( value ) )
          return false ;
      }
      return true;
    }

    const DiscreteFunctionSpaceType& space_;
    const GridPartType& gridPart_;
    const Model& model_;

    const LSRecon lsRecon_;
    const LinearProgramming linProg_;
    const QuadraticProgramming quadProg_;

    LimiterFunctionType limiterFunction_;

    mutable ComboSetMapType storedComboSets_;

    mutable std::vector< GradientType > localGradients_;
    mutable std::vector< CheckType >    comboVec_;

    mutable std::vector< DomainType > barys_;
    mutable std::vector< RangeType >  nbVals_;
    mutable std::vector< MatrixCacheType > matrixCacheVec_;

    std::vector< RangeType >    values_;
    std::vector< DomainType >   centers_;
    std::vector< GradientType > gradients_;
    std::vector< std::vector< int > > numbers_;
    std::vector< RangeType >  factor_;

    double time_;

    const bool cartesianGrid_;
    int sequence_ ;
  };

} // end namespace Fem

} // end namespace Dune

#endif // DUNE_FEM_LIMITER_HH
