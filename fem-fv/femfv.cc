#ifndef GRIDDIM
#define GRIDDIM 3
#endif

// always include this file
#include <config.h>

// C++ includes
#include <iostream>

// dune-common includes
#include <dune/common/fvector.hh>

#include <dune/grid/common/mcmgmapper.hh>

#include <dune/grid/yaspgrid.hh>
#include <dune/grid/io/file/dgfparser/dgfyasp.hh>
#include <dune/grid/io/file/dgfparser/dgfwriter.hh>
#include <dune/fem/gridpart/common/gridpart.hh>

#if HAVE_OPM_GRID
#include <opm/grid/polyhedralgrid.hh>
#include <opm/grid/polyhedralgrid/dgfparser.hh>
#include <opm/grid/CpGrid.hpp>
#include <opm/grid/cpgrid/dgfparser.hh>
#endif

#include <dune/fem/misc/linesegmentsampler.hh>

#if HAVE_DUNE_POLYGONGRID
#include <dune/polygongrid/grid.hh>
#include <dune/polygongrid/dgf.hh>
#endif


// dune-fem includes
#include <dune/fem/function/adaptivefunction.hh>
#include <dune/fem/function/vectorfunction.hh>
#include <dune/fem/function/vectorfunction/managedvectorfunction.hh>
#include <dune/fem/function/blockvectorfunction.hh>
#include <dune/fem/function/common/localfunctionadapter.hh>
#include <dune/fem/gridpart/adaptiveleafgridpart.hh>
#include <dune/fem/io/file/dataoutput.hh>
#include <dune/fem/misc/l1norm.hh>
#include <dune/fem/misc/mpimanager.hh>
//#include <dune/fem/operator/projection/l2projection.hh>
#include <dune/fem/space/common/adaptationmanager.hh>
#include <dune/fem/space/finitevolume.hh>
#include <dune/fem/misc/gridname.hh>

#include <dune/fem/solver/rungekutta/explicit.hh>
#include <dune/fem/solver/rungekutta/implicit.hh>
#include <dune/fem/solver/odesolver.hh>

#include <dune/fem/misc/gridwidth.hh>

#if HAVE_DUNE_ALUGRID
#include <dune/alugrid/grid.hh>
#include <dune/alugrid/dgf.hh>
#endif

#if defined POLYHEDRALGRID
  typedef Dune::PolyhedralGrid< GRIDDIM, GRIDDIM > HGridType;
#elif defined CPGRID
  typedef Dune::CpGrid HGridType;
#elif defined POLYGONALGRID
  typedef Dune::PolygonGrid< double > HGridType;
#elif defined ALUGRID_SIMPLEX && HAVE_DUNE_ALUGRID
  typedef Dune::ALUGrid< GRIDDIM, GRIDDIM, Dune::simplex, Dune::nonconforming > HGridType;
#elif defined ALUGRID_CUBE && HAVE_DUNE_ALUGRID
  typedef Dune::ALUGrid< GRIDDIM, GRIDDIM, Dune::cube, Dune::nonconforming > HGridType;
#else
  typedef Dune :: YaspGrid< GRIDDIM > HGridType;
#endif

/** pde and problem description **/

/*********************************************************/
/***                 NEW FOR LESSON 1                  ***/
/*********************************************************/
#include "adaptation.hh"
/*********************************************************/

#include "fvscheme.hh"
#include "problem-euler.hh"
#include "problem.hh"
#include "problem-transport.hh"
#include "problem-bucklev.hh"
#include "problem-bucklev2.hh"

#include <ctime>

// DataOutputParameters
// --------------------

struct DataOutputParameters
: public Dune::Fem::LocalParameter< Dune::Fem::DataOutputParameters, DataOutputParameters >
{
  DataOutputParameters ( const int step )
  : step_( step )
  {}

  DataOutputParameters ( const DataOutputParameters &other )
  : step_( other.step_ )
  {}

  std::string prefix () const
  {
    std::stringstream s;
    s << "poisson-" << step_ << "-";
    return s.str();
  }

private:
  int step_;
};

// ErrorLocal
// ----------

template< class F1, class F2 >
struct ErrorLocal
{
  // discrete function space
  typedef typename F1::DiscreteFunctionSpaceType DiscreteFunctionSpaceType;
  // function space
  typedef typename DiscreteFunctionSpaceType::FunctionSpaceType FunctionSpaceType;
  // grid part type
  typedef typename DiscreteFunctionSpaceType::GridPartType GridPartType;
  // entity type
  typedef typename GridPartType::GridType::template Codim< 0 >::Entity EntityType;

  ErrorLocal( const F1 &f1, const F2 &f2 )
  : f1_(f1), f2_(f2),
    localf1_(f1), localf2_(f2)
  {}

  template< class PointType >
  void evaluate ( const PointType &x, typename F1::RangeType &val ) const
  {
    localf1_.evaluate( x, val );
    typename F2::RangeType val2;
    localf2_.evaluate( x, val2 );
    val -= val2;
  }

  void init ( const EntityType &entity )
  {
    localf1_.init( entity );
    localf2_.init( entity );
  }

private:
  const F1 &f1_;
  const F2 &f2_;
  typename F1::LocalFunctionType localf1_;
  typename F2::LocalFunctionType localf2_;
};

// algorithm
// ---------
struct EocDataOutputParameters : public Dune::Fem::DataOutputParameters
{
protected:
  std::string loop_;
public:
  EocDataOutputParameters(int loop)
  {
    std::stringstream ss;
    ss << "_" << loop << "_";
    loop_ = ss.str();
  }

  EocDataOutputParameters(const EocDataOutputParameters& other)
  : loop_(other.loop_) {}

  std::string prefix () const {
    return Dune::Fem::DataOutputParameters::prefix() + loop_;
  }

  virtual Dune::Fem::DataOutputParameters* clone() const
  {
    return new EocDataOutputParameters( *this );
  }
};

template <class Problem, class DiscreteFunctionType>
void initialize( const Problem& problem, DiscreteFunctionType& solution )
{
  typedef typename DiscreteFunctionType :: GridPartType GridPartType;
  typedef typename DiscreteFunctionType :: RangeType RangeType;
  const GridPartType& gridPart = solution.space().gridPart();

  double volume = 0 ;
  double xMin =  1e308;
  double xMax = -1e308;
  // loop over all entities
  const auto end = gridPart.template end< 0 >();
  for( auto it = gridPart.template begin< 0 >(); it != end; ++it )
  {
    const auto center = it->geometry().center();
    xMin = std::min( xMin, center[ 0 ] );
    xMax = std::max( xMax, center[ 0 ] );
  }

  std::cout << "xMin = " << xMin << "  xMax " << xMax << std::endl;

  // loop over all entities
  for( auto it = gridPart.template begin< 0 >(); it != end; ++it )
  {
    const auto &entity = *it;
    const auto &geometry = entity.geometry();
    // get barycenter (can be obtained also by geometry.center())
    RangeType value = problem.initial( geometry.center() );
    volume += geometry.volume();

 /*   const int corners = geometry.corners();
    for( int i=0; i<corners; ++i )
    {
      value += problem.initial( geometry.corner( i ) );
    }

    value /= double( corners+1 );
*/
    auto lf = solution.localFunction( entity );
    for( int i=0; i<RangeType::dimension; ++i)
    {
      lf[ i ] = value[ i ];
    }
  }

  std::cout << "Volume of domain = " << volume << std::endl;
}

template <class Problem, class DiscreteFunctionType>
double l1error( const Problem& problem, DiscreteFunctionType& solution )
{
  typedef typename DiscreteFunctionType :: GridPartType GridPartType;
  typedef typename DiscreteFunctionType :: RangeType  RangeType;
  typedef typename DiscreteFunctionType :: DomainType DomainType;
  const GridPartType& gridPart = solution.space().gridPart();

  DomainType local( 0.25 );

  double norm = 0 ;
  // loop over all entities
  const auto end = gridPart.template end< 0 >();
  for( auto it = gridPart.template begin< 0 >(); it != end; ++it )
  {
    const auto &entity = *it;
    const auto &geometry = entity.geometry();
    const DomainType center = geometry.center() ;

    // get barycenter (can be obtained also by geometry.center())
    RangeType uExact;
    problem.evaluate( geometry.center(), uExact );

    const int corners = geometry.corners();
    for( int i=0; i<corners; ++i )
    {
      RangeType tmp;
      problem.evaluate( geometry.corner( i ), tmp );
      uExact += tmp;
    }

    uExact /= double(corners+1);

    RangeType u;
    solution.localFunction( entity ).evaluate( geometry.local( center ), u );

    norm += geometry.volume() * (u - uExact).one_norm();
  }

  return norm;
}

template< class HGridType, class ModelType >
double algorithm ( HGridType &grid, const ModelType &model, int step )
{

  Dune::Timer timer;
/*********************************************************/
/***                 NEW FOR LESSON 1                  ***/
/*********************************************************/
  // step < 0 indicates the adaptive algorithm
  const bool adaptive = (step < 0);
  const int maxLevel = Dune::Fem::Parameter::getValue< int >( "finitevolume.maxlevel" );
/*********************************************************/

  //! [discrete function construction]
  // we want to solve the problem on the leaf elements of the grid
  //typedef Dune::Fem::AdaptiveLeafGridPart< HGridType > GridPartType;
  typedef Dune::Fem::LeafGridPart< HGridType > GridPartType;

  GridPartType gridPart(grid);

  // function space type
  typedef typename ModelType::FunctionSpaceType FunctionSpaceType;
  // discrete function space type
  typedef Dune::Fem::FiniteVolumeSpace< FunctionSpaceType, GridPartType, 0 > DiscreteFunctionSpaceType;
  // discrete function type
  typedef Dune::Fem::AdaptiveDiscreteFunction< DiscreteFunctionSpaceType > DiscreteFunctionType;
  //typedef std::vector< typename DiscreteFunctionSpaceType::RangeType > DofVector;
  //typedef Dune::Fem::VectorDiscreteFunction< DiscreteFunctionSpaceType, DofVector > VecDiscreteFunctionType;
  //typedef Dune::Fem::ManagedDiscreteFunction< VecDiscreteFunctionType > DiscreteFunctionType;
  //typedef Dune::Fem::ISTLBlockVectorDiscreteFunction< DiscreteFunctionSpaceType >  DiscreteFunctionType;

  typedef typename DiscreteFunctionSpaceType :: RangeType   RangeType;

  // create discrete function space
  DiscreteFunctionSpaceType space(gridPart);

  //! [choosing the time provider]
  // type of time provider organizing time for time loops
  typedef Dune::Fem::GridTimeProvider< HGridType > TimeProviderType;
  // initialize time provider
  TimeProviderType tp( 0., grid );
  //! [choosing the time provider]

  //! [choosing a finite volume scheme]
  // create finite volume solver
  typedef FiniteVolumeScheme< DiscreteFunctionType, ModelType > FVScheme;
  FVScheme scheme( space, model, tp );

  std::string solname ( "concentration" );
  if( scheme.higherOrder() )
    solname += "_2";
  // create discrete functions
  DiscreteFunctionType solution( solname, space ), update( "update", space );

  //! [discrete function construction]

/*********************************************************/
/***                 NEW FOR LESSON 1                  ***/
/*********************************************************/
  // type of restriction/prolongation projection for adaptive simulations
  typedef Dune::Fem::RestrictProlongDefault< DiscreteFunctionType >  RestrictionProlongationType;
  // type of adaptation manager
  typedef Dune::Fem::AdaptationManager< HGridType, RestrictionProlongationType > AdaptationManagerType;
/*********************************************************/

  //! [error calculation and output typedefs]
  // convert global function to grid function ...
  typedef Dune::Fem::GridFunctionAdapter< typename ModelType::ProblemType, GridPartType > GridExactSolutionType;
  typedef Dune::Fem::LocalFunctionAdapter< ErrorLocal< DiscreteFunctionType, GridExactSolutionType> > ErrorFunctionType;

  // type of input/output
  typedef std::tuple< DiscreteFunctionType*, GridExactSolutionType*, ErrorFunctionType* > IOTupleType;
  // type of the data writer
  typedef Dune::Fem::DataOutput< HGridType, IOTupleType > DataOutputType;
  //! [error calculation and output typedefs]

/*********************************************************/
/***                 NEW FOR LESSON 1                  ***/
/*********************************************************/
  // construct manager for adaptation
  RestrictionProlongationType rp( solution );
  // set refine weight
  rp.setFatherChildWeight( Dune::DGFGridInfo< HGridType >::refineWeight() );
  // create adaptation manager
  AdaptationManagerType adaptManager( grid, rp );
/*********************************************************/

  typedef DuneODE::OdeSolverInterface< DiscreteFunctionType > OdeSolverType;

  std::unique_ptr< OdeSolverType > ode;
  static const std::string odeSolver[]  = { "EX", "IM" };
  const int odeSolverType = Dune::Fem::Parameter::getEnum( "fem.ode.odesolver", odeSolver, 0 );
  int odeOrder = 1 ;
  odeOrder = Dune::Fem::Parameter::getValue("fem.ode.order", odeOrder );
  if( odeSolverType == 0 )
    ode.reset( new DuneODE::ExplicitOdeSolver< DiscreteFunctionType >( scheme, tp, odeOrder ) );
  else if ( odeSolverType == 1 )
    ode.reset( new DuneODE::ImplicitOdeSolver< DiscreteFunctionType >( scheme, tp, odeOrder ) );

  model.problem().setTime( tp.time() );
  //! [choosing a finite volume scheme]

  //! [error calculation and output]
  // error function
  GridExactSolutionType exactSolution( "exact solution", model.problem(), gridPart, space.order()+1 );
  ErrorLocal< DiscreteFunctionType, GridExactSolutionType > errorLocal( solution, exactSolution );
  ErrorFunctionType errorFunction( "error", errorLocal, gridPart );

  // data output
  IOTupleType dataTup = (model.problem().hasExactSolution()) ?
                          IOTupleType( &solution, &exactSolution, &errorFunction ) :
                          IOTupleType( &solution, nullptr, nullptr );

  EocDataOutputParameters param( step );
  DataOutputType dataOutput( grid, dataTup, tp, param );

  typedef typename GridPartType::GridViewType GridView;
  GridView gridView = static_cast< GridView > (gridPart);
  Dune::MultipleCodimMultipleGeomTypeMapper< GridView > mapper( gridView, Dune::mcmgElementLayout() );
  std::stringstream outputName;
  outputName << param.path() << "/" << param.prefix() << Dune::Fem::gridName( grid ) << "_" <<
    HGridType::dimension << "D_" << scheme.fvOrderName();
  std::cout << "output = " << outputName.str() << std::endl;

  std::vector< RangeType > u( gridPart.indexSet().size( 0 ) );

  // L2 projection operator
  //Dune::Fem::L2Projection< typename ModelType::ProblemType, DiscreteFunctionType > l2projection;//( space.order()+2 );
  //! [error calculation and output]

/*********************************************************/
/***                 NEW FOR LESSON 1                  ***/
/*********************************************************/

  // do initial grid adaptation
  /*
  if( adaptive )
  {
    for( int i = 0; i <= maxLevel; ++i )
    {
      initialize( model.problem(), solution );

      // mark grid for initial refinement
      GridMarker< HGridType > gridMarker( grid, 0, maxLevel );
      scheme.mark( 0, solution, gridMarker );

      // adapt grid
      if( gridMarker.marked() )
        adaptManager.adapt();
      std::cout << "start: " << i << " grid size: " << grid.size(0) << "  space size: " << space.size() << std::endl << std::endl;
    }
  }
  */
/*********************************************************/

  //! [Preparing the initial time step]
  // project initial values
  initialize( model.problem(), solution );

  Dune::Fem::L1Norm< GridPartType > L1norm( gridPart, 2 );
  double massIni = L1norm.norm (solution);

  // write initial values
  dataOutput.write(tp);

  //std::copy( solution.dofVector().begin(), solution.dofVector().end(), u.begin() );
  const int elSize = gridPart.indexSet().size( 0 );
  for( int i=0 ;i<elSize; ++i )
    u[ i ] = solution.dofVector()[ i ];

  //solution.dofVector() );

  // final time for simulation
  const double endTime = model.problem().endTime();

  // we always need a first time step size
  // scheme( solution, update );
  ode->initialize( solution );

  //! [Preparing the initial time step]
  const double fixedTimeStep = Dune::Fem::Parameter::getValue("fem.ode.fixedtimestep", double(0.0) )/ tp.factor();
  if ( fixedTimeStep > 1e-20 )
    tp.init( fixedTimeStep );
  else
    tp.init();

  //! [Computation of all time steps]
  // now do the time stepping
  for( ; tp.time() < endTime ;  )
  {
    const double time = tp.time();
    const double dt   = tp.deltaT();
    const int counter = tp.timeStep();

    model.problem().setTime(time);

    // apply the spatial operator
    //scheme( solution, update );
    ode->solve( solution );

    // communicate update
    //update.communicate();

    // update solution
    //solution.axpy( dt, update );

    //! [Computation of all time steps]

    // assert that no nan have been generated
    if ( !solution.dofsValid() )
    {
      std::cerr<< "time step(" << counter << "): Invalid DOFs" << std::endl;
      dataOutput.writeData(tp.time());
      abort();
    }

/*********************************************************/
/***                 NEW FOR LESSON 1                  ***/
/*********************************************************/
    if( adaptive )
    {
      // mark the grid for adaptation
      GridMarker< HGridType > gridMarker( grid, 0, maxLevel );
      scheme.mark( time, solution, gridMarker );
      // call adaptation algorithm
      if( gridMarker.marked() )
        adaptManager.adapt();
    }
/*********************************************************/

    //! [Computation of all time steps, data output]

    // write data
    dataOutput.write( tp );

    size_t elements = gridPart.indexSet().size( 0 );
    elements = gridPart.grid().comm().sum( elements );
    if( Dune::Fem::Parameter::verbose() )
    {
      // print info about time, timestep size and counter
      std::cout << "elements = " << elements;
      std::cout << "   maxLevel = " << grid.maxLevel();
      std::cout << "   step = " << counter;
      std::cout << "   time = " << time;
      std::cout << "   dt = " << dt;
      std::cout << std::endl;
    }

    // next time step is prescribed by fixedTimeStep
    if ( fixedTimeStep > 1e-20 )
      tp.next( fixedTimeStep );
    else
      tp.next();

  }
  //! [Computation of all time steps, data output]

  //! [Writing data to disc and error calculation]
  // output final resul
  dataOutput.write( tp );

  // compute the L1-error
  double error = 0.0;
  double mass = L1norm.norm (solution );
  size_t elements = gridPart.indexSet().size( 0 );
  elements = gridPart.grid().comm().sum( elements );
  std::cout << "Number of elements: " << elements << std::endl;

  if( model.problem().hasExactSolution() )
  {
    model.problem().setTime( tp.time() );
    //error = L1norm.distance( model.problem(), solution );
    error = l1error( model.problem(), solution );

    double h = Dune::Fem::GridWidth::calcGridWidth( gridPart );
    std::cout << "L1-error at final time t="<<tp.time() << ": " << error << "  h " << h << std::endl;
    std::cout << "Gnuplot: "<< h << " " << error << " " << timer.elapsed() << std::endl;
  }

  {
    typedef typename DiscreteFunctionType :: DomainType DomainType;
    typedef typename DiscreteFunctionType :: RangeType  RangeType;

    // use LineSegmentSampler to get solution values
    // along a segment
    typedef Dune::Fem::LineSegmentSampler< GridPartType > LineSegmentSamplerType;

    // note to me: DomainType should be only defined in NSModel, not like this
    // typedef FieldVector< double, dimension > DomainType;

    // create a line of interest through points point1 and point2
    DomainType point1(0.5), point2(0.5);
    point1[ 0 ] = 0;
    point2[ 0 ] = 1;

    LineSegmentSamplerType lineSegmentSampler( gridPart, point1, point2 );

    // default to 10m resolution for sample points
    const double resolution = Dune::Fem::Parameter::getValue< double >( "linesegmentsamplerresolution", 0.01 );
    const int numPoints = (int) ((point2[0]-point1[0])/resolution);

    // note to me: RangeType should be only defined in NSModel, not like this
    std::vector< RangeType > sampleValues;
    sampleValues.resize( numPoints );

    {
      lineSegmentSampler( solution, sampleValues );
      std::ofstream gnuFile("sample.gnu" );
      for( size_t i=0; i<sampleValues.size(); ++i )
      {
        gnuFile << double(i*resolution) << " "<< sampleValues[ i ] << std::endl;
      }
    }

    {
      lineSegmentSampler( exactSolution, sampleValues );
      std::ofstream gnuFile("exactsample.gnu" );
      for( size_t i=0; i<sampleValues.size(); ++i )
      {
        gnuFile << double(i*resolution) << " "<< sampleValues[ i ] << std::endl;
      }
    }
  }


  //! [Writing data to disc and error calculation]

  std::cout << "Mass change: " << std::abs(mass - massIni) << std::endl;

  std::cout << std::scientific << std::setprecision(8) << "CPU time used: "
            << timer.elapsed() << " seconds." << std::endl;

  return error;
}

template< class HGridType >
double algorithm ( HGridType &grid, int step )
{
  static const std::string models[] = { "transport", "buckley-leverett", "buck-lev-flux", "euler" };
  const int mdl = Dune::Fem::Parameter::getEnum( "finitevolume.model", models );

  if( mdl == 0)
  {
    TransportModel< typename HGridType::ctype, HGridType::dimensionworld > model;
    return algorithm( grid, model, step );
  }
  else if ( mdl == 1 )
  {
    BuckleyLeverettModel< typename HGridType::ctype, HGridType::dimensionworld > model;
    return algorithm( grid, model, step );
  }
  else if ( mdl == 2 )
  {
    BuckleyLeverettModelFluxRec< typename HGridType::ctype, HGridType::dimensionworld > model;
    return algorithm( grid, model, step );
  }
  else
  {
    EulerModel< typename HGridType::ctype, HGridType::dimensionworld > model;
    return algorithm( grid, model, step );
  }
}

template <class Grid>
void writeGrid( const Grid& grid, int i )
{
  /*
  Dune::DGFWriter< typename Grid::LeafGridView > writer( grid.leafGridView() );
  std::stringstream tmp;
  tmp << "out-" << i;
  writer.write( tmp.str() );
  */
}

template <class Grid>
std::shared_ptr< Grid > toDual( const std::shared_ptr< Grid >& gridPtr )
{
  return gridPtr;
}

#if HAVE_DUNE_POLYGONGRID
std::shared_ptr< Dune::PolygonGrid< double > > toDual( const std::shared_ptr< Dune::PolygonGrid< double > >& gridPtr )
{
  typedef Dune::PolygonGrid< double > Grid;
  return std::shared_ptr< Grid > (new Grid( gridPtr->dualGrid() ) );
}
#endif

// main
// ----

int main ( int argc, char **argv )
try
{
  // initialize MPI, if necessary
  Dune::Fem::MPIManager::initialize( argc, argv );

  // append overloaded parameters from the command line
  Dune::Fem::Parameter::append( argc, argv );

  // append possible given parameter files
  for( int i = 1; i < argc; ++i )
    Dune::Fem::Parameter::append( argv[ i ] );

  // append default parameter file
  Dune::Fem::Parameter::append( "parameter" );

  bool hasExactSolution;
  std::string gridfile;
  TransportModel< typename HGridType::ctype, HGridType::dimensionworld > model;
  hasExactSolution = model.problem().hasExactSolution();
  gridfile = model.problem().gridFile( "../grids/" );

  // create grid form
  if( Dune::Fem::MPIManager::rank() == 0 )
    std::cout << "Loading macro grid: " << gridfile << std::endl;
  std::shared_ptr< HGridType > gridPtr;
#if HAVE_ECL_INPUT
  int pos = gridfile.find( "DATA" );
  if( pos > 0 )
  {
    Opm::Parser parser;
    Opm::ParseContext parseContext;
    const auto deck = parser.parseFile(gridfile , parseContext);
    std::vector<double> porv;

#if defined POLYHEDRALGRID
    gridPtr.reset( new HGridType(deck, porv) );
#elif defined CPGRID
    std::shared_ptr<Opm::EclipseState> eclipseState = std::make_shared< Opm::EclipseState > ( deck, parseContext );
    gridPtr.reset( new HGridType() );
    gridPtr->processEclipseFormat(eclipseState->getInputGrid(),
                                    /*isPeriodic=*/false,
                                    /*flipNormals=*/false,
                                    /*clipZ=*/false,
                                    porv);
#else
    std::abort();
#endif

  }
  else
#endif
    gridPtr.reset(  Dune::GridPtr< HGridType >(gridfile).release() );

  // do initial load balance
  // gridPtr->loadBalance();

  // initial grid refinement
  const int level = Dune::Fem::Parameter::getValue< int >( "finitevolume.startlevel" );

  // number of global refinements to bisect grid width
  const int refineStepsForHalf = Dune::DGFGridInfo< HGridType >::refineStepsForHalf();

  // refine grid
  Dune::Fem::GlobalRefine::apply( *gridPtr, level * refineStepsForHalf );

  // convert to dual grid (PolygonGrid only)
  gridPtr = toDual( gridPtr );

  HGridType& grid = *gridPtr ;

  // setup EOC loop
  const int repeats = Dune::Fem::Parameter::getValue< int >( "finitevolume.repeats", 0 );

  // calculate first step
  double oldError = algorithm( grid, (repeats > 0) ? 0 : -1 );
  double dimFactor = std::pow( 2.0, HGridType::dimension-1 );
  double oldDof = 1.0/(dimFactor * double(grid.size( 0 )));
  writeGrid( grid, 0 );

  for( int step = 1; step <= repeats; ++step )
  {

    // refine globally such that grid with is bisected
    // and all memory is adjusted correctly
    Dune::Fem::GlobalRefine::apply( grid, refineStepsForHalf );

    writeGrid( grid, step );

    const double newError = algorithm( grid, step );
    const double newDof   = 1.0/double(grid.size( 0 ));

    if( hasExactSolution )
    {
      const double eoc = std::log( oldError / newError ) / (std::log( oldDof / newDof ));
      if( Dune::Fem::MPIManager::rank() == 0 )
      {
        std::cout << "Error: " << newError << std::endl;
        std::cout << "EOC( " << step << " ) = " << eoc << std::endl;
      }
      oldError = newError;
      oldDof   = newDof / dimFactor;
    }
    std::cout << "---------------------------------------" << std::endl;
  }

  return 0;
}
catch( const Dune::Exception &exception )
{
  std::cerr << "Error: " << exception << std::endl;
  return 1;
}
