#ifndef PROBLEM_HH
#define PROBLEM_HH

// C++ includes
#include <sstream>

// dune-common includes
#include <dune/common/fvector.hh>

/** \class ProblemData
 *  \brief virtual base class for our problems
 *
 *  \tparam  FunctionSpace  function space
 */
template< class FunctionSpace >
struct ProblemData
{
  //! \brief function space type
  typedef FunctionSpace FunctionSpaceType;

  //! \brief domain dimension
  static const int dimDomain = FunctionSpaceType::dimDomain;
  //! \brief range dimension
  static const int dimRange  = FunctionSpaceType::dimRange;

  //! \brief domain field type
  typedef typename FunctionSpaceType::DomainFieldType DomainFieldType;
  //! \brief range type
  typedef typename FunctionSpaceType::RangeFieldType RangeFieldType;

  //! \brief domain type
  typedef typename FunctionSpaceType::DomainType DomainType;
  //! \brief range type
  typedef typename FunctionSpaceType::RangeType RangeType;

  ProblemData() : t_(0) {}

  virtual ~ProblemData() {}

  /** \brief obtain the file name of the macro grid for this problem
   *
   *  \param[in]  path  path to the macro grids
   *
   *  \returns the file name of the macro grid
   */
  virtual std::string gridFile ( const std::string &path ) const = 0;

  /** \brief evaluate the initial data
   *
   *  \param[in]  x  coordinate to evaluate the initial data in
   *
   *  \returns the evaluated initial data
   */
  virtual RangeType initial ( const DomainType &x ) const = 0;

  virtual double f( const double ) const { return 0; }

  /** \brief evaluate the data for inflow boundaries
   *
   *  \param[in]  x     coordinate to evaluate the boundary data in
   *  \param[in]  time  time to evaluate boundary data at
   *
   *  \returns the evaluated boundary data
   */
  virtual RangeType boundaryValue ( const DomainType &x, double time ) const = 0;

  /** \brief obtain the end time for the evolution problem */
  virtual double endTime () const = 0;

  /** \brief obtain the interval for writing data to disk */
  virtual double saveInterval () const = 0;

/*********************************************************/
/***                 NEW FOR LESSON 1                  ***/
/*********************************************************/
  /** \brief compute a jump type indicator
   *
   *  \param uLeft left state
   *  \param uRight right state
   *  \return the indicator
   */
  virtual double adaptationIndicator ( const RangeType &uLeft, const RangeType &uRight ) const = 0;
  /** \brief refine tolerance to use */
  virtual double refineTol () const = 0;
  /** \brief coarsening tolerance (defaults to 1/3 of refinement tolerance
   *         Default implementation is 1/3 of the refinement tolerance
   */
  virtual double coarsenTol () const
  {
    return refineTol()/3.;
  }
/*********************************************************/

  virtual bool hasExactSolution() const = 0;

  /** \brief evaluate the exact solution
   *         in the form required for the error computation.
   *
   *  \param[in]  x    coordinate to evaluate the exact solution
   *  \param[in]  time time at which to evaluate the exact solution
   *  \param[out] ret  the evaluated exact solution.
   *                   Set to 0 if no exact solution available.
   */
  virtual void evaluate ( const DomainType &x, double time, RangeType &ret ) const
  {
    // by default we only have initial data
    ret = initial(x);
  }

  /** \brief evaluate the exact solution at initial time
   *         in the form required for the l2 projection computation.
   *
   *  \param[in]  x   coordinate to evaluate the exact solution
   *  \param[out] ret the evaluated exact solution.
   *                  Set to 0 if no exact solution available.
   */
  virtual void evaluate ( const DomainType &x, RangeType &ret ) const
  {
    evaluate(x,t_,ret);
  }

  /** \brief set time at which to evaluate function with evalute(x,ret)
   */
  void setTime(double t) const
  {
    t_ = t;
  }

  virtual void velocity ( const DomainType &x, double time, const RangeType& u, DomainType& v ) const
  {
    v = 0;
  }

  virtual bool physical( const RangeType &value ) const
  {
    return true;
  }

  protected:
  mutable double t_;
}; // end class ProblemData

#endif // PROBLEM_HH
