/*********************************************************/
/***                 NEW FOR LESSON 1                  ***/
/*********************************************************/

#ifndef ADAPTATION_HH
#define ADAPTATION_HH

/** include the grid capabilities
 ** to distiguish grids without local adaptation **/
#include <dune/grid/common/capabilities.hh>

// GridMarker
// ----------

/** \class GridMarker
 *  \brief class for marking entities for adaptation.
 *
 *  This class provides some additional strategies for marking elements
 *  which are not so much based on an indicator but more on mere
 *  geometrical and run time considerations. If based on some indicator
 *  an entity is to be marked, this class additionally tests for example
 *  that a maximal or minimal level will not be exceeded.
 */
template< class Grid >
struct GridMarker
{
  typedef typename Grid::template Codim< 0 >::Entity EntityType;

  /** \brief constructor
   *  \param grid     the grid. Here we can not use a grid view since they only
   *                  a constant reference to the grid and the
   *                  mark method can not be called.
   *  \param minLevel the minimum refinement level
   *  \param maxLevel the maximum refinement level
   */
  GridMarker( Grid &grid, int minLevel, int maxLevel )
  : grid_(grid),
    minLevel_( minLevel ),
    maxLevel_( maxLevel ),
    wasMarked_( 0 )
  {}

  /** \brief mark an element for refinement
   *  \param entity  the entity to mark; it will only be marked if its level is below maxLevel.
   */
  void refine ( const EntityType &entity )
  {
    if( entity.level() < maxLevel_ )
    {
      grid_.mark( 1, entity );
      wasMarked_ = 1;
    }
  }

  /** \brief mark all neighbors of a given entity for refinement
   *  \param gridView the grid view from which to take the intersection iterator
   *  \param entity the corresponding entity
   */
  template< class GridView >
  void refineNeighbors ( const GridView &gridView, const EntityType &entity )
  {
    typedef typename GridView::IntersectionIteratorType IntersectionIterator;
    typedef typename IntersectionIterator::Intersection Intersection;

    const IntersectionIterator end = gridView.iend( entity );
    for( IntersectionIterator it = gridView.ibegin( entity ); it != end; ++it )
    {
      const Intersection &intersection = *it;
      if( intersection.neighbor() )
        refine( intersection.outside() );
    }
  }

  /** \brief mark an element for coarsening
   *  \param entity  the entity to mark; it will only be marked if its level is above minLevel.
   */
  void coarsen ( const EntityType &entity )
  {
    if( (get( entity ) <= 0) && (entity.level() > minLevel_) )
    {
      grid_.mark( -1, entity );
      wasMarked_ = 1;
    }
  }

  /** \brief get the refinement marker
   *  \param entity entity for which the marker is required
   *  \return value of the marker
   */
  int get ( const EntityType &entity ) const
  {
    return grid_.getMark( entity );
  }
  /** \brief returns true if any entity was marked for refinement
   */
  bool marked()
  {
    wasMarked_ = grid_.comm().max (wasMarked_);
    return (wasMarked_ != 0);
  }

private:
  Grid &grid_;
  int minLevel_;
  int maxLevel_;
  int wasMarked_;
};

#endif
