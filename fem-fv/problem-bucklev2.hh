#ifndef PROBLEM_BUCKLEV_FLUXREC_HH
#define PROBLEM_BUCKLEV_FLUXREC_HH

// C++ includes
#include <iostream>
#include <sstream>

// dune-common includes
#include <dune/common/fvector.hh>

// dune-fem includes
#include <dune/fem/space/common/functionspace.hh>

// local includes
#include "problem.hh"

// BuckleyLeverettModelFluxRecTraits
// --------------------

template< class DomainField, int dimD >
struct BuckleyLeverettModelFluxRecTraits
{
  const static int dimDomain = dimD;
  const static int dimRange = 1;

  typedef DomainField DomainFieldType;
  typedef double RangeFieldType;

  typedef Dune::Fem::FunctionSpace< DomainFieldType, RangeFieldType, dimDomain, dimRange > FunctionSpaceType;
};

/** \class BuckleyLeverettProblemData
 *  \brief basic problem data for BuckleyLeverettProblem
 *
 *  This class defines the data for the following transport problem:
 *  \f{eqnarray*}
 *  \partial_t c + \nabla \cdot (v c)
 *    &=& 0 \quad\mbox{in $\Omega \times ]0,T[$}\\
 *  c &=& g \quad\mbox{on $\Gamma_{\mathrm{in}}$}\\
 *  c &=& c_0 \quad\mbox{on $\Omega \times \lbrace 0 \rbrace$}
 *  \f}
 *
 *  We define the following parameters:
 *  - a grid for the domain \f$\Omega\f$,
 *  - the (constant) velocity \f$v\f$,
 *  - the boundary data \f$b\f$,
 *  - the initial data \f$c_0\f$.
 */
// BuckleyLeverettProblem
// ----------------

/** \class BuckleyLeverettProblem
 *  \brief description of a transport problem
 *
 *  This class describes the following transport problem:
 *  \f{eqnarray*}
 *  \partial_t c + \nabla \cdot (v c)
 *    &=& 0 \quad\mbox{in $\Omega \times ]0,T[$}\\
 *  c &=& g \quad\mbox{on $\Gamma_{\mathrm{in}}$}\\
 *  c &=& c_0 \quad\mbox{on $\Omega \times \lbrace 0 \rbrace$}
 *  \f}
 */
template< class DomainField, int dimD >
struct BuckleyLeverettModelFluxRec
{
  typedef BuckleyLeverettModelFluxRecTraits< DomainField, dimD > Traits;
  typedef typename Traits::FunctionSpaceType FunctionSpaceType;
  typedef BuckleyLeverettProblemData< FunctionSpaceType > ProblemType;

  static const int dimDomain = ProblemType::dimDomain;
  static const int dimRange = ProblemType::dimRange;

  typedef typename ProblemType::DomainType DomainType;
  typedef typename ProblemType::RangeType RangeType;

  /** \brief constructor
   */
  BuckleyLeverettModelFluxRec ( )
  {
    problem_.reset( new BuckleyLeverettProblemData1< FunctionSpaceType >() );
  }

  /** \brief obtain problem */
  const ProblemType &problem () const
  {
    return *problem_;
  }

  bool fluxReconstruction () const { return true; }

  RangeType reactionTerm( const double time, const DomainType &xGlobal, const RangeType& u ) const
  {
    RangeType source ( 0 );
    //source[ 0 ] = -0.1 * u[ 0 ] * u[ 1 ];
    //source[ 0 ] = 0;
    //if( source.dimension > 1 )
    //  source[ 1 ] = -source[ 0 ];
    return source;
  }

  /** \brief evaluate the numerical flux on an intersection
   *
   *  \param[in]   normal   scaled normal of the intersection
   *  \param[in]   time     current time
   *  \param[in]   xGlobal  evaluation point in global coordinates
   *  \param[in]   uLeft    value of the solution in the inside entity
   *  \param[in]   uRight   value of the solution in the outside entity
   *  \param[out]  flux     numercial flux
   *
   *  \returns the maximum wave speed
   */
  double numericalFlux ( const DomainType &normal,
                         const double time,
                         const DomainType &xGlobal,
                         const RangeType &uLeft, const RangeType &uRight,
                         RangeType &flux ) const
  {
    DomainType v;
    problem().velocity(xGlobal, time, uLeft, v);
    const double upwind = normal * v;
    if( upwind > 0 )
    {
      flux = uLeft;
    }
    else
    {
      flux = uRight;
    }
    flux *= upwind;
    return std::abs( upwind );

    /*
    double upwind;
    DomainType vel(0);
    problem_->velocity( xGlobal, time, uLeft, vel );

    upwind = normal*vel;
    if (upwind>0){
      vel[0]   = uLeft[0];
      flux[0] = normal*vel;
      vel[0]   = 1;
      upwind   = normal*vel;
    }
    else{
      vel[0]   = uRight[0];
      flux[0]  = normal*vel;
      vel[0]   = 1;
      upwind   = normal*vel;
    }
    double wave = std::abs( upwind );
    vel[0] = 0.5*(uLeft[0]+uRight[0]);
    double wavem = std::abs(normal*vel);
    wave = (wave>wavem)?wave:wavem;
    return wave; // +model_.tstep_eps;
    */
  }

  /** \brief evaluate the numerical flux on a boundary
   *
   *  \param[in]   bndId    boundary id
   *  \param[in]   normal   scaled normal of the boundary
   *  \param[in]   time     current time
   *  \param[in]   xGlobal  evaluation point in global coordinates
   *  \param[in]   uLeft    value of the solution in the inside entity
   *  \param[out]  flux     numercial flux
   *
   *  \returns the maximum wave speed
   */
  double boundaryFlux ( const int bndId,
                        const DomainType &normal,
                        const double time,
                        const DomainType &xGlobal,
                        const RangeType& uLeft,
                        RangeType &flux ) const
  {
    RangeType uRight = problem().boundaryValue( xGlobal, time );
    return numericalFlux( normal, time, xGlobal, uLeft, uRight, flux );
  }

/*********************************************************/
/***                 NEW FOR LESSON 1                  ***/
/*********************************************************/
  /** \brief compute adaptation indicator at intersection
   *
   *  \param[in]   normal   scaled normal of the intersection
   *  \param[in]   time     current time
   *  \param[in]   xGlobal  evaluation point in global coordinates
   *  \param[in]   uLeft    value of the solution in the inside entity
   *  \param[in]   uRight   value of the solution in the outside entity
   *
   *  \return value of indicator
   */
  double indicator ( const DomainType &normal,
                     const double time,
                     const DomainType &xGlobal,
                     const RangeType &uLeft, const RangeType &uRight) const
  {
    return problem().adaptationIndicator( uLeft, uRight );
  }

  /** \brief compute adaptation indicator at boundary
   *
   *  \param[in]   bndId    boundary id
   *  \param[in]   normal   scaled normal of the intersection
   *  \param[in]   time     current time
   *  \param[in]   xGlobal  evaluation point in global coordinates
   *  \param[in]   uLeft    value of the solution in the inside entity
   *
   *  \return value of indicator
   */
  double boundaryIndicator ( const int bndId,
                             const DomainType &normal,
                             const double time,
                             const DomainType &xGlobal,
                             const RangeType& uLeft) const
  {
    return indicator( normal,time,xGlobal, uLeft, problem().boundaryValue(xGlobal,time) );
  }
/*********************************************************/

protected:
  std::unique_ptr< ProblemType > problem_;

}; // end class BuckleyLeverettProblem
#endif // PROBLEM_TRANSPORT_HH
