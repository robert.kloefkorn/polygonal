#ifndef FVSCHEME_HH
#define FVSCHEME_HH

// C++ includes
#include <limits>

// dune-fem includes
#include <dune/fem/quadrature/intersectionquadrature.hh>
#include <dune/fem/solver/timeprovider.hh>
#include <dune/fem/operator/common/spaceoperatorif.hh>
#include <dune/fem/misc/boundaryidprovider.hh>

#include "reconstruction.hh"
#include "limitermodel.hh"

/*********************************************************/
/***                 NEW FOR LESSON 1                  ***/
/*********************************************************/
#include "adaptation.hh"
/*********************************************************/

// FiniteVolumeScheme
// ------------------
/** \class FiniteVolumeScheme
 *  \brief the implementation of the finite volume scheme
 *
 *  \tparam  DiscreteFunction  discrete function space type
 *  \tparam  Model             discretization of the Model.
 *                             This template class must provide
 *                             the following types and methods:
 *  \code
      typedef ... RangeType;
      const ProblemData &problem () const;
      double numericalFlux ( const DomainType &normal,
                             const double time,
                             const DomainType &xGlobal,
                             const RangeType &uLeft,
                             const RangeType &uRight,
                             RangeType &flux ) const;
      double boundaryFlux ( const int bndId,
                            const DomainType &normal,
                            const double time,
                            const DomainType &xGlobal,
                            const RangeType& uLeft,
                            RangeType &flux ) const;
      double indicator ( const DomainType &normal,
                         const double time,
                         const DomainType &xGlobal,
                         const RangeType &uLeft, const RangeType &uRight) const
      double boundaryIndicator ( const int bndId,
                                 const DomainType &normal,
                                 const double time,
                                 const DomainType &xGlobal,
                                 const RangeType& uLeft) const
 *  \endcode
 */
template< class DiscreteFunction, class Model >
struct FiniteVolumeScheme : public Dune::Fem::SpaceOperatorInterface< DiscreteFunction >
{
  // discrete function type
  typedef DiscreteFunction DiscreteFunctionType;
  // model type
  typedef Model ModelType;

  // discrete function space type
  typedef typename DiscreteFunctionType::DiscreteFunctionSpaceType DiscreteFunctionSpaceType;

  // grid part type
  typedef typename DiscreteFunctionSpaceType::GridPartType GridPartType;

  typedef Dune::Fem::LimiterModel< GridPartType, typename ModelType :: ProblemType >  LimiterModelType;
  typedef Dune::Fem::LimitedReconstruction< LimiterModelType, DiscreteFunctionType >  ReconstructionType;

  typedef typename ReconstructionType :: ReconstructionScheme  ReconstructionSchemeIdentifier;

  // iterator type
  typedef typename DiscreteFunctionSpaceType::IteratorType IteratorType;
  // entity codim 0 type
  typedef typename IteratorType::Entity EntityType;

  // geometry type
  typedef typename EntityType::Geometry GeometryType;
  // global coordinates
  typedef typename GeometryType::GlobalCoordinate GlobalCoordinateType;
  // local coordinates
  typedef typename GeometryType::LocalCoordinate LocalCoordinateType;

  // intersection iterator type
  typedef typename GridPartType::IntersectionIteratorType IntersectionIteratorType;
  // intersection type
  typedef typename IntersectionIteratorType::Intersection IntersectionType;
  // geometry of intersection
  typedef typename IntersectionType::Geometry IntersectionGeometryType;

  // domain type
  typedef typename DiscreteFunctionSpaceType::DomainType DomainType;
  // range type
  typedef typename DiscreteFunctionSpaceType::RangeType RangeType;

public:
  /** \brief constructor
   *
   *  \param[in]  space  the discrete function space to use
   *  \param[in]  model  discretization of the model
   */
  //! [CreateThreadIterator]
  FiniteVolumeScheme ( const DiscreteFunctionSpaceType& space, const ModelType &model,
                       Dune::Fem::TimeProviderBase& timeProvider )
   : space_( space ),
     model_( model ),
     timeProvider_( timeProvider ),
     limiterModel_( model.problem() ),
     reconstruction_( limiterModel_, space ),
     schemeId_( ReconstructionType::getReconstructionSchemeId() ),
     linear_( false )
  {}

  const DiscreteFunctionSpaceType& space () const { return space_; }

  virtual void activateLinear() { //std::cout << "Linear part activated" << std::endl;
    linear_ = true;
  }
  virtual void deactivateLinear() { //std::cout << "Linear part deactivated" << std::endl;
    linear_ = false ;
  }
  //*/


  //! [CreateThreadIterator]

  /** \brief compute the update vector for one time step
   *
   *  \param[in]   time      current time
   *  \param[in]   solution  solution at time <tt>time</tt>
   *                         (arbitrary type with operator[](const Entity&) operator)
   *  \param[out]  update    result of the flux computation
   *
   *  \returns maximal time step
   */
  void
  operator() ( const DiscreteFunctionType &solution, DiscreteFunctionType &update ) const;

/*********************************************************/
/***                 NEW FOR LESSON 1                  ***/
/*********************************************************/
  /** \brief set grid marker for refinement / coarsening
   *
   *  \param[in]  time      current time
   *  \param[in]  solution  solution at time <tt>time</tt>
   *  \param      marker    grid marker
   *
   *  \note The marker is responsible for limiting the grid depth.
   */
  void
  mark ( const double time, const DiscreteFunctionType &solution, GridMarker< typename GridPartType::GridType > &marker ) const;
/*********************************************************/

  /** \brief return model */
  const ModelType &model () const { return model_; }

  double timeStepEstimate () const { return dtEst_; }

  std::string fvOrderName() const
  {
    return ReconstructionType::schemeName( schemeId_ );
  }

  const bool higherOrder() const
  {
    return (schemeId_ != ReconstructionSchemeIdentifier::firstOrder);
  }

protected:
  const DiscreteFunctionSpaceType& space_;
  const ModelType &model_;
  Dune::Fem::TimeProviderBase& timeProvider_;
  LimiterModelType limiterModel_;
  mutable ReconstructionType reconstruction_;

  const ReconstructionSchemeIdentifier schemeId_;

  bool linear_;
  mutable double dtEst_;
}; // end FiniteVolumeScheme

template< class DiscreteFunction, class Model >
inline void FiniteVolumeScheme< DiscreteFunction, Model >
  ::operator() ( const DiscreteFunctionType &solution, DiscreteFunctionType &update ) const
{
  const double time = timeProvider_.time();

  // set update to zero
  update.clear();

  const bool higherOrder = (schemeId_ != ReconstructionSchemeIdentifier::firstOrder);

  // time step size (using std:min(.,dt) so set to maximum)
  double dt = std::numeric_limits<double>::infinity();

  // obtain grid part and index set references
  const DiscreteFunctionSpaceType &space = solution.space();
  const GridPartType &gridPart = space.gridPart();
  const typename GridPartType::IndexSetType &indexSet = gridPart.indexSet();

  typedef Dune::Fem::BoundaryIdProvider< typename GridPartType::GridType >  BoundaryIdProviderType;

  typedef typename ReconstructionType::LocalFunctionType  ReconstructedLocalFunctionType;

  if( higherOrder )
  {
    if( model().fluxReconstruction() )
    {
      std::cout << "Computing flux reconstruction scheme" << std::endl;
      DiscreteFunctionType solcopy( solution );
      // iterate over all entities
      const IteratorType end = space.end();
      for( IteratorType it = space.begin(); it != end; ++it )
      {
        // obtain entity and geometry
        const EntityType &entity = *it;
        // get barycenter (can be obtained also by geometry.center())
        const auto lf = solution.localFunction( entity );
        auto clf = solcopy.localFunction( entity );
        clf[ 0 ] = model().problem().f( lf[ 0 ] );
      }
      // compute linear reconstructions
      reconstruction_.update( time, solcopy, schemeId_, ! linear_ );
    }
    else
      reconstruction_.update( time, solution, schemeId_, ! linear_ );
  }

  //! [PragmaParallel]

  //! [iteration over space]
  // iterate over all entities
  const IteratorType end = space.end();
  for( IteratorType it = space.begin(); it != end; ++it )
  {
    // obtain entity and geometry
    const EntityType &entity = *it;
    const GeometryType &geo = entity.geometry();
    unsigned int enIdx = indexSet.index(entity);
    //! [iteration over space]

    // obtain local functions for solution and update
    typedef typename DiscreteFunctionType::LocalFunctionType LocalFunctionType;
    const LocalFunctionType lfSolEn = solution.localFunction( entity );

    RangeType uLeft;
    // for low order evaluate function
    if( ! higherOrder )
    {
      lfSolEn.evaluate( LocalCoordinateType(0.25), uLeft );
    }

    LocalFunctionType lfUpdEn = update.localFunction( entity );

    // estimate for wave speed
    double waveSpeed = 0.0;

    // cell volume
    const double enVolume = geo.volume();

    // 1 over cell volume
    const double enVolume_1 = 1.0/enVolume;
    double averageVolume = 0;

    int count = 0;

    double sum (0.0);
    GlobalCoordinateType velo( 1 );
    // run through all intersections with neighbors and boundary
    //! [iteration over intersections]
    const IntersectionIteratorType iitend = gridPart.iend( entity );
    for( IntersectionIteratorType iit = gridPart.ibegin( entity ); iit != iitend; ++iit, ++count )
    {
      const IntersectionType &intersection = *iit;
      /* Fetch the intersection's geometry */
      const IntersectionGeometryType &intersectionGeometry = intersection.geometry();
      //! [iteration over intersections]

      //! [evaluation of local function]
      const GlobalCoordinateType globalPoint = intersectionGeometry.center();
      const GlobalCoordinateType  normal = intersection.centerUnitOuterNormal();
      const IntersectionGeometryType& interGeom = intersection.geometry();
      const GlobalCoordinateType interCenter =  interGeom.center();
      const double faceVolume = interGeom.volume();
      averageVolume += faceVolume ;

      sum += (normal * velo) * faceVolume;

      LocalCoordinateType pointEn (0.25);
      // evaluate data
      if( higherOrder )
      {
        // get the point in the entity and neighbor
        ReconstructedLocalFunctionType lfRecEn = reconstruction_.localFunction( entity );
        lfRecEn.evaluateGlobal( interCenter, uLeft );
      }

      //! [evaluation of local function]

      // handle interior face
      // NOTE: this is also true for periodic boundaries
      //! [only intersection with boundary elements]
      if( intersection.neighbor() )
      {
      //! [only intersection with boundary elements]
        //! [avoid double evaluation]
        // access neighbor
        const EntityType neighbor = intersection.outside();

        unsigned int nbIdx = indexSet.index(neighbor);

        // compute flux from one side only
        if( (enIdx < nbIdx) ||
            (neighbor.partitionType() != Dune::InteriorEntity) )
        {
        //! [avoid double evaluation]
        //! [calculate update]
          // calculate (1 / neighbor volume)
          const double nbVolume = neighbor.geometry().volume();
          const double nbVolume_1 = 1.0 / nbVolume;

          // obtain local functions on neighbor entity
          LocalFunctionType lfUpdNb = update.localFunction( neighbor );

          // get the point in the neighbor
          LocalCoordinateType pointNb(0.25);
          //= intersection.geometryInOutside().center();

          // evaluate data
          RangeType uRight;
          if( higherOrder )
          {
            ReconstructedLocalFunctionType lfRecNb = reconstruction_.localFunction( neighbor );
            lfRecNb.evaluateGlobal( interCenter, uRight );
          }
          else
          {
            LocalFunctionType lfSolNb = solution.localFunction( neighbor );
            lfSolNb.evaluate( pointNb, uRight );
          }

          // apply numerical flux
          RangeType flux;
          double ws = model().numericalFlux( normal, time, globalPoint, uLeft, uRight, flux );
          waveSpeed += ws * faceVolume;
          assert( flux == flux );

          // calc update of entity
          RangeType enFlux = flux;
          enFlux *= -enVolume_1 * faceVolume;
          lfUpdEn.axpy( pointEn, enFlux );
          //! [calculate update]

          // calc update of neighbor
          RangeType nbFlux = flux;
          nbFlux *= nbVolume_1 * faceVolume;
          lfUpdNb.axpy( pointNb, nbFlux );

          //! [calculate time step estimate]
          // compute dt restriction
          dt = std::min( dt, std::min( enVolume, nbVolume ) / waveSpeed );
          //! [calculate time step estimate]
        }
      }
      // handle boundary face
      // NOTE: boundary is also true for periodic faces, therefore else if
      //! [calculate boundary flux]
      else if ( intersection.boundary() )
      {
        // apply boundary flux
        RangeType flux;
        double ws = model().boundaryFlux( BoundaryIdProviderType::boundaryId( intersection ), normal, time, globalPoint, uLeft, flux );
        waveSpeed += ws * faceVolume;
        flux *= -enVolume_1*faceVolume;
        assert( flux == flux );

        // apply volume
        lfUpdEn.axpy( pointEn, flux );

        // compute dt restriction
        dt = std::min( dt, enVolume / waveSpeed );
      }
      //! [calculate boundary flux]
    } // end all intersections

    /*
    if( sum > 1e-6 )
    {
      std::cout << "Normals don't sum up to 0 " << sum << std::endl;
      std::abort();
    }
    */

    //RangeType uLeft;
    lfSolEn.evaluate( geo.center(), uLeft );
    RangeType source = model_.reactionTerm( time, geo.center(), uLeft );
    source *= enVolume_1 * averageVolume / double(count);

    // apply volume
    lfUpdEn.axpy( geo.center(), source );


  } // end grid traversal

  // make update consistent on ghost cells
  update.communicate();

  dtEst_ = dt ;
  // return time step
  // timeProvider_.provideTimeStepEstimate( dt );
  // return dt;
}

/*********************************************************/
/***                 NEW FOR LESSON 1                  ***/
/*********************************************************/
template< class DiscreteFunction, class Model >
inline void FiniteVolumeScheme< DiscreteFunction, Model >
  ::mark ( const double time, const DiscreteFunctionType &solution, GridMarker< typename GridPartType::GridType > &marker ) const
{
  return ;
#if 0
  // obtain grid part and index set references
  const DiscreteFunctionSpaceType &space = solution.space();
  const GridPartType &gridPart = space.gridPart();

  typedef typename DiscreteFunctionSpaceType :: IteratorType IteratorType ;
  // iterate over all entities
  const IteratorType end = space.end();
  for( IteratorType it = space.begin(); it != end; ++it )
  {
    const EntityType &entity = *it;

    // if marked for refinement nothing has to be done for this element
    if( marker.get( entity ) > 0 )
      continue;

    // maximum value of the indicator over all intersections
    double entityIndicator = 0.0;

    // obtain local functions for solution and update
    typedef typename DiscreteFunctionType::LocalFunctionType LocalFunctionType;
    const LocalFunctionType lfSolEn = solution.localFunction( entity );

    // run through all intersections with neighbors and boundary
    const IntersectionIteratorType iiterend = gridPart.iend( entity );
    for( IntersectionIteratorType iiter = gridPart.ibegin( entity ); iiter != iiterend; ++iiter )
    {
      const IntersectionType &intersection = *iiter;
      /* Fetch the intersection's geometry */
      const IntersectionGeometryType &intersectionGeometry = intersection.geometry();

      const GlobalCoordinateType globalPoint = intersectionGeometry.center();
      const GlobalCoordinateType normal = intersection.centerUnitOuterNormal();

      // indicator for this intersection
      double localIndicator = 0.0;

      // get the point in the entity and neighbor
      LocalCoordinateType pointEn = intersection.geometryInInside().center();

      // evaluate data
      RangeType uLeft;
      lfSolEn.evaluate( pointEn, uLeft );

      // no neighbor?
      if( !intersection.neighbor() )
      {
        const int bndId = intersection.boundaryId();

        // compute indicator for this quadrature point
        const double indicator
          = model().boundaryIndicator( bndId, normal, time, globalPoint, uLeft );
        // indicator on intersection is the maximum over all quadrature points
        localIndicator = std::max( localIndicator, indicator );
      }
      else
      {
        // access neighbor
        const EntityType neighbor = intersection.outside();

        // obtain local functions on neighbor entity
        LocalFunctionType lfSolNb = solution.localFunction( neighbor );

        // get the point in the neighbor
        LocalCoordinateType pointNb = intersection.geometryInOutside().center();

        // evaluate data
        RangeType uRight;
        lfSolNb.evaluate( pointNb, uRight );

        // compute indicator for this quadrature point
        const double indicator
          = model().indicator( normal, time, globalPoint, uLeft, uRight );
        // indicator on intersection is the maximum over all quadrature points
        localIndicator = std::max( localIndicator, indicator );
      }

      // for coarsening we need maximum indicator over all intersections
      entityIndicator = std::max( entityIndicator, localIndicator );

      // test if we can mark for refinement and quit this entity
      if( localIndicator > model().problem().refineTol() )
      {
        marker.refine( entity );

        // might be a good idea to refine a slightly larger region
        marker.refineNeighbors( gridPart, entity );

        // we can now continue with next entity
        break;
      }
    } // end of loop over intersections

    // now see if this entity can be removed
    if( entityIndicator < model().problem().coarsenTol() )
      marker.coarsen( entity );
  } // end of loop over entities
#endif

}
/*********************************************************/

#endif // #ifndef FVSCHEME_HH
