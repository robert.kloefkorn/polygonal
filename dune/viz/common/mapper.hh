#ifndef DUNE_VIZ_COMMON_MAPPER_HH
#define DUNE_VIZ_COMMON_MAPPER_HH

#include <algorithm>
#include <cstddef>
#include <limits>

#include <dune/geometry/referenceelements.hh>
#include <dune/geometry/type.hh>

namespace Dune
{

  namespace Viz
  {

    // Mapper
    // ------

    template< class GridView, class Layout >
    struct Mapper
    {
      static const int dimension = GridView::dimension;

    private:
      typedef typename GridView::IndexSet IndexSet;

    public:
      explicit Mapper ( const GridView &gridView, const Layout &layout = Layout() )
        : gridView_( gridView ),
          indexSet_( gridView_.indexSet() ),
          layout_( layout ),
          offset_( GlobalGeometryTypeIndex::size( dimension ) )
      {
        update();
      }

      template< class Entity >
      std::size_t operator() ( const Entity &entity, std::size_t k = 0 ) const
      {
        const GeometryType gt = entity.type();
        assert( k < layout().size( gt ) );
        const std::size_t index = indexSet_.index( entity );
        return k + index*layout().size( gt ) + offset_[ GlobalGeometryTypeIndex::index( gt ) ];
      }

      template< class Entity >
      std::size_t operator() ( const Entity &entity, int i, int codim, std::size_t k = 0 ) const
      {
        typedef ReferenceElements< void, dimension > RefElements;

        const GeometryType gt = RefElements::general( entity.type() ).type( i, codim );
        assert( k < layout().size( gt ) );
        const std::size_t index = indexSet_.subIndex( entity, i, codim  );
        return k + index*layout().size( gt ) + offset_[ GlobalGeometryTypeIndex::index( gt ) ];
      }

      const GridView &gridView () const { return gridView_; }
      const Layout &layout () const { return layout_; }

      std::size_t size () const { return size_; }

      template< class Entity >
      bool contains ( const Entity &entity ) const
      {
        return (layout().size( entity.type() ) > 0) && indexSet_.contains( entity );
      }

      void update ()
      {
        // clear offsets (to the invalid value std::numeric_limits< std::size_t >::max())
        std::fill( offset_.begin(), offset_.end(), std::numeric_limits< std::size_t >::max() );

        size_ = 0;

        for( int codim = 0; codim <= dimension; ++codim )
        {
          for( GeometryType type : indexSet_.types( codim ) )
          {
            assert( offset_[ GlobalGeometryTypeIndex::index( type ) ] == std::numeric_limits< std::size_t >::max() );
            offset_[ GlobalGeometryTypeIndex::index( type ) ] = size_;
            size_ += layout().size( type ) * indexSet_.size( type );
          }
        }
      }

    private:
      GridView gridView_;
      const IndexSet &indexSet_;
      Layout layout_;
      std::size_t size_;
      std::vector< std::size_t > offset_;
    };

  } // namespace Viz

} // namespace Dune

#endif // #ifndef DUNE_VIZ_COMMON_MAPPER_HH
