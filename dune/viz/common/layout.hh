#ifndef DUNE_VIZ_COMMON_LAYOUT_HH
#define DUNE_VIZ_COMMON_LAYOUT_HH

#include <cstddef>

#include <dune/geometry/type.hh>

namespace Dune
{

  namespace Viz
  {

    // CodimLayout
    // -----------

    template< int dim >
    struct CodimLayout
    {
      explicit CodimLayout ( int codim, std::size_t size = 1 )
        : codim_( codim ), size_( size )
      {
        assert( (codim >= 0) && (codim <= dim) );
      }

      std::size_t size ( const GeometryType &gt ) const
      {
        return (int(gt.dim()) == int(dim - codim_) ? size_ : 0u);
      }

    private:
      int codim_;
      std::size_t size_;
    };

  } // namespace Viz

} // namespace Dune

#endif // #ifndef DUNE_VIZ_COMMON_LAYOUT_HH
