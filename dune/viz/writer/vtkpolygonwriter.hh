#ifndef DUNE_VIZ_WRITER_VTKPOLYGONWRITER_HH
#define DUNE_VIZ_WRITER_VTKPOLYGONWRITER_HH

#include <cstddef>

#include <fstream>
#include <functional>
#include <iomanip>
#include <sstream>
#include <type_traits>
#include <vector>

#include <dune/common/fvector.hh>

#include <dune/geometry/type.hh>

#include <dune/grid/common/gridenums.hh>
#include <dune/grid/common/rangegenerators.hh>

#include <dune/viz/common/layout.hh>
#include <dune/viz/common/mapper.hh>
#include <dune/viz/writer/vtkdatatype.hh>
#include <dune/viz/writer/xml.hh>

namespace Dune
{

  namespace Viz
  {

    // VTKPolygonWriter
    // ----------------

    template< class GridView >
    class VTKPolygonWriter
    {
      static const int dimension = GridView::dimension;
      typedef typename GridView::ctype ctype;

      // static_assert( dimension == 2, "VTKPolygonWriter can only write two-dimensional polygons." );

      typedef typename GridView::template Codim< 0 >::Entity Element;
      typedef typename GridView::template Codim< dimension >::Entity Vertex;

      typedef Dune::FieldVector< typename GridView::ctype, GridView::dimensionworld > GlobalCoordinate;

      typedef Viz::CodimLayout< dimension > Layout;
      typedef Viz::Mapper< GridView, Layout > Mapper;

      typedef std::function< XMLElement( const Mapper &, const std::string & ) > ToXML;
      typedef std::vector< std::tuple< std::string, int, ToXML > > DataVectorList;

    public:
      explicit VTKPolygonWriter ( const GridView &gridView )
        : pointMapper_( gridView, Layout( dimension ) ),
          elementMapper_( gridView, Layout( 0 ) )
      {}

      template< class Data >
      void addPointData ( const std::string &name, const std::vector< Data > &data )
      {
        addPointData< Data >( name, [ &data ] ( const Mapper &, const std::string &name ) { return xmlDataArray( name, data ); } );
      }

      template< class DataMap >
      void addPointData ( const std::string &name, DataMap dataMap )
      {
        typedef typename std::decay< typename std::result_of< DataMap( const Vertex & ) >::type >::type Data;
        addPointData< Data >( name, [ dataMap ] ( const Mapper &mapper, const std::string &name ) {
            std::vector< Data > values( mapper.size() );
            //for( const Vertex &vertex : vertices( pointMapper_.gridView(), Partitions::interiorBorder ) )
            const auto end = mapper.gridView().template end< dimension, Dune::InteriorBorder_Partition >();
            for( auto it = mapper.gridView().template begin< dimension, Dune::InteriorBorder_Partition >(); it != end; ++it )
            {
              const auto vertex = *it ;
              values[ mapper( vertex ) ] = dataMap( vertex );
            }
            return xmlDataArray( name, values );
          } );
      }

      template< class Data >
      void addCellData ( const std::string &name, const std::vector< Data > &data )
      {
        addCellData< Data >( name, [ &data ] ( const Mapper &, const std::string &name ) { return xmlDataArray( name, data ); } );
      }

      template< class DataMap >
      void addCellData ( const std::string &name, DataMap dataMap )
      {
        typedef typename std::decay< typename std::result_of< DataMap( const Element & ) >::type >::type Data;
        addCellData< Data >( name, [ dataMap ] ( const Mapper &mapper, const std::string &name ) {
            std::vector< Data > values( mapper.size() );
            //for( const Element &element : elements( mapper.gridView(), Partitions::interiorBorder ) )
            const auto end = mapper.gridView().template end< 0, Dune::InteriorBorder_Partition >();
            for( auto it = mapper.gridView().template begin< 0, Dune::InteriorBorder_Partition   >(); it != end; ++it )
            {
              const auto element = *it ;
              values[ mapper( element ) ] = dataMap( element );
            }
            return xmlDataArray( name, values );
          } );
      }

      void write ( const std::string &name ) const;

    private:
      template< class Data >
      void addPointData ( const std::string &name, ToXML toXML )
      {
        pointData_.emplace_back( name, VTKDataType< Data >::components(), std::move( toXML ) );
      }

      template< class Data >
      void addCellData ( const std::string &name, ToXML toXML )
      {
        cellData_.emplace_back( name, VTKDataType< Data >::components(), std::move( toXML ) );
      }

      template <int dim, class Grid, class IndexSet>
      int addFaceVertices( const Dune::Entity< 1, dim, const Grid, PolyhedralGridEntity >& face,
                           const IndexSet& indexSet,
                           std::vector< int >& faces ) const
      {
        int offset = 0;
        const int nVxFace = face.impl().subEntities( dimension );
        faces.push_back( nVxFace );
        ++offset ;
        for( int i=0; i<nVxFace; ++i )
        {
          const int vxIndex = indexSet.subIndex( face, i, dimension );
          faces.push_back( vxIndex );
          ++offset ;
        }
        return offset;
      }

      template <class Entity, class IndexSet>
      int addFaceVertices( const Entity& element,
                           const IndexSet& indexSet,
                           std::vector< int >& faces ) const
      {
        DUNE_THROW(Dune::NotImplemented,"addFaceVertices not implemented");
      }

      std::vector< GlobalCoordinate > coordinates () const
      {
        std::vector< GlobalCoordinate > values( pointMapper_.size() );
        //for( const Vertex &vertex : vertices( pointMapper_.gridView(), Partitions::interiorBorder ) )
        const auto end = pointMapper_.gridView().template end< dimension, Dune::InteriorBorder_Partition >();
        for( auto it = pointMapper_.gridView().template begin< dimension, Dune::InteriorBorder_Partition >(); it != end; ++it )
        {
          const auto vertex = *it ;
          values[ pointMapper_( vertex ) ] = vertex.geometry().center();
        }
        return std::move( values );
      }

      XMLElement xmlConnectivity ( const std::vector< int > &offsets ) const;
      XMLElement xmlFaces        ( std::vector< int > &offsets ) const;

      template< class Data >
      static XMLElement xmlDataArray ( const std::string &name, const std::vector< Data > &data );

      XMLElement xmlDataVectors ( const Mapper &mapper, std::string tag, const DataVectorList &dataVectorList ) const;

      XMLElement xmlPiece () const;

      XMLElement xmlTypes () const;

      DataVectorList pointData_, cellData_;
      mutable Mapper pointMapper_, elementMapper_;
    };



    // Implementation of VTKPolygonWriter
    // ----------------------------------

    template< class GridView >
    inline void VTKPolygonWriter< GridView >::write ( const std::string &name ) const
    {
      pointMapper_.update();
      elementMapper_.update();

      XMLElement vtkFile( "VTKFile" );
      vtkFile += XMLAttribute( "type", "UnstructuredGrid" );
      vtkFile += XMLAttribute( "version", "0.1" );
      vtkFile += XMLAttribute( "byte_order", "LittleEndian" );

      vtkFile += makeXMLElement( "UnstructuredGrid", xmlPiece() );

      writeXML( name + ".vtu", vtkFile );
    }


    template< class GridView >
    inline XMLElement VTKPolygonWriter< GridView >::xmlConnectivity ( const std::vector< int > &offsets ) const
    {
      std::vector< int > connectivity( offsets.empty() ? 0u : offsets.back() );
      //for( const Element &element : elements( pointMapper_.gridView(), Partitions::interiorBorder ) )
      const auto end = pointMapper_.gridView().template end< 0, Dune::InteriorBorder_Partition >();
      for( auto it = pointMapper_.gridView().template begin< 0, Dune::InteriorBorder_Partition  >(); it != end; ++it )
      {
        const auto element = *it ;
        const std::size_t index = elementMapper_( element );

        const std::size_t begin = (index > 0u ? offsets[ index-1 ] : 0u);
        const std::size_t end = offsets[ index ];

        assert( end - begin == element.subEntities( dimension ) );
        for( std::size_t i = begin; i < end; ++i )
          connectivity[ i ] = pointMapper_( element.template subEntity< dimension >( i - begin ) );
        // swap 2 and 3 for quads and hexas
        if( element.type().isCube() )
          std::swap( connectivity[ begin+2 ], connectivity[ begin+3 ] );
        // swap 6 and 7 for hexas, otherwise viz looks wrong
        if( element.type().isHexahedron() )
          std::swap( connectivity[ begin+6 ], connectivity[ begin+7 ] );
      }
      return xmlDataArray( "connectivity", connectivity );
    }

    template< class GridView >
    inline XMLElement VTKPolygonWriter< GridView >::xmlFaces ( std::vector< int > &faceOffsets ) const
    {
      std::vector< int > faces;
      faces.reserve( faceOffsets.size() * 50 );

      //for( const Element &element : elements( pointMapper_.gridView(), Partitions::interiorBorder ) )

      std::vector<int> vertices;
      vertices.reserve( 30 );

      size_t offset = 0 ; // also count the number of faces
      const auto end = pointMapper_.gridView().template end< 0, Dune::InteriorBorder_Partition >();
      const auto& indexSet =  pointMapper_.gridView().indexSet();
      for( auto it = pointMapper_.gridView().template begin< 0, Dune::InteriorBorder_Partition  >(); it != end; ++it )
      {
        const auto element = *it ;

        const Dune::GeometryType geomType = element.type();
        const auto& refElem = Dune::ReferenceElements< ctype, dimension > :: general( geomType );

        // get number of local faces
        const std::size_t nFaces = element.subEntities( 1 );
        faces.push_back( nFaces );
        ++offset;
        for( std::size_t fce = 0; fce < nFaces; ++ fce )
        {
          if( geomType.isNone() )
          {
            const auto face = element.template subEntity< 1 > ( fce );
            offset += addFaceVertices( face, indexSet, faces );
          }
          else
          {
            // get number of vertices for this face
            const size_t nVxFace = refElem.size( fce, 1, dimension );
            faces.push_back( nVxFace );
            vertices.clear();
            ++offset ;
            for( size_t j = 0; j < nVxFace; ++j )
            {
              const int vx = refElem.subEntity( fce, 1, j, dimension );
              const auto vertex = element.template subEntity< dimension > ( vx );
              size_t vxIndex = pointMapper_( vertex );
              vertices.push_back( vxIndex );
              ++offset ;
            }

            // swap order of face vertices which needs to be clockwise for ParaView
            if( geomType.isCube() )
              std::swap( vertices[ 2 ], vertices[ 3 ] );

            for( size_t j = 0; j < nVxFace; ++j )
              faces.push_back( vertices[ j ] );
          }
        }

        const std::size_t index = elementMapper_( element );
        assert( index < faceOffsets.size() );
        // store face offset for this element
        faceOffsets[ index ] = offset ;
      }
      return xmlDataArray( "faces", faces );
    }


    template < class GridView >
    template< class Data >
    inline XMLElement VTKPolygonWriter< GridView >::xmlDataArray ( const std::string &name, const std::vector< Data > &data )
    {
      std::string content;
      std::size_t linelength = 0;
      for( const Data &item : data )
      {
        const std::string s = VTKDataType< Data >::toString( item );
        const std::size_t sz = s.size();
        if( linelength > 0 )
        {
          if( linelength + sz + 1 > 72 )
          {
            content.push_back( '\n' );
            linelength = 0;
          }
          else
          {
            content.push_back( ' ' );
            ++linelength;
          }
        }
        content += s;
        linelength += sz;
      }

      XMLElement xml( "DataArray", std::move( content ) );
      xml += XMLAttribute( "type", VTKDataType< Data >::type() );
      xml += XMLAttribute( "NumberOfComponents", VTKDataType< Data >::components() );
      xml += XMLAttribute( "Name", name );
      xml += XMLAttribute( "format", "ascii" );
      return xml;
    }


    template< class GridView >
    inline XMLElement VTKPolygonWriter< GridView >
      ::xmlDataVectors ( const Mapper &mapper, std::string tag, const DataVectorList &dataVectorList ) const
    {
      std::string scalars, vectors, tensors;
      for( const auto &dataVector : dataVectorList )
      {
        const std::string &name = std::get< 0 >( dataVector );
        const int components = std::get< 1 >( dataVector );
        if( components == 1 )
          scalars += std::string( scalars.empty() ? "" : "," ) + name;
        else if( components == 3 )
          vectors += std::string( vectors.empty() ? "" : "," ) + name;
        else
          tensors += std::string( tensors.empty() ? "" : "," ) + name;
      }

      XMLElement xml( std::move( tag ) );
      if( !scalars.empty() )
        xml += XMLAttribute( "Scalars", std::move( scalars ) );
      if( !vectors.empty() )
        xml += XMLAttribute( "Vectors", std::move( vectors ) );
      if( !tensors.empty() )
        xml += XMLAttribute( "Tensors", std::move( tensors ) );

      for( const auto &dataVector : dataVectorList )
        xml += std::get< 2 >( dataVector )( mapper, std::get< 0 >( dataVector ) );

      return xml;
    }


    template< class GridView >
    inline XMLElement VTKPolygonWriter< GridView >::xmlPiece () const
    {
      const std::size_t numElements = elementMapper_.size();
      std::vector< int > offsets( numElements );
      //for( const Element &element : elements( pointMapper_.gridView(), Partitions::interiorBorder ) )
      const auto end = pointMapper_.gridView().template end< 0, Dune::InteriorBorder_Partition >();
      for( auto it = pointMapper_.gridView().template begin< 0, Dune::InteriorBorder_Partition >(); it != end; ++it )
      {
        const auto element = *it ;
        offsets[ elementMapper_( element ) ] = element.subEntities( dimension );
      }
      for( std::size_t i = 1; i < numElements; ++i )
        offsets[ i ] += offsets[ i-1 ];

      XMLElement xml( "Piece" );
      xml += XMLAttribute( "NumberOfPoints", pointMapper_.size() );
      xml += XMLAttribute( "NumberOfCells", elementMapper_.size() );

      xml += xmlDataVectors( pointMapper_, "PointData", pointData_ );
      xml += xmlDataVectors( elementMapper_, "CellData", cellData_ );

      xml += makeXMLElement( "Points", xmlDataArray( "Coordinates", coordinates() ) );
      if( dimension > 2 )
      {
        std::vector< int > faceOffsets( numElements, int(0) );
        auto xmlFaceArray = xmlFaces( faceOffsets );
        auto xmlFaceOffsets = xmlDataArray( "faceoffsets", faceOffsets );
        xml += makeXMLElement( "Cells", xmlConnectivity( offsets ), xmlDataArray( "offsets", offsets ), xmlTypes(), xmlFaceArray, xmlFaceOffsets );
      }
      else
      {
        xml += makeXMLElement( "Cells", xmlConnectivity( offsets ), xmlDataArray( "offsets", offsets ), xmlTypes() );
      }
      return xml;
    }


    template< class GridView >
    inline XMLElement VTKPolygonWriter< GridView >::xmlTypes () const
    {
      const int type = dimension == 2 ? 7 : 42; // VTK_POLYGON = 7,  VTK_POLYHEDRON = 42
      std::vector< unsigned char > types( elementMapper_.size(), type );
      return xmlDataArray( "types", types );
    }

  } // namespace Viz

} // namespace Dune

#endif // #ifndef DUNE_VIZ_WRITER_VTKPOLYGONWRITER_HH
