#ifndef DUNE_VIZ_WRITER_VTK_DATATYPE_HH
#define DUNE_VIZ_WRITER_VTK_DATATYPE_HH

#include <dune/common/fvector.hh>
#include <dune/common/fmatrix.hh>

namespace Dune
{

  namespace Viz
  {

    // VTKDataType
    // -----------

    template< class Data >
    struct VTKDataType;

    template<>
    struct VTKDataType< float >
    {
      static std::string type () { return "Float32"; }
      static int components () { return 1; }

      static std::string toString ( const float &value )
      {
        std::ostringstream ret;
        ret << std::scientific << std::setprecision( 8 ) << value;
        return ret.str();
      }
    };

    template<>
    struct VTKDataType< double >
    {
      static std::string type () { return "Float64"; }
      static int components () { return 1; }

      static std::string toString ( const double &value )
      {
        std::ostringstream ret;
        ret << std::scientific << std::setprecision( 16 ) << value;
        return ret.str();
      }
    };

    template<>
    struct VTKDataType< int >
    {
      static std::string type () { return "Int32"; }
      static int components () { return 1; }

      static std::string toString ( const int &value )
      {
        std::ostringstream ret;
        ret << value;
        return ret.str();
      }
    };

    template<>
    struct VTKDataType< unsigned char >
    {
      static std::string type () { return "UInt8"; }
      static int components () { return 1; }

      static std::string toString ( const unsigned char &value )
      {
        std::ostringstream ret;
        ret << (int)value;
        return ret.str();
      }
    };



    // VTKDataType for FieldMatrix
    // ---------------------------

    template< class ctype >
    struct VTKDataType< FieldVector< ctype, 1 > >
    {
      static std::string type () { return VTKDataType< ctype >::type(); }
      static int components () { return VTKDataType< ctype >::components(); }

      static std::string toString ( const FieldVector< ctype, 1 > &value )
      {
        return VTKDataType< ctype >::toString( value[ 0 ] );
      }
    };

    template< class ctype >
    struct VTKDataType< FieldVector< ctype, 2 > >
    {
      static std::string type () { return VTKDataType< ctype >::type(); }
      static int components () { return 3; }

      static std::string toString ( const FieldVector< ctype, 2 > &value )
      {
        std::string ret( VTKDataType< ctype >::toString( value[ 0 ] ) );
        ret += std::string( " " ) + VTKDataType< ctype >::toString( value[ 1 ] );
        ret += std::string( " " ) + VTKDataType< ctype >::toString( ctype( 0 ) );
        return ret;
      }
    };

    template< class ctype >
    struct VTKDataType< FieldVector< ctype, 3 > >
    {
      static std::string type () { return VTKDataType< ctype >::type(); }
      static int components () { return 3; }

      static std::string toString ( const FieldVector< ctype, 3 > &value )
      {
        std::string ret( VTKDataType< ctype >::toString( value[ 0 ] ) );
        ret += std::string( " " ) + VTKDataType< ctype >::toString( value[ 1 ] );
        ret += std::string( " " ) + VTKDataType< ctype >::toString( value[ 2 ] );
        return ret;
      }
    };



    // VTKDataType for FieldMatrix
    // ---------------------------

    template< class ctype >
    struct VTKDataType< FieldMatrix< ctype, 1, 1 > >
    {
      static std::string type () { return VTKDataType< ctype >::type(); }
      static int components () { return 1; }

      static std::string toString ( const FieldMatrix< ctype, 1, 1 > &value )
      {
        return VTKDataType< ctype >::toString( value[ 0 ][ 0 ] );
      }
    };

    template< class ctype >
    struct VTKDataType< FieldMatrix< ctype, 2, 2 > >
    {
      static std::string type () { return VTKDataType< ctype >::type(); }
      static int components () { return 9; }

      static std::string toString ( const FieldMatrix< ctype, 2, 2 > &value )
      {
        std::string ret( VTKDataType< ctype >::toString( value[ 0 ][ 0 ] ) );
        ret += std::string( " " ) + VTKDataType< ctype >::toString( value[ 0 ][ 1 ] );
        ret += std::string( " " ) + VTKDataType< ctype >::toString( ctype( 0 ) );
        ret += std::string( " " ) + VTKDataType< ctype >::toString( value[ 1 ][ 0 ] );
        ret += std::string( " " ) + VTKDataType< ctype >::toString( value[ 1 ][ 1 ] );
        ret += std::string( " " ) + VTKDataType< ctype >::toString( ctype( 0 ) );
        ret += std::string( " " ) + VTKDataType< ctype >::toString( ctype( 0 ) );
        ret += std::string( " " ) + VTKDataType< ctype >::toString( ctype( 0 ) );
        ret += std::string( " " ) + VTKDataType< ctype >::toString( ctype( 0 ) );
        return ret;
      }
    };

    template< class ctype >
    struct VTKDataType< FieldMatrix< ctype, 3, 3 > >
    {
      static std::string type () { return VTKDataType< ctype >::type(); }
      static int components () { return 9; }

      static std::string toString ( const FieldMatrix< ctype, 3, 3 > &value )
      {
        std::string ret( VTKDataType< ctype >::toString( value[ 0 ][ 0 ] ) );
        ret += std::string( " " ) + VTKDataType< ctype >::toString( value[ 0 ][ 1 ] );
        ret += std::string( " " ) + VTKDataType< ctype >::toString( value[ 0 ][ 2 ] );
        ret += std::string( " " ) + VTKDataType< ctype >::toString( value[ 1 ][ 0 ] );
        ret += std::string( " " ) + VTKDataType< ctype >::toString( value[ 1 ][ 1 ] );
        ret += std::string( " " ) + VTKDataType< ctype >::toString( value[ 1 ][ 2 ] );
        ret += std::string( " " ) + VTKDataType< ctype >::toString( value[ 2 ][ 0 ] );
        ret += std::string( " " ) + VTKDataType< ctype >::toString( value[ 2 ][ 1 ] );
        ret += std::string( " " ) + VTKDataType< ctype >::toString( value[ 2 ][ 2 ] );
        return ret;
      }
    };

  } // namespace Viz

} // namespace Dune

#endif // #ifndef DUNE_VIZ_WRITER_VTK_DATATYPE_HH
