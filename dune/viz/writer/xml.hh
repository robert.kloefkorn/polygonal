#ifndef DUNE_VIZ_WRITER_XML_HH
#define DUNE_VIZ_WRITER_XML_HH

#include <fstream>
#include <list>
#include <map>
#include <string>
#include <utility>

namespace Dune
{

  namespace Viz
  {

    // XMLFileWriter
    // -------------

    class XMLFileWriter
    {
      typedef XMLFileWriter This;

    public:
      explicit XMLFileWriter ( const std::string &fileName )
        : stream_( fileName ), indent_( 0 )
      {
        stream_ << "<?xml version=\"1.0\"?>" << std::endl;
      }

      This &beginElement ( const std::string &tag, const std::map< std::string, std::string > &attributes )
      {
        indent( indent_++ );
        stream_ << "<" << tag;
        for( const auto &attribute : attributes )
          stream_ << " " << attribute.first << "=\"" << attribute.second << "\"";
        stream_ << ">" << std::endl;
        return *this;
      }

      This &emptyElement ( const std::string &tag, const std::map< std::string, std::string > &attributes )
      {
        indent( indent_ );
        stream_ << "<" << tag;
        for( const auto &attribute : attributes )
          stream_ << " " << attribute.first << "=\"" << attribute.second << "\"";
        stream_ << " />" << std::endl;
        return *this;
      }

      This &endElement ( const std::string &tag )
      {
        indent( --indent_ );
        stream_ << "</" << tag << ">" << std::endl;
        return *this;
      }

      This &data ( const std::string &data )
      {
        const std::size_t n = data.size();
        for( std::size_t i = 0; i < n; )
        {
          std::size_t j = i;
          for( ; (j < n) && data[ j ] != '\n'; ++j )
            continue;
          if( j-i > 0u )
          {
            indent( indent_ );
            stream_.write( data.data() + i, j-i );
            stream_ << std::endl;
          }
          i = j+1;
        }
        return *this;
      }

    private:
      void indent ( std::size_t indent )
      {
        for( std::size_t i = 0; i < indent; ++i )
          stream_ << "  ";
      }

      std::ofstream stream_;
      std::size_t indent_;
    };



    // XMLAttribute
    // ------------

    struct XMLAttribute
    {
      XMLAttribute ( std::string n, std::string v ) : name( std::move( n ) ), value( std::move( v ) ) {}

      XMLAttribute ( std::string n, int v ) : name( std::move( n ) ), value( std::to_string( v ) ) {}
      XMLAttribute ( std::string n, long v ) : name( std::move( n ) ), value( std::to_string( v ) ) {}
      XMLAttribute ( std::string n, long long v ) : name( std::move( n ) ), value( std::to_string( v ) ) {}
      XMLAttribute ( std::string n, unsigned int v ) : name( std::move( n ) ), value( std::to_string( v ) ) {}
      XMLAttribute ( std::string n, unsigned long v ) : name( std::move( n ) ), value( std::to_string( v ) ) {}
      XMLAttribute ( std::string n, unsigned long long v ) : name( std::move( n ) ), value( std::to_string( v ) ) {}

      std::string name, value;
    };



    // XMLElement
    // ----------

    class XMLElement
    {
      typedef XMLElement This;

    public:
      explicit XMLElement ( std::string tag, std::string data = std::string() )
        : tag_( std::move( tag ) ), data_( std::move( data ) )
      {}

      template< class T >
      This &operator+= ( T &&value ) { add( value ); return *this; }

      void add ( const XMLAttribute &attribute ) { attributes_[ attribute.name ] = attribute.value; }
      void add ( XMLAttribute &&attribute ) { attributes_[ attribute.name ] = std::move( attribute.value ); }
      void add ( const XMLElement &element ) { elements_.push_back( element ); }
      void add ( XMLElement &&element ) { elements_.push_back( std::move( element ) ); }

      template< class X, class Y, class... Z >
      void add ( X &&x, Y &&y, Z &&... z )
      {
        add( std::forward< X >( x ) );
        add( std::forward< Y >( y ), std::forward< Z >( z )... );
      }

      void write ( XMLFileWriter &writer ) const
      {
        if( !(data_.empty() && elements_.empty()) )
        {
          writer.beginElement( tag_, attributes_ );
          for( const XMLElement &element : elements_ )
            element.write( writer );
          writer.data( data_ );
          writer.endElement( tag_ );
        }
        else
          writer.emptyElement( tag_, attributes_ );
      }

    private:
      std::string tag_, data_;
      std::map< std::string, std::string > attributes_;
      std::list< XMLElement > elements_;
    };



    // makeXMLElement
    // --------------

    template< class... Args >
    inline static XMLElement makeXMLElement ( const std::string &tag, Args &&... args )
    {
      XMLElement xmlElement( tag );
      xmlElement.add( std::forward< Args >( args )... );
      return xmlElement;
    }



    // writeXML
    // --------

    inline static void writeXML ( const std::string &fileName, const XMLElement &root )
    {
      XMLFileWriter writer( fileName );
      root.write( writer );
    }

  } // namespace Viz

} // namespace Dune

#endif // #ifndef DUNE_VIZ_WRITER_XML_HH
