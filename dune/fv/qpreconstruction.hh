#ifndef DUNE_FV_QPRECONSTRUCTION_HH
#define DUNE_FV_QPRECONSTRUCTION_HH

#include <cassert>
#include <cstddef>

#include <utility>
#include <vector>

#include <dune/common/dynvector.hh>
#include <dune/common/fmatrix.hh>
#include <dune/common/fvector.hh>

#include <dune/grid/common/gridenums.hh>
#include <dune/grid/common/rangegenerators.hh>

#include <dune/common/getreference.hh>

#include <dune/grid/utility/vectorcommdatahandle.hh>

#if HAVE_DUNE_OPTIM
#include <dune/optim/common/smallobject.hh>
#include <dune/optim/constraint/linear.hh>
#include <dune/optim/qp.hh>
#include <dune/optim/qp/reducedgauss.hh>

#include <dune/fv/function/piecewiselinear.hh>

namespace Dune
{

  namespace FV
  {

    // QPReconstruction
    // ----------------

    template< class GV, class SV, class BV >
    class QPReconstruction
    {
      typedef QPReconstruction< GV, SV, BV > This;

    public:
      typedef GV GridView;
      typedef SV StateVector;
      typedef BV BoundaryValue;

      typedef FieldVector< typename GridView::ctype, GridView::dimensionworld > GlobalCoordinate;

      typedef typename GridView::Intersection Intersection;

      typedef typename FieldTraits< StateVector >::field_type Field;
      typedef typename FieldTraits< StateVector >::real_type Real;
      typedef FieldMatrix< Field, StateVector::dimension, GlobalCoordinate::dimension > Jacobian;

      template< class Mapper, class Vector >
      using Reconstruction = PiecewiseLinearFunction< GridView, Mapper, Vector, std::vector< Jacobian > >;

    private:
      typedef Optim::LinearConstraint< GlobalCoordinate > Constraint;
      typedef std::vector< Constraint > Constraints;

      typedef Optim::ReducedGaussQP< Field > QP;
      typedef Optim::ActiveSetStrategy< QP, false > ActiveSetStrategy;

    public:
      QPReconstruction ( const GridView &gridView, BoundaryValue boundaryValue, Real tolerance )
        : gridView_( gridView ), boundaryValue_( std::move( boundaryValue ) ), activeSetStrategy_( std::move( tolerance ) )
      {}

      template< class Mapper, class Vector >
      Reconstruction< Mapper, Vector > operator() ( const Mapper &mapper, Vector u ) const
      {
        std::vector< Jacobian > du;
        (*this)( mapper, getReference( u ), du );
        return Reconstruction< Mapper, Vector >( mapper, std::move( u ), std::move( du ) );
      }

      template< class Mapper, class Vector >
      void operator () ( const Mapper &mapper, const Vector &u, std::vector< Jacobian > &du ) const
      {
        du.resize( u.size() );

        std::vector< std::pair< GlobalCoordinate, StateVector > > differences;
        Constraints constraints;
        Dune::DynamicVector< Field > lambda;

        const auto end = gridView().template end< 0, Dune::InteriorBorder_Partition >();
        for( auto it = gridView().template begin< 0, Dune::InteriorBorder_Partition>(); it != end; ++it )
        //for( const auto element : elements( gridView(), Partitions::interiorBorder ) )
        {
          const auto element = *it;

          const std::size_t elIndex = mapper.index( element );
          const GlobalCoordinate elCenter = element.geometry().center();

          differences.clear();
          Dune::FieldMatrix< Field, GlobalCoordinate::dimension, GlobalCoordinate::dimension > hessianInverse( 0 );
          Dune::FieldMatrix< Field, StateVector::dimension, GlobalCoordinate::dimension > negGradient( 0 );

          const auto iend = gridView().iend( element );
          for( auto iit = gridView().ibegin( element ); iit != iend; ++iit )
          //for( const auto intersection : intersections( gridView(), element ) )
          {
            const auto intersection = *iit;

            if( intersection.boundary() )
            {
              const GlobalCoordinate iCenter = intersection.geometry().center();
              const GlobalCoordinate iNormal = intersection.centerUnitOuterNormal();
              const StateVector du = boundaryValue_( intersection, iCenter, iNormal, u[ elIndex ] ) - u[ elIndex ];
              const GlobalCoordinate dx = iCenter - elCenter;
              for( int i = 0; i < GlobalCoordinate::dimension; ++i )
                hessianInverse[ i ].axpy( dx[ i ], dx );
              for( int i = 0; i < StateVector::dimension; ++i )
                negGradient[ i ].axpy( du[ i ], dx );
              differences.emplace_back( dx, du );
            }
            else if( intersection.neighbor() )
            {
              const auto neighbor = intersection.outside();
              const GlobalCoordinate dx = neighbor.geometry().center() - elCenter;
              const StateVector du = u[ mapper.index( neighbor ) ] - u[ elIndex ];
              for( int i = 0; i < GlobalCoordinate::dimension; ++i )
                hessianInverse[ i ].axpy( dx[ i ], dx );
              for( int i = 0; i < StateVector::dimension; ++i )
                negGradient[ i ].axpy( du[ i ], dx );
              differences.emplace_back( dx, du );
            }
          }

          hessianInverse.invert();

          // reserve memory for constraints
          const std::size_t numConstraints = differences.size();
          constraints.resize( 2u*numConstraints );
          lambda.resize( 2u*numConstraints );
          for( int j = 0; j < StateVector::dimension; ++j )
          {
            auto problem = QP::problem( hessianInverse, negGradient[ j ] );

            // initialize constraints
            for( std::size_t i = 0u; i < numConstraints; ++i )
            {
              const Field sign = (differences[ i ].second[ j ] >= 0 ? 1 : -1);

              constraints[ 2*i ].normal() = differences[ i ].first;
              constraints[ 2*i ].normal() *= sign;
              constraints[ 2*i ].rhs() = sign*differences[ i ].second[ j ];

              constraints[ 2*i+1 ].normal() = constraints[ 2*i ].normal();
              constraints[ 2*i+1 ].normal() *= Field( -1 );
              constraints[ 2*i+1 ].rhs() = 0;
            }

            // solve
            du[ elIndex ][ j ] = 0;
            activeSetStrategy_( problem, constraints, du[ elIndex ][ j ], lambda );
          }
        }

        //auto handle = vectorCommDataHandle( mapper, du, [] ( Jacobian a, Jacobian b ) { return b; } );
        //gridView().communicate( handle, InteriorBorder_All_Interface, ForwardCommunication );
      }

      const GridView &gridView () const { return gridView_; }

    private:
      GridView gridView_;
      BoundaryValue boundaryValue_;
      ActiveSetStrategy activeSetStrategy_;
    };



    // qpReconstruction
    // ----------------

    template< class SV, class GV, class BV >
    inline static QPReconstruction< GV, SV, BV > qpReconstruction ( const GV &gridView, BV boundaryValue, typename FieldTraits< SV >::real_type tolerance )
    {
      return QPReconstruction< GV, SV, BV >( gridView, std::move( boundaryValue ), std::move( tolerance ) );
    }

  } // namespace FV

} // namespace Dune

#endif // #if HAVE_DUNE_OPTIM

#endif // #ifndef DUNE_FV_QPRECONSTRUCTION_HH
