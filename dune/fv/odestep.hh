#ifndef DUNE_FV_ODESTEP_HH
#define DUNE_FV_ODESTEP_HH

#include <algorithm>
#include <functional>
#include <vector>

#include <dune/common/ftraits.hh>

namespace Dune
{

  namespace FV
  {

    // AbstractOdeStep
    // ---------------

    template< class SV >
    using AbstractOdeStep = std::function< double( double, const std::vector< SV > &, double, std::vector< SV > & ) >;



    // ExplicitEulerOdeStep
    // --------------------

    template< class RHS, class SV >
    class ExplicitEulerOdeStep
    {
      typedef ExplicitEulerOdeStep< RHS, SV > This;

    public:
      typedef RHS RightHandSide;
      typedef SV StateVector;

      typedef std::vector< StateVector > Vector;

      explicit ExplicitEulerOdeStep ( RightHandSide rightHandSide )
        : rightHandSide_( std::move( rightHandSide ) )
      {}

      double operator() ( double t, const Vector &u, double dt, Vector &w ) const
      {
        assert( u.size() == w.size() );

        const double maxdt = rightHandSide_( t, u, w );

        std::transform( w.begin(), w.end(), u.begin(), w.begin(), [ dt ] ( StateVector w, StateVector u ) { u.axpy( dt, w ); return u; } );

        return maxdt;
      }

    private:
      RightHandSide rightHandSide_;
    };



    // explicitEulerOdeStep
    // --------------------

    template< class SV, class RHS >
    inline static ExplicitEulerOdeStep< typename std::decay< RHS >::type, SV >
    explicitEulerOdeStep ( RHS &&rightHandSide ) noexcept
    {
      return ExplicitEulerOdeStep< typename std::decay< RHS >::type, SV >( std::forward< RHS >( rightHandSide ) );
    }



    // HeunOdeStep
    // -----------

    template< class RHS, class SV >
    class HeunOdeStep
    {
      typedef HeunOdeStep< RHS, SV > This;

    public:
      typedef RHS RightHandSide;
      typedef SV StateVector;

      typedef std::vector< StateVector > Vector;

      explicit HeunOdeStep ( RightHandSide rightHandSide )
        : rightHandSide_( std::move( rightHandSide ) )
      {}

      double operator() ( double t, const Vector &u, double dt, Vector &w ) const
      {
        assert( u.size() == w.size() );

        k_[ 0 ].resize( w.size() );
        const double maxdt0 = rightHandSide_( t, u, k_[ 0 ] );

        std::transform( u.begin(), u.end(), k_[ 0 ].begin(), w.begin(), [ dt ] ( StateVector u, StateVector k ) { u.axpy( dt, k ); return u; } );
        k_[ 1 ].resize( w.size() );
        const double maxdt1 = rightHandSide_( t + dt, w, k_[ 1 ] );

        std::transform( u.begin(), u.end(), k_[ 0 ].begin(), w.begin(), [ dt ] ( StateVector u, StateVector k ) { u.axpy( dt / 2.0, k ); return u; } );
        std::transform( w.begin(), w.end(), k_[ 1 ].begin(), w.begin(), [ dt ] ( StateVector u, StateVector k ) { u.axpy( dt / 2.0, k ); return u; } );

        return std::max( maxdt0, maxdt1 );
      }

    private:
      RightHandSide rightHandSide_;
      mutable std::array< Vector, 2 > k_;
    };



    // heunOdeStep
    // -----------

    template< class SV, class RHS >
    inline static HeunOdeStep< typename std::decay< RHS >::type, SV >
    heunOdeStep ( RHS &&rightHandSide ) noexcept
    {
      return HeunOdeStep< typename std::decay< RHS >::type, SV >( std::forward< RHS >( rightHandSide ) );
    }

  } // namespace FV

} // namespace Dune

#endif // #ifndef DUNE_FV_ODESTEP_HH
