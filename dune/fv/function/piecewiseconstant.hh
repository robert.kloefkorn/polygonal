#ifndef DUNE_FV_FUNCTION_PIECEWISECONSTANT_HH
#define DUNE_FV_FUNCTION_PIECEWISECONSTANT_HH

#include <type_traits>
#include <utility>

#include <dune/common/fvector.hh>

#include <dune/common/getreference.hh>

namespace Dune
{

  namespace FV
  {

    // PiecewiseConstantFunction
    // -------------------------

    template< class GridView, class Mapper, class Vector >
    class PiecewiseConstantFunction
    {
      typedef PiecewiseConstantFunction< GridView, Mapper, Vector > This;

    public:
      typedef FieldVector< typename GridView::ctype, GridView::dimension > GlobalCoordinate;

      typedef typename GridView::template Codim< 0 >::Entity Entity;
      typedef typename GridView::template Codim< 0 >::Geometry Geometry;

      typedef typename getReferredType< const Vector & >::value_type Value;

    private:
      struct LocalFunction
      {
        explicit LocalFunction ( Value value ) : value_( std::move( value ) ) {}

        Value operator() ( const GlobalCoordinate &x ) const noexcept { return value_; }

      private:
        Value value_;
      };

    public:
      PiecewiseConstantFunction ( const Mapper &mapper, Vector vector ) noexcept
        : mapper_( mapper ), vector_( std::move( vector ) )
      {}

      friend LocalFunction localFunction ( const This &f, const Entity &entity )
      {
        return LocalFunction( getReference( f.vector_ )[ f.mapper_.index( entity ) ] );
      }

      friend LocalFunction localFunction ( const This &f, const Entity &entity, const Geometry &geometry )
      {
        return localFunction( f, entity );
      }

      friend typename FieldTraits< Value >::field_type l1Norm ( const GridView &gridView, const This &f )
      {
        using std::sqrt;
        typename FieldTraits< Value >::field_type l1Norm( 0 );
        const auto end = gridView.template end< 0, Dune::InteriorBorder_Partition >();
        for( auto it = gridView.template begin< 0, Dune::InteriorBorder_Partition >(); it != end; ++it )
        //for( const Entity entity : elements( gridView, Partitions::interiorBorder ) )
        {
          const auto entity = *it ;
          const Value &value = getReference( f.vector_ )[ f.mapper_.index( entity ) ];
          l1Norm += entity.geometry().volume() * sqrt( dot( value, value ) );
        }
        return gridView.comm().sum( l1Norm );
      }

      friend typename FieldTraits< Value >::field_type l2Norm ( const GridView &gridView, const This &f )
      {
        using std::sqrt;
        typename FieldTraits< Value >::field_type l2Norm( 0 );
        const auto end = gridView.template end< 0, Dune::InteriorBorder_Partition >();
        for( auto it = gridView.template begin< 0, Dune::InteriorBorder_Partition >(); it != end; ++it )
        //for( const Entity entity : elements( gridView, Partitions::interiorBorder ) )
        {
          const auto entity = *it ;
          const Value &value = getReference( f.vector_ )[ f.mapper_.index( entity ) ];
          l2Norm += entity.geometry().volume() * dot( value, value );
        }
        return sqrt( gridView.comm().sum( l2Norm ) );
      }

    public:
      const Mapper &mapper_;
      Vector vector_;
    };



    // piecewiseConstantFunction
    // -------------------------

    template< class GridView, class Mapper, class Vector >
    inline static PiecewiseConstantFunction< GridView, Mapper, typename std::decay< Vector >::type >
    piecewiseConstantFunction ( const GridView &gridView, const Mapper &mapper, Vector vector ) noexcept
    {
      return PiecewiseConstantFunction< GridView, Mapper, typename std::decay< Vector >::type >( mapper, std::move( vector ) );
    }

  } // namespace FV

} // namespace Dune

#endif // #ifndef DUNE_FV_FUNCTION_PIECEWISECONSTANT_HH
