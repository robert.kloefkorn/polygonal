#ifndef DUNE_FV_FUNCTION_VTK_HH
#define DUNE_FV_FUNCTION_VTK_HH

#include <memory>
#include <string>
#include <utility>

#include <dune/common/fvector.hh>

#include <dune/grid/io/file/vtk/function.hh>

namespace Dune
{

  namespace FV
  {

    // VTKFunction
    // -----------

    template< class GridView, class Function >
    class VTKFunction final
      : public Dune::VTKFunction< GridView >
    {
      typedef Dune::VTKFunction< GridView > Base;

    public:
      typedef typename Base::Entity Entity;
      typedef typename Base::ctype ctype;

      using Base::dim;

      VTKFunction ( const GridView &gridView, Function function, std::string name )
        : function_( std::move( function ) ), name_( std::move( name ) )
      {}

      virtual int ncomps () const override { return decltype( ncomps( std::declval< typename Function::Value >() ) )::value; }

      virtual double evaluate ( int component, const Entity &entity, const Dune::FieldVector< ctype, dim > &x ) const override
      {
        const auto geometry = entity.geometry();
        return get( component, localFunction( function_, entity, geometry )( geometry.global( x ) ) );
      }

      virtual std::string name () const override { return name_; }

    private:
      template< class K, int n >
      static std::integral_constant< int, n > ncomps ( FieldVector< K, n > );

      template< class K >
      static std::integral_constant< int, 1 > ncomps( K );

      template< class K, int n >
      static double get ( int component, const FieldVector< K, n > &x )
      {
        return static_cast< double >( x[ component ] );
      }

      template< class K >
      static double get ( int component, const K &x )
      {
        return static_cast< double >( x );
      }

      Function function_;
      std::string name_;
    };


    // makeVTKFunction
    // ---------------

    template< class GridView, class Function >
    inline static std::shared_ptr< VTKFunction< GridView, Function > >
    makeVTKFunction ( const GridView &gridView, Function function, std::string name )
    {
      return std::make_shared< VTKFunction< GridView, Function > >( gridView, std::move( function ), std::move( name ) );
    }

  } // namespace FV

} // namespace Dune

#endif // #ifndef DUNE_FV_FUNCTION_VTK_HH
