#ifndef DUNE_FV_INTERPOLATE_HH
#define DUNE_FV_INTERPOLATE_HH

#include <type_traits>

#include <dune/common/fvector.hh>

#include <dune/grid/common/rangegenerators.hh>

#include <dune/grid/utility/vectorcommdatahandle.hh>

namespace Dune
{

  namespace FV
  {

    // interpolate
    // -----------

    template< class GridView, class Function, class Mapper >
    void interpolate ( const GridView &gridView, const Function &function, const Mapper &mapper,
                       std::vector< typename std::result_of< Function( FieldVector< typename GridView::ctype, GridView::dimensionworld > ) >::type > &values,
                       Partitions::InteriorBorder = Partitions::interiorBorder )
    {
      values.resize( mapper.size() );
      const auto end = gridView.template end< 0, Dune::InteriorBorder_Partition > ();
      for(auto it = gridView.template begin< 0, Dune::InteriorBorder_Partition >(); it != end; ++it)
      {
        const auto element = *it;
      //for( const auto element : Dune::elements( gridView, Partitions::interiorBorder ) )
        values[ mapper.index( element ) ] = function( element.geometry().center() );
      }

      typedef typename std::result_of< Function( FieldVector< typename GridView::ctype, GridView::dimensionworld > ) >::type Value;
      auto handle = vectorCommDataHandle( mapper, values, [] ( Value a, Value b ) { return b; } );
      gridView.communicate( handle, InteriorBorder_All_Interface, ForwardCommunication );
    }

    template< class GridView, class Function, class Mapper >
    void interpolate ( const GridView &gridView, const Function &function, const Mapper &mapper,
                       std::vector< typename std::result_of< Function( FieldVector< typename GridView::ctype, GridView::dimensionworld > ) >::type > &values,
                       Partitions::All )
    {
      values.resize( mapper.size() );
      const auto end = gridView.template end<0, Dune::InteriorBorder_Partition > ();
      for(auto it = gridView.template begin<0, Dune::InteriorBorder_Partition >(); it != end; ++it)
      {
        const auto element = *it;
      //for( const auto element : elements( gridView(), Partitions::all ) )
        values[ mapper.index( element ) ] = function( element.geometry().center() );
      }
    }

  } // namespace FV

} // namespace Dune

#endif // #ifndef DUNE_FV_INTERPOLATE_HH
