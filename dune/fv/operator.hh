#ifndef DUNE_FV_OPERATOR_HH
#define DUNE_FV_OPERATOR_HH

#include <cassert>
#include <cstddef>

#include <algorithm>
#include <vector>
#include <utility>

#include <dune/common/fvector.hh>

#include <dune/grid/common/gridenums.hh>
#include <dune/grid/common/rangegenerators.hh>

#include <dune/grid/utility/vectorcommdatahandle.hh>

namespace Dune
{

  namespace FV
  {

    // FiniteVolumeOperator
    // --------------------

    template< class GV, class NF, class BV >
    class FiniteVolumeOperator
    {
      typedef FiniteVolumeOperator< GV, NF, BV > This;

    public:
      typedef GV GridView;
      typedef NF NumericalFlux;
      typedef BV BoundaryValue;

      typedef FieldVector< typename GridView::ctype, GridView::dimensionworld > GlobalCoordinate;
      typedef typename GridView::Intersection Intersection;

      typedef typename NumericalFlux::AnalyticalFlux AnalyticalFlux;
      typedef typename NumericalFlux::StateVector StateVector;

      typedef typename FieldTraits< StateVector >::field_type Field;

      FiniteVolumeOperator ( GridView gridView, NumericalFlux numericalFlux, BoundaryValue boundaryValue )
        : gridView_( std::move( gridView ) ),
          numericalFlux_( std::move( numericalFlux ) ),
          boundaryValue_( std::move( boundaryValue ) )
      {}

      template< class Function, class Mapper >
      Field operator () ( const Function &u, const Mapper &mapper, std::vector< StateVector > &update ) const
      {
        update.resize( mapper.size() );
        std::fill( update.begin(), update.end(), 0 );

        std::vector< Field > waveSpeed( mapper.size(), 0 );
        const auto end = gridView().template end< 0, Dune::InteriorBorder_Partition >();
        for( auto it = gridView().template begin< 0, Dune::InteriorBorder_Partition >(); it != end; ++it )
        //for( const auto element : elements( gridView(), Partitions::interiorBorder ) )
        {
          const auto element = *it ;
          const std::size_t elIdx = mapper.index( element );

          const auto elGeo = element.geometry();
          const Field elWeight = (Field( 1 ) / elGeo.volume());

          const auto elU = localFunction( u, element, elGeo );

          const auto iend = gridView().iend( element );
          for( auto iit = gridView().ibegin( element ); iit != iend; ++iit )
          //for( const Intersection intersection : intersections( gridView(), element ) )
          {
            const auto intersection = *iit ;
            if( intersection.neighbor() )
            {
              const auto neighbor = intersection.outside();
              const std::size_t nbIdx = mapper.index( neighbor );

              // only evaluate fluxes from one side
              if( nbIdx > elIdx )
                continue;

              const auto nbGeo = neighbor.geometry();
              const Field nbWeight = (Field( 1 ) / nbGeo.volume());

              const auto nbU = localFunction( u, neighbor, nbGeo );

              const auto iGeo = intersection.geometry();
              const GlobalCoordinate iCenter = iGeo.center();
              GlobalCoordinate iNormal = intersection.centerUnitOuterNormal();
              iNormal *= iGeo.volume();

              const StateVector elValue = elU( iCenter );
              const StateVector nbValue = nbU( iCenter );
              //assert( physical( elValue ) && physical( nbValue ) );

              StateVector flux;
              const std::pair< Field, Field > ws = numericalFlux()( iCenter, iNormal, elValue, nbValue, flux );

              update[ elIdx ].axpy( -elWeight, flux );
              waveSpeed[ elIdx ] += std::max( ws.second, Field( 0 ) ) * elWeight;

              update[ nbIdx ].axpy( nbWeight, flux );
              waveSpeed[ nbIdx ] += std::max( -ws.first, Field( 0 ) ) * nbWeight;
            }
            else if( intersection.boundary() )
            {
              const auto iGeo = intersection.geometry();
              const GlobalCoordinate iCenter = iGeo.center();
              GlobalCoordinate iNormal = intersection.centerUnitOuterNormal();
              iNormal *= iGeo.volume();

              const StateVector elValue = elU( iCenter );
              const StateVector bndValue = boundaryValue()( intersection, iCenter, iNormal, elValue );
              //assert( physical( elValue ) && physical( bndValue ) );

              StateVector flux;
              const std::pair< Field, Field > ws = numericalFlux()( iCenter, iNormal, elValue, bndValue, flux );

              update[ elIdx ].axpy( -elWeight, flux );
              waveSpeed[ elIdx ] += std::max( ws.second, Field( 0 ) ) * elWeight;
            }
          }
        }

        auto handle = vectorCommDataHandle( mapper, update, [] ( StateVector a, StateVector b ) { return b; } );
        gridView().communicate( handle, InteriorBorder_All_Interface, ForwardCommunication );

        return Field( 1 ) / gridView().comm().max( *std::max_element( waveSpeed.begin(), waveSpeed.end() ) );
      }

      const GridView &gridView () const { return gridView_; }

      AnalyticalFlux analyticalFlux () const { return numericalFlux().analyticalFlux(); }
      const NumericalFlux &numericalFlux () const { return numericalFlux_; }

      const BoundaryValue &boundaryValue () const { return boundaryValue_; }

      //bool physical ( const StateVector &u ) const { return analyticalFlux().states().contains( u ); }

    private:
      GridView gridView_;
      NumericalFlux numericalFlux_;
      BoundaryValue boundaryValue_;
    };



    // finiteVolumeOperator
    // --------------------

    template< class GV, class NF, class BV >
    inline static FiniteVolumeOperator< GV, NF, BV >
    finiteVolumeOperator ( GV gridView, NF numericalFlux, BV boundaryValue )
    {
      return FiniteVolumeOperator< GV, NF, BV >( std::move( gridView ), std::move( numericalFlux ), std::move( boundaryValue ) );
    }

  } // namespace FV

} // namespace Dune

#endif // #ifndef DUNE_FV_OPERATOR_HH
