#ifndef DUNE_FV_LEASTSQUARESRECONSTRUCTION_HH
#define DUNE_FV_LEASTSQUARESRECONSTRUCTION_HH

#include <cassert>
#include <cstddef>

#include <utility>
#include <vector>

#include <dune/common/fmatrix.hh>
#include <dune/common/fvector.hh>

#include <dune/grid/common/gridenums.hh>
#include <dune/grid/common/rangegenerators.hh>

#include <dune/common/getreference.hh>

#include <dune/grid/utility/vectorcommdatahandle.hh>

#include <dune/fv/function/piecewiselinear.hh>

#include <fem-fv/limiterutility.hh>
#include <fem-fv/limitermodel.hh>

namespace Dune
{

  namespace FV
  {

    // LeastSquaresReconstruction
    // --------------------------

    template< class GV, class SV, class BV >
    class LeastSquaresReconstruction
    {
      typedef LeastSquaresReconstruction< GV, SV, BV > This;

    public:
      typedef GV GridView;
      typedef SV StateVector;
      typedef BV BoundaryValue;

      typedef FieldVector< typename GridView::ctype, GridView::dimensionworld > GlobalCoordinate;

      typedef typename GridView::Intersection Intersection;

      typedef typename FieldTraits< StateVector >::field_type Field;
      typedef FieldMatrix< Field, StateVector::dimension, GlobalCoordinate::dimension > Jacobian;

      template< class Mapper, class Vector >
      using Reconstruction = PiecewiseLinearFunction< GridView, Mapper, Vector, std::vector< Jacobian > >;

      LeastSquaresReconstruction ( const GridView &gridView, BoundaryValue boundaryValue )
        : gridView_( gridView ), boundaryValue_( std::move( boundaryValue ) )
      {}

      template< class Mapper, class Vector >
      Reconstruction< Mapper, Vector > operator() ( const Mapper &mapper, Vector u ) const
      {
        std::vector< Jacobian > du;
        (*this)( mapper, getReference( u ), du );
        return Reconstruction< Mapper, Vector >( mapper, std::move( u ), std::move( du ) );
      }

      template< class Mapper, class Vector >
      void operator () ( const Mapper &mapper, const Vector &u, std::vector< Jacobian > &du ) const
      {
        du.resize( mapper.size() );

        const auto end = gridView().template end<0, Dune::InteriorBorder_Partition>();
        for(auto it = gridView().template begin< 0, Dune::InteriorBorder_Partition>(); it != end; ++it)
        //for( const auto element : elements( gridView(), Partitions::interiorBorder ) )
        {
          const auto element = *it;

          const std::size_t elIndex = mapper.index( element );
          const GlobalCoordinate elCenter = element.geometry().center();

          Dune::FieldMatrix< Field, GlobalCoordinate::dimension, GlobalCoordinate::dimension > hessianInverse( 0 );
          Dune::FieldMatrix< Field, StateVector::dimension, GlobalCoordinate::dimension > negGradient( 0 );
          const auto iend = gridView().iend( element );
          for( auto iit = gridView().ibegin( element ); iit != iend; ++iit )
          //for( const auto intersection : intersections( gridView(), element ) )
          {
            const auto intersection = *iit ;
            const GlobalCoordinate iCenter = intersection.geometry().center();
            if( intersection.boundary() )
            {
              const GlobalCoordinate iNormal = intersection.centerUnitOuterNormal();
              const StateVector du = boundaryValue_( intersection, iCenter, iNormal, u[ elIndex ] ) - u[ elIndex ];
              const GlobalCoordinate dx = iCenter - elCenter;
              for( int i = 0; i < GlobalCoordinate::dimension; ++i )
                hessianInverse[ i ].axpy( dx[ i ], dx );
              for( int i = 0; i < StateVector::dimension; ++i )
                negGradient[ i ].axpy( du[ i ], dx );
            }
            else if( intersection.neighbor() )
            {
              const auto neighbor = intersection.outside();
              const GlobalCoordinate dx = neighbor.geometry().center() - elCenter;
              const StateVector du = u[ mapper.index( neighbor ) ] - u[ elIndex ];
              for( int i = 0; i < GlobalCoordinate::dimension; ++i )
                hessianInverse[ i ].axpy( dx[ i ], dx );
              for( int i = 0; i < StateVector::dimension; ++i )
                negGradient[ i ].axpy( du[ i ], dx );
            }
          }

          hessianInverse.invert();
          for( int j = 0; j < StateVector::dimension; ++j )
            hessianInverse.mv( negGradient[ j ], du[ elIndex ][ j ] );
        }

        auto handle = vectorCommDataHandle( mapper, du, [] ( Jacobian a, Jacobian b ) { return b; } );
        gridView().communicate( handle, InteriorBorder_All_Interface, ForwardCommunication );
      }

      const GridView &gridView () const { return gridView_; }

    private:
      GridView gridView_;
      BoundaryValue boundaryValue_;
    };



    // centralDifference
    // -----------------

    template< class SV, class GV, class BV >
    inline static LeastSquaresReconstruction< GV, SV, BV > leastSquaresReconstruction ( const GV &gridView, BV boundaryValue )
    {
      return LeastSquaresReconstruction< GV, SV, BV >( gridView, std::move( boundaryValue ) );
    }



    // LimitedLeastSquaresReconstruction
    // ---------------------------------

    template< class GV, class SV, class BV >
    class LimitedLeastSquaresReconstruction
    {
      typedef LimitedLeastSquaresReconstruction< GV, SV, BV > This;

    public:
      typedef GV GridView;
      typedef SV StateVector;
      typedef BV BoundaryValue;

      typedef FieldVector< typename GridView::ctype, GridView::dimensionworld > GlobalCoordinate;

      typedef typename GridView::Intersection Intersection;

      typedef typename FieldTraits< StateVector >::field_type Field;
      typedef FieldMatrix< Field, StateVector::dimension, GlobalCoordinate::dimension > Jacobian;

      typedef Dune::Fem::FemFVLimiterFunctionType  LimiterFunction;

      template< class Mapper, class Vector >
      using Reconstruction = PiecewiseLinearFunction< GridView, Mapper, Vector, std::vector< Jacobian > >;

      LimitedLeastSquaresReconstruction ( const GridView &gridView, BoundaryValue boundaryValue )
        : gridView_( gridView ), boundaryValue_( std::move( boundaryValue ) ), limiterFunction_()
      {}

      template< class Mapper, class Vector >
      Reconstruction< Mapper, Vector > operator() ( const Mapper &mapper, Vector u ) const
      {
        std::vector< Jacobian > du;
        (*this)( mapper, getReference( u ), du );
        return Reconstruction< Mapper, Vector >( mapper, std::move( u ), std::move( du ) );
      }

      template< class Mapper, class Vector >
      void operator () ( const Mapper &mapper, const Vector &u, std::vector< Jacobian > &du ) const
      {
        Vector factor;
        this->operator()( mapper, u, du, factor );
      }

      template< class Mapper, class Vector >
      void operator () ( const Mapper &mapper, const Vector &u, std::vector< Jacobian > &du, Vector& factor, const bool recompute = true ) const
      {
        du.resize( u.size() );

        std::vector< std::pair< GlobalCoordinate, StateVector > > differences;

        const auto end = gridView().template end<0, Dune::InteriorBorder_Partition>();
        for(auto it = gridView().template begin< 0, Dune::InteriorBorder_Partition>(); it != end; ++it)
        {
          const auto element = *it;
          const std::size_t elIndex = mapper.index( element );
          const GlobalCoordinate elCenter = element.geometry().center();

          differences.clear();
          Dune::FieldMatrix< Field, GlobalCoordinate::dimension, GlobalCoordinate::dimension > hessianInverse( 0 );
          Dune::FieldMatrix< Field, StateVector::dimension, GlobalCoordinate::dimension > negGradient( 0 );
          const auto iend = gridView().iend( element );
          for( auto iit = gridView().ibegin( element ); iit != iend; ++iit )
          {
            const auto intersection = *iit ;
            const GlobalCoordinate iCenter = intersection.geometry().center();
            if( intersection.boundary() )
            {
              const GlobalCoordinate iNormal = intersection.centerUnitOuterNormal();
              const StateVector du = boundaryValue_( intersection, iCenter, iNormal, u[ elIndex ] ) - u[ elIndex ];
              const GlobalCoordinate dx = iCenter - elCenter;
              for( int i = 0; i < GlobalCoordinate::dimension; ++i )
                hessianInverse[ i ].axpy( dx[ i ], dx );
              for( int i = 0; i < StateVector::dimension; ++i )
                negGradient[ i ].axpy( du[ i ], dx );
              differences.emplace_back( dx, du );
            }
            else if( intersection.neighbor() )
            {
              const auto neighbor = intersection.outside();
              const GlobalCoordinate dx = neighbor.geometry().center() - elCenter;
              const StateVector du = u[ mapper.index( neighbor ) ] - u[ elIndex ];
              for( int i = 0; i < GlobalCoordinate::dimension; ++i )
                hessianInverse[ i ].axpy( dx[ i ], dx );
              for( int i = 0; i < StateVector::dimension; ++i )
                negGradient[ i ].axpy( du[ i ], dx );
              differences.emplace_back( dx, du );
            }
          }

          hessianInverse.invert();
          for( int j = 0; j < StateVector::dimension; ++j )
            hessianInverse.mv( negGradient[ j ], du[ elIndex ][ j ] );

          if( ! factor.empty() )
          {
            StateVector& localfactor = factor[ elIndex ];
            if( recompute )
            {
              localfactor = 1;
              du[ elIndex ] = limit( differences, du[ elIndex ], localfactor );
            }
            else
            {
              for( int j = 0; j < StateVector::dimension; ++j )
                du[ j ] *= localfactor[ j ];
            }
          }
          else
          {
            StateVector localfactor( 1 );
            du[ elIndex ] = limit( differences, du[ elIndex ], localfactor );
          }
        }

        //auto handle = vectorCommDataHandle( mapper, du, [] ( Jacobian a, Jacobian b ) { return b; } );
        //gridView().communicate( handle, InteriorBorder_All_Interface, ForwardCommunication );
      }

      const GridView &gridView () const { return gridView_; }

    private:
      inline Jacobian limit ( const std::vector< std::pair< GlobalCoordinate, StateVector > > &differences, Jacobian du, StateVector& factor ) const
      {
        using std::abs;
        for( const auto &difference : differences )
        {
          StateVector slope;
          du.mv( difference.first, slope );
          for( int j = 0; j < StateVector::dimension; ++j )
          {
            const Field localFactor = limiterFunction_( slope[ j ], difference.second[ j ] );
            factor[ j ] = std::min( factor[ j ], localFactor );
            /*
            if( slope[ j ] * difference.second[ j ] <= 0 )
              return Jacobian( 0 );

            if( abs( difference.second[ j ] ) < abs( slope[ j ] ) )
              factor[ j ] = std::min( factor[ j ], difference.second[ j ] / slope[ j ] );
              */
          }
        }

        for( int j = 0; j < StateVector::dimension; ++j )
          du[ j ] *= factor[ j ];
        return du;
      }

      GridView gridView_;
      BoundaryValue boundaryValue_;
      LimiterFunction limiterFunction_;
    };



    // limitedLeastSquaresReconstruction
    // ---------------------------------

    template< class SV, class GV, class BV >
    inline static LimitedLeastSquaresReconstruction< GV, SV, BV > limitedLeastSquaresReconstruction ( const GV &gridView, BV boundaryValue )
    {
      return LimitedLeastSquaresReconstruction< GV, SV, BV >( gridView, std::move( boundaryValue ) );
    }

  } // namespace FV

} // namespace Dune

#endif // #ifndef DUNE_FV_LEASTSQUARESRECONSTRUCTION_HH
