#ifndef ROTATION_INITIALDATA_HH
#define ROTATION_INITIALDATA_HH

template <class DomainVector>
struct InitialData
{
  typedef Dune::FieldVector< double, 1 > RangeVector;
  static inline RangeVector initialData ( const DomainVector &x )
  {
    DomainVector c1( 0.5 ), c2( 0.5 ), c3( 0.5 );
    c1[ 0 ] = 0.5;  c1[ 1 ] = 0.75;
    c2[ 0 ] = 0.5;  c2[ 1 ] = 0.25;
    c3[ 0 ] = 0.25; c3[ 1 ] = 0.5;
    const double r = 0.15;

    // slotted cylinder
    if( (x - c1).two_norm() < r )
    {
      if( (std::abs( x[ 0 ] - c1[ 0 ] ) >= 0.025) || (x[ 1 ] >= c1[ 1 ] + 0.1) )
        return RangeVector{ 1.0 };
      else
        return RangeVector{ 0.0 };
    }

    // cone
    if( (x - c2).two_norm() < r )
      return RangeVector{ (r - (x - c2).two_norm()) / r };

    // hump
    if( (x - c3).two_norm() < r )
      return RangeVector{ (1.0 + std::cos( M_PI * (x - c3).two_norm() / r )) / 4.0 };

    // default
    return RangeVector{ 0.0 };
  }
};

#endif // #ifndef ROTATION_INITIALDATA_HH
