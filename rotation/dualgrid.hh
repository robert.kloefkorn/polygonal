#ifndef ROTATION_DUALGRID_HH
#define ROTATION_DUALGRID_HH

#include <memory>

#if HAVE_DUNE_POLYGONGRID
#include <dune/polygongrid/declaration.hh>

template< class ct >
inline static std::unique_ptr< Dune::PolygonGrid< ct > > createDualGrid ( const Dune::PolygonGrid< ct > &grid )
{
  return std::unique_ptr< Dune::PolygonGrid< ct > >( new Dune::PolygonGrid< ct >( grid.dualGrid() ) );
}
#endif // #if HAVE_DUNE_POLYGONGRID

template< class Grid >
inline static std::unique_ptr< Grid > createDualGrid ( const Grid &grid )
{
  DUNE_THROW( Dune::Exception, Dune::className< Grid >() << " does not provide the dual grid" );
}

#endif // #ifndef ROTATION_DUALGRID_HH
