#ifndef ROTATION_UPWINDFLUX_HH
#define ROTATION_UPWINDFLUX_HH

#include <utility>

#include "analyticalflux.hh"


// UpwindFlux
// ----------

template< class AnalyticalFlux >
class UpwindFlux;

template< class DV, class SV, class VF >
class UpwindFlux< TransportFlux< DV, SV, VF > >
{
  typedef UpwindFlux< TransportFlux< DV, SV, VF > > This;

public:
  typedef TransportFlux< DV, SV, VF > AnalyticalFlux;

  typedef typename AnalyticalFlux::field_type field_type;

  typedef typename AnalyticalFlux::DomainVector DomainVector;
  typedef typename AnalyticalFlux::StateVector StateVector;

  explicit UpwindFlux ( const AnalyticalFlux &analyticalFlux = AnalyticalFlux() ) : analyticalFlux_( analyticalFlux ) {}

  std::pair< field_type, field_type >
  operator() ( const DomainVector &x, const DomainVector &direction, const StateVector &u, const StateVector &v, StateVector &flux ) const
  {
    const field_type normalVelocity = analyticalFlux().velocityField()( x )*direction;
    flux = normalVelocity * (normalVelocity < 0 ? v : u);
    return std::make_pair( normalVelocity, normalVelocity );
  }

  const AnalyticalFlux &analyticalFlux () const { return analyticalFlux_; }

private:
  AnalyticalFlux analyticalFlux_;
};

#endif // #ifndef ROTATION_UPWINDFLUX_HH
