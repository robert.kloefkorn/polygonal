#ifndef ROTATION_BRICKEDWALL_HH
#define ROTATION_BRICKEDWALL_HH

#include <cstddef>
#include <type_traits>

#include <dune/common/exceptions.hh>
#include <dune/common/classname.hh>
#include <dune/common/math.hh>

#include <dune/geometry/type.hh>

#include <dune/grid/common/gridfactory.hh>

template< class Grid >
inline static std::unique_ptr< Grid > createBrickedWall ( std::size_t nx, std::size_t ny )
{
  typedef typename Grid::ctype ctype;

  Dune::GridFactory< Grid > factory;

  // insert vertices
  const ctype hx = ctype( 1 ) / ctype( nx );
  const ctype hy = ctype( 1 ) / ctype( ny );
  for( std::size_t j = 0u; j <= ny; ++j )
  {
    for( std::size_t i = 0u; i <= nx; ++i )
      factory.insertVertex( { i*hx, j*hy } );
  }

  // insert elements
  Dune::GeometryType type( Dune::GeometryType::none, 2 );
  std::vector< unsigned int > vertices;
  for( std::size_t j = 0u; j < ny; ++j )
  {
    for( std::size_t left = 0, right = (2u - (j & 1u)); left < nx; left = right, right = std::min( right+2u, nx ) )
    {
      vertices.clear();
      for( std::size_t i = left; i < right+1u; ++i )
        vertices.push_back( i + j*(nx+1u) );
      for( std::size_t i = right+1u; i > left; --i )
        vertices.push_back( (i-1u) + (j+1u)*(nx+1u) );
      factory.insertElement( type, vertices );
    }
  }

  // create grid
  return std::unique_ptr< Grid >( factory.createGrid() );
}

#endif // #ifndef ROTATION_BRICKEDWALL_HH
