#ifndef BURGERS_VTK_HH
#define BURGERS_VTK_HH

#include <functional>
#include <utility>

#include <dune/common/exceptions.hh>
#include <dune/common/fmatrix.hh>
#include <dune/common/fvector.hh>

#include <dune/geometry/dimension.hh>

#include <dune/grid/io/file/vtk.hh>

#include <dune/fv/function/piecewiseconstant.hh>
#include <dune/fv/function/vtk.hh>

#include <dune/viz/writer/vtkpolygonwriter.hh>

// setupVTKOutput
// --------------

template< class GridView, class CellData >
inline static void setupVTKOutput ( Dune::VTKWriter< GridView > &vtkWriter, const GridView &gridView, const CellData &cellData )
{
  vtkWriter.addCellData( makeVTKFunction( gridView, cellData, "concentration" ) );
}

template< class GridView, class CellData >
inline static void setupVTKOutput ( Dune::Viz::VTKPolygonWriter< GridView > &vtkWriter, const GridView &gridView, const CellData &cellData )
{
  typedef typename GridView::template Codim< 0 >::Entity Element;
  vtkWriter.addCellData( "concentration", [ cellData ] ( const Element &e ) {
      return localFunction( cellData, e )( e.geometry().center() );
    } );
}



// VTKOutput
// ---------

template< class GV, class M, class SV, class BV >
class VTKOutput
{
  typedef VTKOutput< GV, M, SV, BV > This;

public:
  typedef GV GridView;
  typedef M Mapper;
  typedef SV StateVector;
  typedef BV BoundaryValue;

  static const int dimension = GridView::dimensionworld;

  typedef Dune::FieldVector< typename GridView::ctype, dimension > GlobalCoordinate;

  typedef typename Dune::FieldTraits< StateVector >::field_type Field;
  typedef Dune::FieldMatrix< Field, StateVector::dimension, dimension > Jacobian;

private:
  struct Write;

  typedef std::function< void( const std::vector< StateVector > &, std::string ) > WriteFunction;

public:
  VTKOutput ( const GridView &gridView, const Mapper &mapper, BoundaryValue boundaryValue, const std::string& name );

  void operator() ( double time, const std::vector< StateVector > &u )
  {
    write_( u, name_ + "-" + std::to_string( count_++ ) );
  }

  std::size_t count () const { return count_; }

private:
  template< class Reconstruction >
  static WriteFunction makeWrite ( const GridView &gridView, const Mapper &mapper, std::string writer, Reconstruction reconstruction );

  std::string name_;
  std::size_t count_ = 0u;
  WriteFunction write_;
};



// vtkOutput
// ---------

template< class SV, class GV, class M, class BV >
inline static VTKOutput< GV, M, SV, BV >
vtkOutput ( const GV &gridView, const M &mapper, BV boundaryValues, const std::string& name )
{
  return VTKOutput< GV, M, SV, BV >( gridView, mapper, std::move( boundaryValues ), name );
}



// VTKOutput::Write
// ----------------

template< class GV, class M, class SV, class BV >
struct VTKOutput< GV, M, SV, BV >::Write
{
  Write ( const GridView &gridView, const Mapper &mapper )
    : gridView_( gridView ), mapper_( mapper )
  {}

  template< class VTKWriter >
  void operator() ( VTKWriter &vtkWriter, const std::vector< StateVector > &u, std::string name ) const
  {
    const auto cellData = Dune::FV::piecewiseConstantFunction( gridView(), mapper_, std::ref( u ) );
    setupVTKOutput( vtkWriter, gridView(), cellData );
    vtkWriter.write( std::move( name ) );
  }

  const GridView &gridView () const { return gridView_; }

private:
  const GridView &gridView_;
  const Mapper &mapper_;
};



// Implementation of VTKOutput
// ---------------------------

template< class GV, class M, class SV, class BV >
inline VTKOutput< GV, M, SV, BV >::VTKOutput ( const GridView &gridView, const Mapper &mapper, BoundaryValue boundaryValue, const std::string& name ) //, const Dune::ParameterTree &parameterTree )
  : name_( name )
{
  Write write( gridView, mapper );

  const std::string writer = "polygon" ;
  //parameterTree.get< std::string >( "output.writer", "none" );
  if( writer == "none" )
    write_ = [] ( const std::vector< StateVector > &u, std::string name ) {};
  if( writer == "default" )
  {
    write_ = [ write ] ( const std::vector< StateVector > &u, std::string name ) {
               Dune::VTKWriter< GridView > vtkWriter( write.gridView() );
               write( vtkWriter, u, std::move( name ) );
             };
  }
  else if( writer == "polygon" )
  {
    write_ = [ write ] ( const std::vector< StateVector > &u, std::string name ) {
               Dune::Viz::VTKPolygonWriter< GridView > vtkWriter( write.gridView() );
               write( vtkWriter, u, std::move( name ) );
             };
  }
  else
    DUNE_THROW( Dune::RangeError, "Invalid writer: '" << writer << "'" );
}

#endif // #ifndef BURGERS_VTK_HH
