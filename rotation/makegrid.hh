#ifndef ROTATION_MAKEGRID_HH
#define ROTATION_MAKEGRID_HH

#include <cstdlib>
#include <iostream>

#include <dune/common/parametertreeparser.hh>

#include <dune/grid/io/file/dgfparser/gridptr.hh>

#include "blossomgrid.hh"
#include "brickedwall.hh"
#include "dualgrid.hh"


// readDGF
// -------

template< class Grid >
inline static std::unique_ptr< Grid > readDGF ( const std::string &fileName, int gridLevel )
{
  Dune::GridPtr< Grid > gridPtr( fileName );
  gridPtr->globalRefine( gridLevel );
  return std::unique_ptr< Grid >( gridPtr.release() );
}


template< class Grid >
inline static std::unique_ptr< Grid > makeGrid ( const Dune::ParameterTree &parameterTree )
{
  const std::string gridType = parameterTree.get< std::string >( "grid.type", "dgf" );
  const int gridLevel = parameterTree.get< int >( "grid.level", 0 );
  if( gridType == "dgf" )
    return readDGF< Grid >( parameterTree.get< std::string >( "grid.dgf" ), gridLevel );
  else if( gridType == "dgf-dual" )
    return createDualGrid( *readDGF< Grid >( parameterTree.get< std::string >( "grid.dgf" ), gridLevel ) );
  else if( gridType == "blossoms" )
  {
    const std::size_t cellsX = parameterTree.get< std::size_t >( "grid.cells.x" ) << gridLevel;
    const std::size_t cellsY = parameterTree.get< std::size_t >( "grid.cells.y" ) << gridLevel;
    return createBlossomGrid< Grid >( cellsX, cellsY );
  }
  else if( gridType == "bricks" )
  {
    const std::size_t cellsX = parameterTree.get< std::size_t >( "grid.cells.x" ) << gridLevel;
    const std::size_t cellsY = parameterTree.get< std::size_t >( "grid.cells.y" ) << gridLevel;
    return createBrickedWall< Grid >( cellsX, cellsY );
  }
  else
  {
    std::cerr << "Error: Invalid grid type (" << gridType << ")." << std::endl;
    std::exit( 1 );
  }
}

#endif // #ifndef ROTATION_MAKEGRID_HH
