#include <config.h>

#include <iostream>

#include <dune/common/exceptions.hh>
#include <dune/common/parallel/mpihelper.hh>
#include <dune/common/parametertreeparser.hh>
#include <dune/common/timer.hh>

#include <dune/grid/io/file/dgfparser/gridptr.hh>
#include <dune/grid/common/mcmgmapper.hh>

#include <dune/fv/function/piecewiseconstant.hh>
#include <dune/fv/function/piecewiselinear.hh>
#include <dune/fv/interpolate.hh>
#include <dune/fv/leastsquaresreconstruction.hh>
#include <dune/fv/lpreconstruction.hh>
#include <dune/fv/qpreconstruction.hh>
#include <dune/fv/odestep.hh>
#include <dune/fv/operator.hh>

#if HAVE_OPM_GRID
#include <dune/grid/polyhedralgrid.hh>
#include <dune/grid/polyhedralgrid/dgfparser.hh>
#endif // #if HAVE_OPM_GRID

#include <dune/grid/io/file/dgfparser/dgfyasp.hh>

#if HAVE_DUNE_POLYGONGRID
#include <dune/polygongrid/dgf.hh>
#include <dune/polygongrid/gridfactory.hh>
#include <dune/polygongrid/grid.hh>
#endif // #if HAVE_DUNE_POLYGONGRID

#if HAVE_DUNE_ALUGRID
#include <dune/alugrid/grid.hh>
#include <dune/alugrid/dgf.hh>
#endif // #if HAVE_DUNE_ALUGRID


#include "analyticalflux.hh"
#include "initialdata.hh"
#include "makegrid.hh"
#include "vtk.hh"
#include "upwindflux.hh"

using Dune::FV::finiteVolumeOperator;
using Dune::FV::interpolate;
using Dune::FV::piecewiseConstantFunction;
using Dune::FV::piecewiseLinearFunction;



// makeRightHandSide
// -----------------

template< class FVOperator, class Mapper, class SV = typename FVOperator::StateVector >
inline static std::function< double( double, const std::vector< SV > &, std::vector< SV > & ) >
makeRightHandSide ( const Dune::ParameterTree &parameterTree, const FVOperator &fvOperator, const Mapper &mapper )
{
  const std::string reconstruction = parameterTree.get< std::string >( "reconstruction", "constant" );
  if( reconstruction == "constant" )
    return [ &fvOperator, &mapper ] ( double, const std::vector< SV > &u, std::vector< SV > &update ) {
             return fvOperator( piecewiseConstantFunction( fvOperator.gridView(), mapper, std::ref( u ) ), mapper, update );
           };
  else if( reconstruction == "linear" )
  {
    auto reconstruction = Dune::FV::leastSquaresReconstruction< SV >( fvOperator.gridView(), fvOperator.boundaryValue() );
    return [ &fvOperator, &mapper, reconstruction ] ( double, const std::vector< SV > &u, std::vector< SV > &update ) {
             return fvOperator( reconstruction( mapper, std::ref( u ) ), mapper, update );
           };
  }
  else if( reconstruction == "limited" )
  {
    auto reconstruction = Dune::FV::limitedLeastSquaresReconstruction< SV >( fvOperator.gridView(), fvOperator.boundaryValue() );
    return [ &fvOperator, &mapper, reconstruction ] ( double, const std::vector< SV > &u, std::vector< SV > &update ) {
             return fvOperator( reconstruction( mapper, std::ref( u ) ), mapper, update );
           };
  }
  else if( reconstruction == "lp" )
  {
    typedef typename Dune::FieldTraits< SV >::real_type Real;
    const Real tolerance = parameterTree.get< Real >( "lpreconstruction.tolerance" );
    auto reconstruction = Dune::FV::lpReconstruction< SV >( fvOperator.gridView(), fvOperator.boundaryValue(), tolerance );
    return [ &fvOperator, &mapper, reconstruction ] ( double, const std::vector< SV > &u, std::vector< SV > &update ) {
             return fvOperator( reconstruction( mapper, std::ref( u ) ), mapper, update );
           };
  }
  else if( reconstruction == "qp" )
  {
    typedef typename Dune::FieldTraits< SV >::real_type Real;
    const Real tolerance = parameterTree.get< Real >( "qpreconstruction.tolerance" );
    auto reconstruction = Dune::FV::qpReconstruction< SV >( fvOperator.gridView(), fvOperator.boundaryValue(), tolerance );
    return [ &fvOperator, &mapper, reconstruction ] ( double, const std::vector< SV > &u, std::vector< SV > &update ) {
             return fvOperator( reconstruction( mapper, std::ref( u ) ), mapper, update );
           };
  }
  else
    DUNE_THROW( Dune::RangeError, "Invalid reconstruction: '" << reconstruction << "'" );
}



// makeOdeStep
// -----------

template< class SV, class RightHandSide >
inline static Dune::FV::AbstractOdeStep< SV >
makeOdeStep ( const Dune::ParameterTree &parameterTree, const RightHandSide &rightHandSide )
{
  const std::string solver = parameterTree.get< std::string >( "ode.solver" );
  if( solver == "euler" )
    return Dune::FV::explicitEulerOdeStep< SV >( rightHandSide );
  else if( solver == "heun" )
    return Dune::FV::heunOdeStep< SV >( rightHandSide );
  else
  {
    std::cerr << "Error: Invalid ode solver (" << solver << ")." << std::endl;
    std::exit( 1 );
  }
}

template <class GridView, class Problem, class StateVector>
double l1error( const GridView& gridView, const Problem& problem, const StateVector& u )
{
  typedef typename GridView :: template Codim< 0 > :: Entity :: Geometry :: GlobalCoordinate  GlobalCoordinate;
  typedef typename StateVector :: value_type RangeType;
  GlobalCoordinate local( 0.25 );

  double norm = 0 ;
  // loop over all entities
  const auto end = gridView.template end< 0 >();
  for( auto it = gridView.template begin< 0 >(); it != end; ++it )
  {
    const auto &entity = *it;
    const auto &geometry = entity.geometry();
    const GlobalCoordinate center = geometry.center() ;

    const int index = gridView.indexSet().index( entity );

    // get barycenter (can be obtained also by geometry.center())
    RangeType uExact = problem( geometry.center() );

    const int corners = geometry.corners();
    for( int i=0; i<corners; ++i )
    {
      RangeType tmp = problem( geometry.corner( i ) );
      uExact += tmp;
    }

    uExact /= double(corners+1);
    norm += geometry.volume() * (u[ index ] - uExact).one_norm();
  }

  return norm;
}



// main
// ----

int main ( int argc, char **argv )
try
{
  Dune::MPIHelper::instance( argc, argv );

  if( argc < 2 )
  {
    std::cerr << "Usage: " << argv[ 0 ] << " <parameter file>" << std::endl;
    return 1;
  }

  Dune::ParameterTree parameterTree;
  Dune::ParameterTreeParser::readINITree( argv[ 1 ], parameterTree );

  // create grid
  //typedef Dune::PolyhedralGrid< 3, 3 > Grid;
  //typedef Dune::PolyhedralGrid< 2, 2 > Grid;
  //typedef Dune::YaspGrid< 2 > Grid ;
  //typedef Dune::ALUGrid< 2, 2, Dune::simplex, Dune::nonconforming >  Grid;
  //typedef Dune::YaspGrid< 3 > Grid ;
  //typedef Dune::GridSelector::GridType Grid;
  typedef GRID Grid;
  std::unique_ptr< Grid > gridPtr = makeGrid< Grid >( parameterTree );
  Grid &grid = *gridPtr;

  // create fluxes
  typedef Dune::FieldVector< double, 1 > StateVector;
  typedef Dune::FieldVector< typename Grid::ctype, Grid::dimensionworld > GlobalCoordinate;
  auto analyticalFlux = rotationTransportFlux< StateVector >( parameterTree.get< GlobalCoordinate >( "rotation.center" ), parameterTree.get< double >( "rotation.angularvelocity" ) );
  typedef UpwindFlux< decltype( analyticalFlux ) > NumericalFlux;
  NumericalFlux numericalFlux( analyticalFlux );

  // create mapper
  typedef Grid::LeafGridView GridView;
  const GridView gridView = grid.leafGridView();
  Dune::MultipleCodimMultipleGeomTypeMapper< GridView, Dune::MCMGElementLayout > mapper( gridView );

  // create data vectors
  std::vector< StateVector > u( mapper.size() );
  std::vector< StateVector > tmp( mapper.size() );

  // interpolate initial values
  interpolate( gridView, &InitialData< GlobalCoordinate >::initialData, mapper, u );

  // ode parameters
  const double startTime = parameterTree.get< double >( "time.start" );
  const double endTime = parameterTree.get< double >( "time.end" );
  const bool dtFixed = parameterTree.hasKey( "time.fixeddt" );
  const double dtMax = parameterTree.get< double >( dtFixed ? "time.fixeddt" : "time.maxdt" );
  const double cfl = (dtFixed ? 1.0 : parameterTree.get< double >( "time.cfl" ));

  // create finite volume operator and update vector
  const auto boundaryValue = [] ( const typename GridView::Intersection &i, const GlobalCoordinate &x, const GlobalCoordinate &n, const StateVector &uIn ) { return StateVector{ 0.0 }; };
  const auto fvOperator = finiteVolumeOperator( gridView, numericalFlux, boundaryValue );

  const auto rightHandSide = makeRightHandSide( parameterTree.sub( "fv" ), fvOperator, mapper );

  // set up output
  const double outputStep = parameterTree.get< double >( "output.timestep" );
  auto ioCallback = vtkOutput< StateVector >( gridView, mapper, boundaryValue, parameterTree.get<std::string>("output.name") );

  // create ODE solver
  const auto odeStep = makeOdeStep< StateVector >( parameterTree, rightHandSide );

  // solve ode
  double time = startTime;
  double dt = dtMax;
  Dune::Timer timer( false );
  std::size_t numTimeSteps = 0;
  for( ; time < endTime; ++numTimeSteps )
  {
    if( time + 64*std::numeric_limits< double >::epsilon() >= startTime + ioCallback.count()*outputStep )
      ioCallback( time, u );

    timer.start();
    const double dtEstimate = odeStep( time, u, dt, tmp );

    std::cout << "# time: " << time << ", dt = " << dt << ", dtEstimate = " << dtEstimate << std::endl;
    if( (dt <= dtEstimate) || dtFixed )
    {
      std::swap( u, tmp );
      time += dt;
    }
    dt = (dtFixed ? dtMax : std::min( cfl*dtEstimate, dtMax ));
    timer.stop();
  }
  ioCallback( time, u );

  // compute element error
  const double angle = (time - startTime) * analyticalFlux.velocityField().angularVelocity();
  Dune::FieldMatrix< Grid::ctype, Grid::dimensionworld, Grid::dimensionworld > rotation( 0 );
  for( int i=0; i<Grid::dimension; ++i ) rotation[i][i] = 1.0;
  rotation[ 0 ][ 0 ] =  std::cos( angle );
  rotation[ 0 ][ 1 ] =  std::sin( angle );
  rotation[ 1 ][ 0 ] = -std::sin( angle );
  rotation[ 1 ][ 1 ] =  std::cos( angle );

  const GlobalCoordinate center = analyticalFlux.velocityField().center();
  const auto finalData = [ rotation, center ] ( GlobalCoordinate x ) { rotation.mv( x - center, x ); return InitialData< GlobalCoordinate >::initialData( x + center ); };
  interpolate( gridView, finalData, mapper, tmp );
  std::transform( u.begin(), u.end(), tmp.begin(), tmp.begin(), std::minus< StateVector >() );

  double error = l1error( gridView, finalData, u );

  std::cout << "L1 error " << error << std::endl;

  std::cout << "# Elements       dt           L1-Error            L2-Error        CPU-Time [s]" << std::endl;
  std::cout << std::setw( 10 ) << mapper.size() << "    "
            << std::setw( 8 ) << std::setprecision( 6 ) << std::fixed << (time / static_cast< double >( numTimeSteps )) << "    "
            << std::setw( 16 ) << std::setprecision( 10 ) << std::scientific << l1Norm( gridView, piecewiseConstantFunction( gridView, mapper, std::ref( tmp ) ) ) << "    "
            << std::setw( 16 ) << std::setprecision( 10 ) << std::scientific << l2Norm( gridView, piecewiseConstantFunction( gridView, mapper, std::ref( tmp ) ) ) << "    "
            << std::setw( 12 ) << std::setprecision( 2 ) << std::fixed << timer.elapsed() << std::endl;

  return 0;
}
catch( const Dune::Exception &e )
{
  std::cerr << e << std::endl;
  return 1;
}
