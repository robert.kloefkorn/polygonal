#ifndef ROTATION_ANALYTICALFLUX_HH
#define ROTATION_ANALYTICALFLUX_HH

#include <type_traits>
#include <utility>

#include <dune/common/fmatrix.hh>
#include <dune/common/fvector.hh>


// TransportFlux
// -------------

template< class DV, class SV, class VF = std::function< DV( const DV & ) > >
class TransportFlux
{
  typedef TransportFlux< DV, SV, VF > This;

public:
  typedef DV DomainVector;
  typedef SV StateVector;
  typedef VF VelocityField;

  typedef typename Dune::FieldTraits< StateVector >::field_type field_type;

  explicit TransportFlux ( const VelocityField &velocityField ) noexcept : velocityField_( velocityField ) {}

  void operator() ( const DomainVector &x, const StateVector &u,
                    Dune::FieldMatrix< field_type, StateVector::dimension, DomainVector::dimension > &flux ) const noexcept
  {
    const auto velocity = velocityField()( x );
    for( int i = 0; i < StateVector::dimension; ++i )
      flux[ i ] = u[ i ]*velocity;
  }

  std::pair< field_type, field_type >
  operator() ( const DomainVector &x, const DomainVector &direction, const StateVector &u, StateVector &flux ) const noexcept
  {
    const field_type normalVelocity = velocityField()( x ) * direction;
    flux = normalVelocity * u;
    return std::make_pair( normalVelocity, normalVelocity );
  }

  const VelocityField &velocityField () const noexcept { return velocityField_; }

private:
  VelocityField velocityField_;
};



// RotationVelocityField
// ---------------------

template< class K, int dim >
class RotationVelocityField
{
  typedef RotationVelocityField< K, dim > This;

public:
  typedef K field_type;

  typedef Dune::FieldVector< field_type, dim > DomainVector;

  RotationVelocityField ( const DomainVector &center, field_type angularVelocity ) noexcept
    : center_( center ), angularVelocity_( angularVelocity )
  {}

  DomainVector operator() ( const DomainVector &x ) const
  {
    DomainVector z = x - center();
    z *= angularVelocity();
    DomainVector ret( 0 );
    ret[ 0 ] = -z[ 1 ];
    ret[ 1 ] =  z[ 0 ];
    ret[ 2 ] =  z[ 2 ];
    return ret;
  }

  const DomainVector &center () const noexcept { return center_; }
  field_type angularVelocity () const noexcept { return angularVelocity_; }

private:
  DomainVector center_;
  field_type angularVelocity_;
};



// rotationTransportFlux
// ---------------------

template< class SV, class K, int dim >
inline static TransportFlux< Dune::FieldVector< K, dim >, SV, RotationVelocityField< K, dim > >
rotationTransportFlux ( const Dune::FieldVector< K, dim > &center, K angularVelocity ) noexcept
{
  return TransportFlux< Dune::FieldVector< K, dim >, SV, RotationVelocityField< K, dim > >( RotationVelocityField< K, dim >( center, angularVelocity ) );
}

#endif // #ifndef ROTATION_ANALYTICALFLUX_HH
