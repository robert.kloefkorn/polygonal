#ifndef ROTATION_BLOSSOMGRID_HH
#define ROTATION_BLOSSOMGRID_HH

#include <type_traits>

#include <dune/common/exceptions.hh>
#include <dune/common/classname.hh>
#include <dune/common/math.hh>

#include <dune/geometry/type.hh>

#include <dune/grid/common/gridfactory.hh>

template< class Grid >
inline static std::unique_ptr< Grid > createBlossomGrid ( unsigned int nx, unsigned int ny )
{
  typedef typename Grid::ctype ctype;

  Dune::GridFactory< Grid > factory;

  // insert vertices
  const ctype hx = ctype( 1 ) / ctype( 3u*nx );
  const ctype hy = ctype( 1 ) / ctype( 3u*ny );
  for( unsigned int j = 0u; j <= 3u*ny; ++j )
  {
    for( unsigned int i = 0u; i <= 3u*nx; ++i )
      factory.insertVertex( { i*hx, j*hy } );
  }

  // insert elements
  Dune::GeometryType type( Dune::GeometryType::none, 2 );
  for( unsigned int j = 0u; j < ny; ++j )
  {
    for( unsigned int i = 0u; i < nx; ++i )
    {
      const unsigned int p = 3u*nx+1u;
      const unsigned int k = 3u*(j*p + i);
      factory.insertElement( type, { k, k+1u, k+2u, k+p+2u, k+p+1u, k+p } );
      factory.insertElement( type, { k+2u, k+3u, k+p+3u, k+2u*p+3u, k+2u*p+2u, k+p+2u } );
      factory.insertElement( type, { k+p, k+p+1u, k+2u*p+1u, k+3u*p+1u, k+3u*p, k+2u*p } );
      factory.insertElement( type, { k+p+1u, k+p+2u, k+2u*p+2u, k+2u*p+1u } );
      factory.insertElement( type, { k+2u*p+1u, k+2u*p+2u, k+2u*p+3u, k+3u*p+3u, k+3u*p+2u, k+3u*p+1u } );
    }
  }

  // create grid
  return std::unique_ptr< Grid >( factory.createGrid() );
}

#endif // #ifndef ROTATION_BLOSSOMGRID_HH
