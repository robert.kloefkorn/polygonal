#!/bin/bash
# on openSUSE mpi variables need to be loaded
#source /usr/lib64/mpi/gcc/openmpi/bin/mpivars.sh

PING_PATH=/bin
if ! test -f $PING_PATH/ping ; then
  echo "Couldn't find ping command in '$PING_PATH' needed for ert installation"
  exit 1
fi

OPMURL=https://github.com/dr-robertk/
# this script downloads the necessary set of dune opm modules
# to run the opm-autodiff blackoil simulator

#change appropriately, i.e. 2.2, 2.3 or empty which refers to master

# when empty the current master is selected
DUNEVERSION=2.4
OPMVERSION=2016.04

FLAGS="-O3 -DNDEBUG -funroll-loops -finline-functions -Wall -ftree-vectorize -fno-stack-protector -mtune=native"

COREMODULES="dune-common dune-istl dune-geometry dune-grid"
OPMMODULES="dune-fem opm-common opm-parser opm-material opm-core opm-grid polygonal-fv"

############################################################################
# Installation of ERT
git clone -b release/$OPMVERSION http://github.com/Ensembles/ert.git
if ! test -d ert ; then
  echo "Problem with ert directory"
  exit 1
fi
cd ert
ERTINSTALLDIR=`pwd`
if ! test -d build ; then
  mkdir build ; cd build
  # sometimes it might be necessary to also specify the path to ping command
  PINGPATH=/bin
  cmake ../devel/ -DCMAKE_INSTALL_PREFIX=$ERTINSTALLDIR -DPING_PATH=$PING_PATH
  make -j4 ; make install
  cd ../
fi
cd ../
############################################################################

# build flags for all DUNE and OPM modules
# change according to your needs
if ! test -f config.opts ; then
echo "\
OPMDIR=`pwd`
BUILDDIR=./
USE_CMAKE=yes
MAKE_FLAGS=-j4
CMAKE_FLAGS=\"-DCMAKE_CXX_FLAGS=\\\"$FLAGS\\\"  \\
 -DDUNE_GRID_EXPERIMENTAL_GRID_EXTENSIONS=TRUE  \\
 -DALLOW_CXXFLAGS_OVERWRITE=ON \\
 -DOPM_COMMON_ROOT=\$OPMDIR/opm-common \\
 -DUSE_MPI=ON \\
 -DBUILD_TESTING=OFF \\
 -DSIBLING_SEARCH=OFF \\
 -DCMAKE_DISABLE_FIND_PACKAGE_LATEX=TRUE\" " > config.opts
fi

DUNEBRANCH=
if [ "$DUNEVERSION" != "" ] ; then
  DUNEBRANCH="-b releases/$DUNEVERSION"
fi

# get all dune modules necessary
for MOD in $COREMODULES ; do
  git clone $DUNEBRANCH http://gitlab.dune-project.org/core/$MOD.git
done

OPMBRANCH="-b polygonal-fv"
# get all OPM modules necessary
for MOD in $OPMMODULES ; do
  if [ "$MOD" == "dune-fem" ] ; then
    git clone $OPMBRANCH http://gitlab.dune-project.org/dune-fem/$MOD.git
  elif [ "$MOD" == "polygonal-fv" ] ; then
    git clone -b polygonal-fv https://users.dune-project.org/repositories/projects/$MOD.git
  else
    URL="$OPMURL$MOD"
    git clone $OPMBRANCH $URL
  fi
done

# build all DUNE and OPM modules in the correct order
./dune-common/bin/dunecontrol --opts=config.opts --module=polygonal-fv all
