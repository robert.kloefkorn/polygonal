#include <iostream>
#include <fstream>
#include <cmath>
#include <vector>
#include <string>
#include <algorithm>
#include <map>
#include <set>

template <class Coord, class Vec>
void findBoundingBox( const Coord& coords, Vec& bndBox )
{
  bndBox.clear();
  bndBox.resize( 3 );

  for( int j=0; j<3; ++j )
  {
    bndBox[ j ].push_back(  1e308 );
    bndBox[ j ].push_back( -1e308 );
  }

  const int nVx = coords.size() ;
  for( int vx = 0; vx < nVx; ++ vx )
  {
    const int j = vx % 3;
    bndBox[ j ][ 0 ] = std::min( bndBox[ j ][ 0 ], coords[ vx ] );
    bndBox[ j ][ 1 ] = std::max( bndBox[ j ][ 1 ], coords[ vx ] );
  }

  for( int j=0; j<3; ++j )
  {
    std::cout << "Dir " << j << " bnd = " << bndBox[ j ][ 0 ] << " " << bndBox[ j ][ 1 ] << std::endl;
  }
}

template <class BndBox, class Coord, class Elem, class Face>
void findBoundaryFaces( const BndBox& bndBox, const Coord& coords, const Elem& element, Face& faces )
{
  const int nVx = element.size();

  std::map< int , std::set< int > > faceMap;

  for( int vx = 0; vx < nVx; ++ vx )
  {
    int vertex = element[ vx ] * 3;
    double coord[ 3 ] = { coords[ vertex ], coords[ vertex+1 ], coords[ vertex+2 ] };
    for( int j=0; j<3; ++j )
    {
      for( int k=0; k<2; ++k )
      {
        if( std::abs( coord[ j ] - bndBox[ j ][ k ] ) < 1e-12 )
        {
          faceMap[ j*2+k ].insert( element[ vx ] );
        }
      }
    }
  }


  faces.clear();
  faces.resize( faceMap.size(), std::vector<int> () );

  int count = 0;
  for( const auto& face : faceMap )
  {
    //std::cout << "Found boundary Face ";
    for( const auto& vxs : face.second )
    {
      faces[ count ].push_back( vxs );
      //std::cout << vxs << " ";
    }
    ++count;
  }
}

template <class Vector>
void setIntersection( const Vector& vec1, const Vector& vec2, Vector& result )
{
  const int v1 = vec1.size();
  const int v2 = vec2.size();
  result.clear();
  for( int i=0; i < v1; ++i )
  {
    for( int j=0; j<v2; ++j )
    {
      if( vec1[ i ] == vec2[ j ] )
      {
         result.push_back( vec1[ i ] );
         break;
      }
    }
  }
}


int main( int argc, char** argv)
{
  if( argc < 2 )
  {
    std::cout << "usage: " << argv[0] << " <flma file> <optional dgf filename>" << std::endl;
    return 1;
  }

  std::ifstream file( argv[1] );
  int nVx = 0;
  file >> nVx;

  std::vector< double > vertices( nVx * 3, 0.0 );
  for( int i=0; i<nVx*3; ++i )
  {
    file >> vertices[ i ];
  }

  std::vector< std::vector< double > > bndBox;

  findBoundingBox( vertices, bndBox );

  int nElem = 0;
  file >> nElem;
  std::vector< std::vector< int > > elements( nElem );

  for( int i=0; i<nElem; ++i )
  {
    int nElVx = 0;
    file >> nElVx;
    elements[ i ].resize( nElVx );
    for( int vx=0; vx < nElVx; ++vx )
    {
      file >> elements[ i ][ vx ];
    }

    //std::sort( elements[ i ].begin(), elements[ i ].end() );
    std::cout << "Elem " << i << " = ";
    for( int vx=0; vx < nElVx; ++vx )
    {
      std::cout << elements[ i ][ vx ] << " " ;
    }
    std::cout << std::endl;
  }

  std::vector< std::vector< int > > polygons;
  std::vector< std::vector< int > > polyhedrons( nElem );

  std::vector< int > intersection;
  intersection.reserve( 100 );

  std::vector< std::vector< int > > boundaryFace;

  for( int i=0; i<nElem; ++i )
  {
    findBoundaryFaces( bndBox, vertices, elements[ i ], boundaryFace );

    const int bndSize = boundaryFace.size();
    if( bndSize > 0 )
    {
      for( int j=0; j<bndSize; ++j )
      {
        if( boundaryFace[ j ].size() > 2 )
        {
          int polyIdx = polygons.size();
          std::cout << "Found bndpoly " << polyIdx << ": ";
          for( size_t k=0; k<boundaryFace[ j ].size(); ++k )
          {
            std::cout << boundaryFace[ j ][ k ] << " ";
          }
          std::cout << std::endl;
          polygons.push_back( boundaryFace[ j ] );
          polyhedrons[ i ].push_back( polyIdx );
        }
      }
    }

    for( int j=i+1; j<nElem; ++j )
    {
      setIntersection( elements[ i ], elements[ j ], intersection );
      if( intersection.size() > 2 )
      {
        int polyIdx = polygons.size();
        std::cout << "Found polygon " << polyIdx << ": ";
        for( size_t k=0; k<intersection.size() ; ++k )
        {
          std::cout << intersection[ k ] << " ";
        }
        std::cout << std::endl;
        polygons.push_back( intersection );
        polyhedrons[ i ].push_back( polyIdx );
        polyhedrons[ j ].push_back( polyIdx );
      }
    }
  }

  std::string dgfname( argv[1] );
  dgfname += ".dgf";
  if( argc > 2 )
    dgfname = argv[ 2 ];
  std::ofstream dgffile( dgfname );
  dgffile << "DGF" << std::endl;
  dgffile << "Vertex" << std::endl;
  for( int i=0; i<3*nVx; )
  {
    for( int j=0; j<3; ++j, ++i )
      dgffile << vertices[ i ] << " ";
    dgffile << std::endl;
  }
  dgffile << "#" << std::endl;

  dgffile << "Polygon" << std::endl;
  const int poly = polygons.size();
  for( int i=0; i< poly; ++i )
  {
    const int nPolyVx = polygons[ i ].size();
    dgffile << nPolyVx << " ";
    for( int j=0; j<nPolyVx; ++j )
    {
      dgffile << polygons[ i ][ j ] << " " ;
    }
    dgffile << std::endl;
  }
  dgffile << "#" << std::endl;

  dgffile << "Polyhedron" << std::endl;
  const int polyh = polyhedrons.size();
  for( int i=0; i< polyh; ++i )
  {
    const int nPolyVx = polyhedrons[ i ].size();
    dgffile << nPolyVx << " ";
    for( int j=0; j<nPolyVx; ++j )
    {
      dgffile << polyhedrons[ i ][ j ] << " " ;
    }
    dgffile << std::endl;
  }
  dgffile << "#" << std::endl;
  return 0;
}
